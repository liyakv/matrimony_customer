from __future__ import division
from ast import Str
from email.charset import Charset
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Table, BigInteger, Text, Enum, true
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Boolean, Date, DateTime, Float, Time as SqlTime
from sqlalchemy.sql import func
from app.database.main import Base
from datetime import datetime
from sqlalchemy_utils import URLType, JSONType
from sqlalchemy_utils.types.choice import ChoiceType
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import backref


class BaseModel(Base):
    __abstract__ = True
    is_deleted =  Column(Boolean(), default=False)
    created_on = Column(DateTime(timezone=True), default=datetime.now)
    updated_on = Column(DateTime,  default=datetime.now, onupdate=datetime.now)
    deleted_on = Column(DateTime(timezone=True), nullable=True)


class User(Base):
    __tablename__ = 'matrimony_user'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=True)
    username = Column(String(100), unique=True)
    email = Column(String(100), nullable=True)
    gender = Column(String(15), nullable=True)
    phone_number = Column(String(50), unique=True)
    dob = Column(DateTime(timezone=True), nullable=True)
    is_user_verified = Column(Boolean(), default=False)
    is_number_verified = Column(Boolean(), default=False)
    is_photos_verified = Column(Boolean(), default=False)
    is_documents_verified = Column(Boolean(), default=False)
    date_joined = Column(DateTime(timezone=True), nullable=True)
    is_admin = Column(Boolean(), default=False)
    is_staff = Column(Boolean(), default=False)
    is_customer = Column(Boolean(), default=False)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
    password = Column(String(512), nullable=True)


class OtpVerification(Base):
    __tablename__ = 'otp_verification'
    id = Column(Integer, primary_key=True)
    phone_number = Column(String(15))
    otp = Column(String(300))
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_otp_details')
    is_verified = Column(Boolean(), default=False)
    created_at = Column(DateTime(timezone=True), default=datetime.now)


class Packages(BaseModel):
    __tablename__ = 'matrimony_packages'
    id = Column(Integer, primary_key=True, autoincrement=True)
    package_name = Column(String(200))
    is_premium = Column(Boolean(), default=False)
    months = Column(Integer, default=1)
    amount = Column(Integer, default=0)
    view_profiles = Column(Boolean(), default=False)
    send_interest = Column(Boolean(), default=False)
    recomd_profiles = Column(Boolean(), default=False)
    advced_privacy_optn = Column(Boolean(), default=False)
    send_ultd_prnal_msg = Column(Boolean(), default=False)
    prty_in_srch_rslt = Column(Boolean(), default=False)
    vw_cnts_adrs = Column(Boolean(), default=False)
    prfl_tgd_as_pd_membr = Column(Boolean(), default=False)
    sms_alert = Column(Boolean(), default=False)
    notifications = Column(Boolean(), default=False)
    vw_socl_mda_prfl_urs = Column(Boolean(), default=False)
    persanal_astnce = Column(Boolean(), default=False)
    create_quize = Column(Boolean(), default=False)
    attend_quize = Column(Boolean(), default=False)
    vw_al_phts = Column(Boolean(), default=False)
    rqst_fr_phts = Column(Boolean(), default=False)


class PackageLog(BaseModel):
    __tablename__ = 'matrimony_package_log'
    LOG_STATUS = [
        ('added', 'added'),
        ('updated', 'updated')
    ]
    id = Column(Integer, primary_key=True, autoincrement=True)
    package_id = Column(Integer, ForeignKey('matrimony_packages.id'))
    package = relationship("Packages", backref='logged_package')
    package_name = Column(String(200))
    months = Column(Integer, default=1)
    amount = Column(Integer, default=0)
    is_premium = Column(Boolean(), default=False)
    view_profiles = Column(Boolean(), default=False)
    send_interest = Column(Boolean(), default=False)
    recomd_profiles = Column(Boolean(), default=False)
    advced_privacy_optn = Column(Boolean(), default=False)
    send_ultd_prnal_msg = Column(Boolean(), default=False)
    prty_in_srch_rslt = Column(Boolean(), default=False) # priority package
    vw_cnts_adrs = Column(Boolean(), default=False)
    prfl_tgd_as_pd_membr = Column(Boolean(), default=False)
    sms_alert = Column(Boolean(), default=False)
    notifications = Column(Boolean(), default=False)
    vw_socl_mda_prfl_urs = Column(Boolean(), default=False)
    persanal_astnce = Column(Boolean(), default=False)
    create_quize = Column(Boolean(), default=False)
    attend_quize = Column(Boolean(), default=False)
    vw_al_phts = Column(Boolean(), default=False)
    rqst_fr_phts = Column(Boolean(), default=False)
    log_status = Column(ChoiceType(LOG_STATUS, impl=String()), default='added')


class SuccessStories(BaseModel):
    __tablename__ = 'matrimony_success_stories'
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(256),nullable=True)
    image = Column(String(256),nullable=True)
    cldnry_public_id = Column(String(256), nullable=True)
    desc = Column(String(1000),nullable=True)


class Advertisement(BaseModel):
    __tablename__ = 'matrimony_advertisement'
    AD_TYPE = [
        ('Horizontal', 'Horizontal'),
        ('Vertical', 'Vertical')
    ]
    AD_POSITION = [
        ('Up', 'Up'),
        ('Down', 'Down')
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    ad_name = Column(String(256),nullable=True)
    # type = Column(String(20),nullable=True)
    type = Column(ChoiceType(AD_TYPE, impl=String()))
    position = Column(ChoiceType(AD_POSITION, impl=String()), nullable=True)
    image = Column(String(256),nullable=True)
    cldnry_public_id = Column(String(256), nullable=True)
    url = Column(String(1000),nullable=True)


class Religion(BaseModel):
    __tablename__ = 'matrimony_religion'
    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Integer, nullable=True) #1-hindu, 2-muslim, 3-cristian
    level_one = Column(String(50)) #religion
    level_two = Column(String(50)) #caste/division
    level_three = Column(String(50), nullable=True) #subcaste/caste


class Star(BaseModel):
    __tablename__ = 'matrimony_star'
    id = Column(Integer, primary_key=True, autoincrement=True)
    star_name = Column(String(100))


class UserReligionInfo(BaseModel):
    __tablename__ = 'matrimony_user_religion_info'
    DOSHAM = [
        ('Yes', 'Yes'),
        ('No', 'No'),
        ("Doesn't Know", "Doesn't Know"),
    ]
    id = Column(Integer, primary_key=True, autoincrement=True)
    # dosham = Column(String(50),nullable=True)
    dosham = Column(ChoiceType(DOSHAM, impl=String()), nullable=True)
    raasi = Column(String(50),nullable=True)
    gothram = Column(String(200),nullable=True)
    place_of_birth = Column(String(200),nullable=True)
    time_of_birth = Column(String(50),nullable=True)
    star_id = Column(Integer, ForeignKey('matrimony_star.id'), nullable=True)
    star = relationship("Star", backref='user_star_table')
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("user_religion_details", uselist=False))
    religion_id = Column(Integer, ForeignKey('matrimony_religion.id'), nullable=True)
    religion = relationship("Religion", backref='user_religion_table')


class Occupation(BaseModel):
    __tablename__ = 'matrimony_occupation'
    id = Column(Integer, primary_key=True, autoincrement=True)
    occupation_name = Column(String(100))


class Education(BaseModel):
    __tablename__ = 'matrimony_education'
    id = Column(Integer, primary_key=True, autoincrement=True)
    qualification = Column(String(100))


class AreaOfSpecialization(BaseModel):
    __tablename__ = 'matrimony_area_of_specialization'
    id = Column(Integer, primary_key=True, autoincrement=True)
    area_of_specialization = Column(String(250), nullable=True)
    education_id = Column(Integer, ForeignKey('matrimony_education.id'))
    education = relationship("Education", backref='education_table')


class UserBasicDetails(BaseModel):
    __tablename__ = 'matrimony_user_basic_details'
    PROFILE_FOR = [
        ('Self', 'Self'),
        ('Daughter', 'Daughter'),
        ('Son', 'Son'),
        ('Siblings', 'Siblings'),
        ('Relative', 'Relative'),
        ('Friend', 'Friend'),
    ]
    MOTHER_TONGUE = [
        ('Malayalam', 'Malayalam'),
        ('Hindi', 'Hindi'),
        ('Tamil', 'Tamil'),
        ('Punjabi', 'Punjabi'),
        ('Gujarati', 'Gujarati'),
        ('Bengla', 'Bengla'),
        ('Telugu', 'Telugu'),
        ('marathi', 'Marathi'),
        ('Konkani', 'Konkani'),
        ('Kannada', 'Kannada'),
        ('Nepali', 'Nepali'),
        ('Lushai', 'Lushai'),
        ('Manipuri', 'Manipuri'),
        ('Assamese', 'Assamese'),
        ('Nissi', 'Nissi'),
    ]
    BODY_TYPE = [
        ('Slim', 'Slim'),
        ('Average', 'Average'),
        ('Athletic', 'Athletic'),
        ('Heavy', 'Heavy')
    ]
    PHYSICAL_STATUS = [
        ('Normal', 'Normal'),
        ('Physically Challenged', 'Physically Challenged'),

    ]
    SKIN_COLOR = [
        ('White', 'White'),
        ('Whitish', 'Whitish'),
        ('Brown', 'Brown'),
        ('Dark Brown', 'Dark Brown')
    ]
    MARITAL_STATUS = [
        ('Never Married', 'Never Married'),
        ('Widowed', 'Widowed'),
        ('Divorced', 'Divorced'),
        ('Awaiting Divorce', 'Awaiting Divorce')
    ]
    GET_MARRIED_IN = [
        ('ASAP', 'ASAP'),
        ('In 6 Months', 'In 6 Months'),
        ('In an Year', 'In an Year'),
        ('With in 2 Years', 'With in 2 Years')
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    profile_created_for = Column(ChoiceType(PROFILE_FOR, impl=String()), nullable=True)
    mother_tongue = Column(ChoiceType(MOTHER_TONGUE, impl=String()), default='Malayalam', nullable=True)
    body_type = Column(ChoiceType(BODY_TYPE, impl=String()), nullable=True)
    physical_status = Column(ChoiceType(PHYSICAL_STATUS, impl=String()), default='Normal', nullable=True)
    skin_color = Column(ChoiceType(SKIN_COLOR, impl=String()), nullable=True)
    height_in_cm = Column(Integer, nullable=True)
    height_in_ft = Column(String(10), nullable=True)
    weight = Column(Integer, nullable=True)
    marital_status = Column(ChoiceType(MARITAL_STATUS, impl=String()), nullable=True)
    get_married_in = Column(ChoiceType(GET_MARRIED_IN, impl=String()), nullable=True)
    about_me = Column(String(500), nullable=True)
    have_child = Column(Boolean(), default=False)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("user_basic_details", uselist=False))


class UserLifeStyle(BaseModel):
    __tablename__ = 'matrimony_user_lifestyle'
    EATING = [
        ('Vegetarian', 'Vegetarian'),
        ('Non Vegetarian', 'Non Vegetarian'),
        ('Eggetarian', 'Eggetarian'),
    ]
    SMOKING = [
        ('Smokes regularly','Smokes regularly'),
        ('Smokes rarely','Smokes rarely'),
        ('Never smokes','Never smokes')
    ]
    DRINKING = [
        ('Drinks regularly','Drinks regularly'),
        ('Drinks rarely','Drinks rarely'),
        ('Never Drinks','Never Drinks')
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    smoking_habits = Column(ChoiceType(SMOKING, impl=String()), nullable=True)
    drinking_habits = Column(ChoiceType(DRINKING, impl=String()), nullable=True)
    food_habits = Column(ChoiceType(EATING, impl=String()), nullable=True)
    hobbies = Column(JSONType, nullable=True)
    interests = Column(JSONType, nullable=True)
    favourite_music = Column(JSONType, nullable=True)
    favourite_books = Column(JSONType, nullable=True)
    preffered_movies = Column(JSONType, nullable=True)
    sports_activities = Column(JSONType, nullable=True)
    fitness_activities = Column(JSONType, nullable=True)
    favourite_cuisine = Column(JSONType, nullable=True)
    preferred_dress_style = Column(JSONType, nullable=True)
    spoken_languages = Column(JSONType, nullable=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("user_lifestyle_details", uselist=False))


class UserSocialMediaProfiles(BaseModel):
    __tablename__ = 'matrimony_socialmedia_profile'
    SOCIALMEDIA_PROFILES = [
        ('Facebook', 'Facebook'),
        ('Instagram', 'Instagram'),
        ('Twitter', 'Twitter'),
        ('Youtube Channel', 'Youtube Channel'),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    link = Column(URLType)
    social_media_profile = Column(ChoiceType(SOCIALMEDIA_PROFILES, impl=String()), nullable=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_socialmedia_details')


class UserFamilyDetails(BaseModel):
    __tablename__ = 'matrimony_user_familydetails'
    FAMILY_TYPE = [
        ('Joint', 'Joint'),
        ('Nuclear', 'Nuclear'),
    ]
    FAMILY_VALUES = [
        ('Orthodox', 'Orthodox'),
        ('Traditional', 'Traditional'),
        ('Moderate', 'Moderate'),
        ('Liberal', 'Liberal'),
    ]
    FAMILY_STATUS = [
        ('Middle Class', 'Middle Class'),
        ('Upper Middle Class', 'Upper Middle Class'),
        ('Rich', 'Rich'),
        ('Affluent', 'Affluent'),
    ]
    LIVING_STATUS = [
        ('With Family', 'With Family'),
        ('Seperate', 'Seperate'),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    family_type = Column(ChoiceType(FAMILY_TYPE, impl=String()), nullable=True)
    family_values = Column(ChoiceType(FAMILY_VALUES, impl=String()), nullable=True)
    family_status = Column(ChoiceType(FAMILY_STATUS, impl=String()), nullable=True)
    living_status = Column(ChoiceType(LIVING_STATUS, impl=String()), nullable=True)
    description = Column(String(500),nullable=True)
    father_name = Column(String(50),nullable=True)
    father_occupation_id = Column(Integer, ForeignKey('matrimony_occupation.id'),nullable=True)
    father_occupation = relationship("Occupation", foreign_keys=[father_occupation_id], backref='user_father_occupation')
    mother_name = Column(String(50),nullable=True)
    mother_occupation_id = Column(Integer, ForeignKey('matrimony_occupation.id'),nullable=True)
    mother_occupation = relationship("Occupation", foreign_keys=[mother_occupation_id], backref='user_mother_occupation')
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("user_family_details", uselist=False))


class UserSiblingsDetails(BaseModel):
    __tablename__ = 'matrimony_user_siblings_details'
    SIBLINGS_TYPE = [
        ('Younger', 'Younger'),
        ('Elder', 'Elder'),
    ]
    SIBLINGS_RELATION = [
        ('Brother', 'Brother'),
        ('Sister', 'Sister'),
    ]
    SIBLINGS_MARITAL_STATUS = [
        ('Married', 'Married'),
        ('Not Married', 'Not Married'),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    siblings_type = Column(ChoiceType(SIBLINGS_TYPE, impl=String()), nullable=True)
    relation = Column(ChoiceType(SIBLINGS_RELATION, impl=String()), nullable=True)
    marital_status = Column(ChoiceType(SIBLINGS_MARITAL_STATUS, impl=String()), nullable=True)
    count = Column(Integer, default=1)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_siblings_details')


class UserProfessionalDetails(BaseModel):
    __tablename__ = 'matrimony_user_profession_details'
    EMPLOYED_IN = [
        ('Government', 'Government'),
        ('PSU', 'PSU'),
        ('Private', 'Private'),
        ('Business', 'Business'),
        ('Defence', 'Defence'),
        ('Self-employed', 'Self-employed'),
        ('Not working', 'Not working'),
    ]
    CURRENCY_TYPE = [
        ('INR', 'INR'),
        ('AED', 'AED'),
        ('CAD', 'CAD'),
        ('USD', 'USD'),
        ('EUR', 'EUR'),
        ('GBP', 'GBP'),
        ('CHF', 'CHF'),
        ('ZAR', 'ZAR'),
    ]
    INCOME_CHOICE = [
        ('Less than 20,000','Less than 20,000'),
        ('20,000 to 40,000','20,000 to 40,000'),
        ('40,000 to 65,000','40,000 to 65,000'),
        ('65,000 to 1,00,000','65,000 to 1,00,000'),
        ('1,00,000 to 2,00,000','1,00,000 to 2,00,000'),
        ('2,00,000 to 5,00,000','2,00,000 to 5,00,000'),
        ('5,00,000 to 10,00,000','5,00,000 to 10,00,000'),
        ('10,00,000 to 20,00,000','10,00,000 to 20,00,000'),
        ('20,00,000 to 50,00,000','20,00,000 to 50,00,000'),
        ('50,00,000 to 1,00,00,000','50,00,000 to 1,00,00,000'),
        ('More than 1,00,00,000','More than 1,00,00,000'),
    ]
    RESIDENTIAL_STATUS = [
        ('Permanent Resident', 'Permanent Resident'),
        ('Work Permit', 'Work Permit'),
        ('Student Visa', 'Student Visa'),
        ('Temporary Visa', 'Temporary Visa'),
    ]


    id = Column(Integer, primary_key=True, autoincrement=True)
    highest_education_id = Column(Integer, ForeignKey('matrimony_education.id'), nullable=True)
    highest_education = relationship("Education", backref='user_education_details')
    institutions = Column(String(50), nullable=True)
    area_of_specialization_id = Column(Integer, ForeignKey('matrimony_area_of_specialization.id'))
    area_of_specialization = relationship("AreaOfSpecialization", backref='user_specialized_details')
    employed_in = Column(ChoiceType(EMPLOYED_IN, impl=String()), nullable=True)
    occupation_id = Column(Integer, ForeignKey('matrimony_occupation.id'), nullable=True)
    occupation = relationship("Occupation", backref='user_occupation_details')
    organization = Column(String(50), nullable=True)
    role = Column(String(50), nullable=True)
    currency_type = Column(ChoiceType(CURRENCY_TYPE, impl=String()), nullable=True)
    annual_income = Column(ChoiceType(INCOME_CHOICE, impl=String()), nullable=True)
    annual_income_lower_limit_inr = Column(Float, default=0)
    annual_income_upper_limit_inr = Column(Float, default=0)
    work_location = Column(String(50), nullable=True)
    state = Column(String(50), nullable=True)
    city = Column(String(50), nullable=True)
    citizenship = Column(String(50), nullable=True)
    residential_status = Column(ChoiceType(RESIDENTIAL_STATUS, impl=String()), nullable=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User",backref=backref("user_professional_details", uselist=False))


class UserLocationDetails(BaseModel):
    __tablename__ = 'matrimony_user_location_details'
    id = Column(Integer, primary_key=True, autoincrement=True)
    country = Column(String(50), nullable=True)
    state = Column(String(50), nullable=True)
    city = Column(String(50), nullable=True)
    district = Column(String(50), nullable=True)
    citizenship = Column(String(50), nullable=True)
    ancestral_origin = Column(String(50), nullable=True)
    longitude = Column(String(20), nullable=True)
    latitude = Column(String(20), nullable=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("user_location_details", uselist=False))


class UserImage(BaseModel):
    __tablename__ = 'matrimony_user_image'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_table')
    image = Column(String(256))
    cldnry_public_id = Column(String(256), nullable=True)
    is_main = Column(Boolean(), default=False)
    is_verified = Column(Boolean(), default=False)
    blurred_image = Column(String(256),nullable=True)
    watermarked_image = Column(String(256),nullable=True)
    thumbnail_image = Column(String(256),nullable=True)


class AdditionalImage(BaseModel):
    __tablename__ = 'matrimony_additional_image'
    IMAGE_SECTION = [
        ('family photo','family photo'),
        ('house photo','house photo'),
        ('vehicle photo','vehicle photo'),
        ('office photo','office/workplace and colleagues photo'),
        ('my creations photo','my creations photo'),
        ('my pets photo','my pets photo'),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='additional_image_table')
    image = Column(String(256))
    cldnry_public_id = Column(String(256), nullable=True)
    is_verified = Column(Boolean(), default=False)
    section = Column(ChoiceType(IMAGE_SECTION, impl=String()))
    tag = Column(JSONType, nullable=True)


class Documents(BaseModel):
    __tablename__ = 'matrimony_documents'
    ID_PROOFS = [
        ('Aadhar card','Aadhar card'),
        ('Driving license','Driving license'),
        ('Passport','Passport'),
        ('Voter id','Voter id'),
        ('Pan card','Pan card'),
    ]
    DOCS_STATUS = [
        ('pending','pending'),
        ('approved','approved'),
        ('rejected','rejected'),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='documents_table')
    document = Column(String(256))
    doc_type = Column(ChoiceType(ID_PROOFS, impl=String()))
    status = Column(ChoiceType(DOCS_STATUS, impl=String()), default='pending')
    cldnry_public_id = Column(String(256), nullable=True)


class UserProfileQuestions(BaseModel):
    __tablename__ = 'matrimony_user_profile_questions'

    id = Column(Integer, primary_key=True, autoincrement=True)
    question = Column(String(500), nullable=True)
    answer = Column(String(500), nullable=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_profile_questions')


class UserPrivacyOption(BaseModel):
    __tablename__ = 'matrimony_user_privacy_option'
    PRIVACY_FOR = [
        ('photo','photo'),
        ('phone_number','phone number'),
    ]
    PRIVACY_OPTIONS = [
        ('Everyone','Everyone'),
        ('Verified account only','Verified account only'),
        ('By request only','By request only'),
    ]
    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(ChoiceType(PRIVACY_FOR, impl=String()))
    privacy_option = Column(ChoiceType(PRIVACY_OPTIONS, impl=String()))
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_privacy_option')


class BasicPartnerPreferences(BaseModel):
    __tablename__ = 'matrimony_basic_preference'
    MARITAL_STATUS = [
        ('Any','Any'),
        ('Never married','Never married'),
        ('Widowed','Widowed'),
        ('Divorced','Divorced'),
        ('Awaiting divorce','Awaiting divorce'),
    ]
    SMOKING_CHOICE = [
        ("Doesn't matter","Doesn't matter"),
        ('Smokes regularly','Smokes regularly'),
        ('Smokes rarely','Smokes rarely'),
        ('Never smokes','Never smokes')
    ]
    DRINKING_CHOICE = [
        ("Doesn't matter","Doesn't matter"),
        ('Drinks regularly','Drinks regularly'),
        ('Drinks rarely','Drinks rarely'),
        ('Never Drinks','Never Drinks')
    ]
    DISABILITY_CHOICE = [
        ('Any','Any'),
        ('Normal','Normal'),
        ('Physically Challenged','Physically Challenged')
    ]
    CHILD_CHOICE = [
        ("Doesn't matter","Doesn't matter"),
        ('Yes','Yes'),
        ('No','No')
    ]
    EATING = [
        ('Any','Any'),
        ('Vegetarian', 'Vegetarian'),
        ('Non Vegetarian', 'Non Vegetarian'),
        ('Eggetarian', 'Eggetarian'),
    ]
    MOTHER_TONGUE = [
        ('Any','Any'),
        ('Malayalam', 'Malayalam'),
        ('Hindi', 'Hindi'),
        ('Tamil', 'Tamil'),
        ('Punjabi', 'Punjabi'),
        ('Gujarati', 'Gujarati'),
        ('Bengla', 'Bengla'),
        ('Telugu', 'Telugu'),
        ('marathi', 'Marathi'),
        ('Konkani', 'Konkani'),
        ('Kannada', 'Kannada'),
        ('Nepali', 'Nepali'),
        ('Lushai', 'Lushai'),
        ('Manipuri', 'Manipuri'),
        ('Assamese', 'Assamese'),
        ('Nissi', 'Nissi'),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("basic_preferences_table", uselist=False))
    gender = Column(String(15), nullable=True)
    age_upperLimit = Column(Integer, nullable=True)
    age_lowerLimit = Column(Integer, nullable=True)
    age_strict = Column(Boolean(), default=False)
    height_upperLimit = Column(Integer, nullable=True)
    height_lowerLimit = Column(Integer, nullable=True)
    height_strict = Column(Boolean(), default=False)
    # physical_status = Column(ChoiceType(DISABILITY_CHOICE, impl=String()), nullable=True)
    physical_status = Column(JSON, nullable=True)
    physical_status_strict = Column(Boolean(), default=False)
    # marital_status = Column(ChoiceType(MARITAL_STATUS, impl=String()), nullable=True)
    marital_status = Column(JSON, nullable=True)
    marital_status_strict = Column(Boolean(), default=False)
    # having_child = Column(ChoiceType(CHILD_CHOICE, impl=String()), nullable=True)
    having_child = Column(JSON, nullable=True)
    having_child_strict = Column(Boolean(), default=False)
    eating_habit = Column(JSON, nullable=True)
    eating_habit_strict = Column(Boolean(), default=False)
    # smoking_habit = Column(ChoiceType(SMOKING_CHOICE, impl=String()), nullable=True)
    smoking_habit = Column(JSON, nullable=True)
    smoking_habit_strict = Column(Boolean(), default=False)
    # drinking_habit = Column(ChoiceType(DRINKING_CHOICE, impl=String()), nullable=True)
    drinking_habit = Column(JSON, nullable=True)
    drinking_habit_strict = Column(Boolean(), default=False)
    mother_tounge = Column(JSON, nullable=True)
    mother_tounge_strict = Column(Boolean(), default=False)


class ProffesionalPartnerPreferences(BaseModel):
    __tablename__ = 'matrimony_proffesional_preference'
    PREFERENCE_INCOME_CHOICE = [
        ("Doesn't matter","Doesn't matter"),
        ('Less than 20,000','Less than 20,000'),
        ('20,000 to 40,000','20,000 to 40,000'),
        ('40,000 to 65,000','40,000 to 65,000'),
        ('65,000 to 1,00,000','65,000 to 1,00,000'),
        ('1,00,000 to 2,00,000','1,00,000 to 2,00,000'),
        ('2,00,000 to 5,00,000','2,00,000 to 5,00,000'),
        ('5,00,000 to 10,00,000','5,00,000 to 10,00,000'),
        ('10,00,000 to 20,00,000','10,00,000 to 20,00,000'),
        ('20,00,000 to 50,00,000','20,00,000 to 50,00,000'),
        ('50,00,000 to 1,00,00,000','50,00,000 to 1,00,00,000'),
        ('More than 1,00,00,000','More than 1,00,00,000'),
    ]
    EMPLOYED_IN = [
        ('Any', 'Any'),
        ('Government', 'Government'),
        ('PSU', 'PSU'),
        ('Private', 'Private'),
        ('Business', 'Business'),
        ('Defence', 'Defence'),
        ('Self-employed', 'Self-employed'),
        ('Not working', 'Not working'),
    ]
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("proffesional_preferences_table", uselist=False))
    education = Column(JSONType, nullable=True)
    education_strict = Column(Boolean(), default=False)
    employed_in = Column(JSONType, nullable=True)
    employed_in_strict = Column(Boolean(), default=False)
    occupation = Column(JSONType, nullable=True)
    occupation_strict = Column(Boolean(), default=False)
    annual_income = Column(ChoiceType(PREFERENCE_INCOME_CHOICE, impl=String()), nullable=True)
    # annual_income = Column(JSONType, nullable=True)
    annual_income_lower_limit_inr = Column(Float, default=0)
    annual_income_upper_limit_inr = Column(Float, default=0)
    annual_income_strict = Column(Boolean(), default=False)
    income_currency = Column(String(50), nullable=True)


class LocationPartnerPreferences(BaseModel):
    __tablename__ = 'matrimony_location_preference'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("location_preferences_table", uselist=False))
    country = Column(JSON, nullable=True)
    country_strict = Column(Boolean(), default=False)
    residing_city = Column(JSON, nullable=True)
    residing_city_strict = Column(Boolean(), default=False)
    residing_state = Column(JSON, nullable=True)
    residing_state_strict = Column(Boolean(), default=False)
    citizenship  = Column(JSON, nullable=True)
    citizenship_strict = Column(Boolean(), default=False)


class ReligiousPartnerPreferences(BaseModel):
    __tablename__ = 'matrimony_religious_preference'
    DOSHAM_PREF = [
        ('Yes', 'Yes'),
        ('No', 'No'),
        ("Doesn't matter", "Doesn't matter"),
    ]
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'), unique=True)
    user = relationship("User", backref=backref("religious_preferences_table", uselist=False))
    level_one = Column(String(50), nullable=True)
    level_one_strict = Column(Boolean(), default=False)
    level_two = Column(JSONType, nullable=True)
    level_two_strict = Column(Boolean(), default=False)
    level_three = Column(JSONType, nullable=True)
    level_three_strict = Column(Boolean(), default=False)
    star = Column(JSONType, nullable=True)
    star_strict = Column(Boolean(), default=False)
    # dosham = Column(JSONType, nullable=True)
    dosham = Column(ChoiceType(DOSHAM_PREF, impl=String()), nullable=True)
    dosham_strict = Column(Boolean(), default=False)


class SubscribePackage(BaseModel):
    __tablename__ = 'matrimony_subscribe_package'
    SUBSCRIPTION_STATUS = [
        ('new', 'new'),
        ('completed', 'completed'),
        ('failed', 'failed'),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    package_log_id = Column(Integer, ForeignKey('matrimony_package_log.id'))
    package_log = relationship("PackageLog", backref='package_log_subscribed')
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_subscribed')
    payment_id = Column(String(100))
    expiry_date = Column(Date(), nullable=True)
    subscription_status = Column(ChoiceType(SUBSCRIPTION_STATUS, impl=String()), nullable=True)


class Payment(Base):
    __tablename__ = 'matrimony_payment'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_payments')
    payment_id = Column(String(100))
    payment_method = Column(String(100))
    amount_paid = Column(Float)
    subscribed_package_id = Column(Integer, ForeignKey('matrimony_subscribe_package.id'))
    subscribed_package = relationship("SubscribePackage", backref='subscribe_payments')
    status = Column(String(100))


class QuizQuestions(BaseModel):
    __tablename__ = 'matrimony_quiz_questions'
    QUESTION_TYPE = [
        ('multiple choice', 'multiple choice'),
        ('rate', 'rate'),
        ('open ended', 'open ended'),
    ]
    QUIZ_CREATED_BY = [
        ('user', 'user'),
        ('admin', 'admin'),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_created_quiz_questions')
    created_by = Column(ChoiceType(QUIZ_CREATED_BY, impl=String()), default='admin')
    question_type = Column(ChoiceType(QUESTION_TYPE, impl=String()))
    question = Column(String(500))
    is_verified = Column(Boolean(), default=False)


class QuizQuestionsLog(BaseModel):
    __tablename__ = 'matrimony_quiz_questions_log'

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_created_quiz_questions_log')
    quiz_question_id = Column(Integer, ForeignKey('matrimony_quiz_questions.id'))
    quiz_question = relationship("QuizQuestions", backref='quiz_question_logs')
    created_by = Column(String(50), nullable=True)
    question_type = Column(String(50), nullable=True)
    question = Column(String(500))
    is_verified = Column(Boolean(), default=False)


class QuizQuestionChoices(BaseModel):
    __tablename__ = 'matrimony_quiz_question_choices'

    id = Column(Integer, primary_key=True, autoincrement=True)
    choice = Column(String(500))
    question_id = Column(Integer, ForeignKey('matrimony_quiz_questions.id'))
    question = relationship("QuizQuestions", backref='quiz_question_choices')


class QuizQuestionChoicesLog(BaseModel):
    __tablename__ = 'matrimony_quiz_question_choices_log'

    id = Column(Integer, primary_key=True, autoincrement=True)
    choice = Column(String(500))
    question_log_id = Column(Integer, ForeignKey('matrimony_quiz_questions_log.id'))
    question_log = relationship("QuizQuestionsLog", backref='quiz_question_choices_log')


class UserQuizQuestions(BaseModel):
    __tablename__ = 'matrimony_user_quiz_questions'

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_quiz_questions')
    question_id = Column(Integer, ForeignKey('matrimony_quiz_questions.id'))
    question = relationship("QuizQuestions", backref='quiz_questions_users')


class UserQuizAnswers(BaseModel):
    __tablename__ = 'matrimony_user_quiz_answers'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_attended_quiz')
    question_id = Column(Integer, ForeignKey('matrimony_user_quiz_questions.id'))
    question = relationship("UserQuizQuestions", backref='question_user_answers')
    question_log_id = Column(Integer, ForeignKey('matrimony_quiz_questions_log.id'))
    question_log = relationship("QuizQuestionsLog", backref='quiz_question_log_user_questions')
    answer = Column(String(500))

APPROVAL_STATUS = [
    ('pending', 'pending'),
    ('accepted', 'accepted'),
    ('rejected', 'rejected'),
]


class PartnerInterests(BaseModel):
    __tablename__ = 'matrimony_partner_interests'

    id = Column(Integer, primary_key=True, autoincrement=True)
    from_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    from_user = relationship("User", foreign_keys=[from_user_id], backref='user_sended_interests')
    to_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    to_user = relationship("User", foreign_keys=[to_user_id], backref='user_received_interests')
    status = Column(ChoiceType(APPROVAL_STATUS, impl=String()), default='pending')
    is_viewed = Column(Boolean(), default=False)


class ProfileVisits(BaseModel):
    __tablename__ = 'matrimony_profile_visits'
    id = Column(Integer, primary_key=True, autoincrement=True)
    from_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    from_user = relationship("User", foreign_keys=[from_user_id], backref='user_visited_profile')
    to_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    to_user = relationship("User", foreign_keys=[to_user_id], backref='profile_visitors')
    visit_count = Column(Integer, default=1)
    is_viewed = Column(Boolean(), default=False)


class ProfileShortlist(BaseModel):
    __tablename__ = 'matrimony_profile_shortlist'
    id = Column(Integer, primary_key=True, autoincrement=True)
    from_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    from_user = relationship("User", foreign_keys=[from_user_id], backref='user_shortlisted_profile')
    to_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    to_user = relationship("User", foreign_keys=[to_user_id], backref='profile_shortlisted_users')
    is_viewed = Column(Boolean(), default=False)


class DailyRecommendations(BaseModel):
    __tablename__ = 'matrimony_daily_recommendations'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='test_db')
    user_list = Column(String(500))
    date = Column(Date())


class PhotoRequests(BaseModel):
    __tablename__ = 'matrimony_photo_requests'
    id = Column(Integer, primary_key=True, autoincrement=True)
    from_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    from_user = relationship("User", foreign_keys=[from_user_id], backref='user_requested_photos')
    to_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    to_user = relationship("User", foreign_keys=[to_user_id], backref='user_received_photo_requests')
    status = Column(ChoiceType(APPROVAL_STATUS, impl=String()), default='pending')
    is_viewed = Column(Boolean(), default=False)


class NumberRequests(BaseModel):
    __tablename__ = 'matrimony_number_requests'
    id = Column(Integer, primary_key=True, autoincrement=True)
    from_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    from_user = relationship("User", foreign_keys=[from_user_id], backref='user_requested_numbers')
    to_user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    to_user = relationship("User", foreign_keys=[to_user_id], backref='user_received_number_requests')
    status = Column(ChoiceType(APPROVAL_STATUS, impl=String()), default='pending')
    is_viewed = Column(Boolean(), default=False)


class PartnerInterestReminder(BaseModel):
    __tablename__ = 'matrimony_partner_interest_reminder'
    id = Column(Integer, primary_key=True, autoincrement=True)
    partner_interest_id = Column(Integer, ForeignKey('matrimony_partner_interests.id'))
    partner_interest = relationship("PartnerInterests", backref='partner_interest_reminder')
    is_viewed = Column(Boolean(), default=False)


class UserDeactivation(BaseModel):
    __tablename__ = 'matrimony_user_deactivation'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('matrimony_user.id'))
    user = relationship("User", backref='user_deactivation')
    reason = Column(String(500))
