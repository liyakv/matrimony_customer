from sqlalchemy.orm import Session
from app.crud.profile.profile import user_registration_crud
from app.models.model import *
from app.schemas.preferences.preference import *
from app.crud.home.home import get_gender_preference,active_users,gender_pref_list
import collections
import operator
from unittest import result



# def get_user_basic_preference_details(db: Session, bsc_pref_id: int):
#     return db.query(BasicPartnerPreferences).get(bsc_pref_id)

# def get_user_professional_preference_details(db: Session, prof_pref_id: int):
#     return db.query(ProffesionalPartnerPreferences).get(prof_pref_id)

# def get_user_location_preference_details(db: Session, loc_pref_id: int):
#     return db.query(LocationPartnerPreferences).get(loc_pref_id)

# def get_user_religion_preference_details(db: Session, rel_pref_id: int):
#     return db.query(ReligiousPartnerPreferences).get(rel_pref_id)


# def add_user_basic_preferenes(db: Session, basic_partner_preferenes: PartnerPreferencesBasicDetails, user_id: int):
#     if basic_partner_preferenes.id is not None:
#         cur_bsc_pref = get_user_basic_preference_details(db, basic_partner_preferenes.id)
#         cur_bsc_pref.gender = basic_partner_preferenes.gender
#         cur_bsc_pref.age_upperLimit = basic_partner_preferenes.age_upperLimit
#         cur_bsc_pref.age_lowerLimit = basic_partner_preferenes.age_lowerLimit
#         cur_bsc_pref.age_strict = basic_partner_preferenes.age_strict
#         cur_bsc_pref.height_upperLimit = basic_partner_preferenes.height_upperLimit
#         cur_bsc_pref.height_lowerLimit = basic_partner_preferenes.height_lowerLimit
#         cur_bsc_pref.height_strict = basic_partner_preferenes.height_strict
#         cur_bsc_pref.physical_status = basic_partner_preferenes.physical_status
#         cur_bsc_pref.physical_status_strict = basic_partner_preferenes.physical_status_strict
#         cur_bsc_pref.marital_status = basic_partner_preferenes.marital_status
#         cur_bsc_pref.marital_status_strict = basic_partner_preferenes.marital_status_strict
#         cur_bsc_pref.having_child = basic_partner_preferenes.having_child
#         cur_bsc_pref.having_child_strict = basic_partner_preferenes.having_child_strict
#         cur_bsc_pref.eating_habit = basic_partner_preferenes.eating_habit
#         cur_bsc_pref.eating_habit_strict = basic_partner_preferenes.eating_habit_strict
#         cur_bsc_pref.smoking_habit = basic_partner_preferenes.smoking_habit
#         cur_bsc_pref.smoking_habit_strict = basic_partner_preferenes.smoking_habit_strict
#         cur_bsc_pref.drinking_habit = basic_partner_preferenes.drinking_habit
#         cur_bsc_pref.drinking_habit_strict = basic_partner_preferenes.drinking_habit_strict
#         cur_bsc_pref.mother_tounge = basic_partner_preferenes.mother_tounge
#         cur_bsc_pref.mother_tounge_strict = basic_partner_preferenes.mother_tounge_strict
#         db.commit()
#     else:
#         basic_preferences  = BasicPartnerPreferences(gender = basic_partner_preferenes.gender, age_upperLimit = basic_partner_preferenes.age_upperLimit,
#                         age_lowerLimit = basic_partner_preferenes.age_lowerLimit, age_strict = basic_partner_preferenes.age_strict,
#                         height_upperLimit = basic_partner_preferenes.height_upperLimit, height_lowerLimit = basic_partner_preferenes.height_lowerLimit,
#                         height_strict = basic_partner_preferenes.height_strict, physical_status = basic_partner_preferenes.physical_status,
#                         physical_status_strict = basic_partner_preferenes.physical_status_strict, marital_status = basic_partner_preferenes.marital_status,
#                         marital_status_strict = basic_partner_preferenes.marital_status_strict, having_child = basic_partner_preferenes.having_child,
#                         having_child_strict = basic_partner_preferenes.having_child_strict, eating_habit = basic_partner_preferenes.eating_habit,
#                         eating_habit_strict = basic_partner_preferenes.eating_habit_strict, smoking_habit = basic_partner_preferenes.smoking_habit,
#                          smoking_habit_strict = basic_partner_preferenes.smoking_habit_strict, drinking_habit = basic_partner_preferenes.drinking_habit,
#                         drinking_habit_strict = basic_partner_preferenes.drinking_habit_strict, mother_tounge = basic_partner_preferenes.mother_tounge,
#                          mother_tounge_strict = basic_partner_preferenes.mother_tounge_strict,
#                         user_id = user_id)
#         db.add(basic_preferences)
#         db.commit()
#         db.refresh(basic_preferences)

#     # proffesional_preferences = ProffesionalPartnerPreferences(education = preference_schema.proffesional_preference.education, education_strict = preference_schema.proffesional_preference.education_strict, employed_in = preference_schema.proffesional_preference.employed_in,
#     #                 employed_in_strict = preference_schema.proffesional_preference.employed_in_strict, occupation = preference_schema.proffesional_preference.occupation, occupation_strict = preference_schema.proffesional_preference.occupation_strict,
#     #                 annual_income = preference_schema.proffesional_preference.annual_income, annual_income_lower_limit_inr = preference_schema.proffesional_preference.annual_income_lower_limit_inr, annual_income_upper_limit_inr = preference_schema.proffesional_preference.annual_income_upper_limit_inr,
#     #                 annual_income_strict = preference_schema.proffesional_preference.annual_income_strict, income_currency = preference_schema.proffesional_preference.income_currency,
#     #                 user_id = user_id)
#     # db.add(proffesional_preferences)
#     # db.commit()
#     # db.refresh(proffesional_preferences)


# ///
def get_user_basic_preference_details(db: Session, id: int):
    return db.query(BasicPartnerPreferences).filter_by(user_id=id).first()


def add_user_basic_preferenes(db: Session, basic_partner_preferenes: PartnerPreferencesBasicDetails, user_id: int):
    cur_bsc_pref = get_user_basic_preference_details(db, id=user_id)
    if basic_partner_preferenes.gender is not None:
        cur_bsc_pref.gender = basic_partner_preferenes.gender
    if basic_partner_preferenes.age_upperLimit is not None:
        cur_bsc_pref.age_upperLimit = basic_partner_preferenes.age_upperLimit
    if basic_partner_preferenes.age_lowerLimit is not None:
        cur_bsc_pref.age_lowerLimit = basic_partner_preferenes.age_lowerLimit
    if basic_partner_preferenes.age_strict is not None:
        cur_bsc_pref.age_strict = basic_partner_preferenes.age_strict
    if basic_partner_preferenes.height_upperLimit is not None:
        cur_bsc_pref.height_upperLimit = basic_partner_preferenes.height_upperLimit
    if basic_partner_preferenes.height_lowerLimit is not None:
        cur_bsc_pref.height_lowerLimit = basic_partner_preferenes.height_lowerLimit
    if basic_partner_preferenes.height_strict is not None:
        cur_bsc_pref.height_strict = basic_partner_preferenes.height_strict
    if basic_partner_preferenes.physical_status is not None:
        if len(basic_partner_preferenes.physical_status["physical_status"]) > 0:
            cur_bsc_pref.physical_status = basic_partner_preferenes.physical_status
        else:
            cur_bsc_pref.physical_status = {"physical_status":["Any"]}
    if basic_partner_preferenes.physical_status_strict is not None:
        cur_bsc_pref.physical_status_strict = basic_partner_preferenes.physical_status_strict
    if basic_partner_preferenes.marital_status is not None:
        if len(basic_partner_preferenes.marital_status["marital_status"]) > 0:
            cur_bsc_pref.marital_status = basic_partner_preferenes.marital_status
        else:
            cur_bsc_pref.marital_status = {"marital_status":["Any"]}
    if basic_partner_preferenes.marital_status_strict is not None:
        cur_bsc_pref.marital_status_strict = basic_partner_preferenes.marital_status_strict
    if basic_partner_preferenes.having_child is not None:
        if len(basic_partner_preferenes.having_child["having_child"]) > 0:
            cur_bsc_pref.having_child = basic_partner_preferenes.having_child
        else:
            cur_bsc_pref.having_child = {"having_child":["Doesn't matter"]}
    if basic_partner_preferenes.having_child_strict is not None:
        cur_bsc_pref.having_child_strict = basic_partner_preferenes.having_child_strict
    if basic_partner_preferenes.eating_habit is not None:
        if len(basic_partner_preferenes.eating_habit["eating_habit"]) > 0:
            cur_bsc_pref.eating_habit = basic_partner_preferenes.eating_habit
        else:
            cur_bsc_pref.eating_habit = {"eating_habit":["Any"]}
    if basic_partner_preferenes.eating_habit_strict is not None:
        cur_bsc_pref.eating_habit_strict = basic_partner_preferenes.eating_habit_strict
    if basic_partner_preferenes.smoking_habit is not None:
        if len(basic_partner_preferenes.smoking_habit["smoking_habit"]) > 0:
            cur_bsc_pref.smoking_habit = basic_partner_preferenes.smoking_habit
        else:
            cur_bsc_pref.smoking_habit = {"smoking_habit":["Doesn't matter"]}
    if basic_partner_preferenes.smoking_habit_strict is not None:
        cur_bsc_pref.smoking_habit_strict = basic_partner_preferenes.smoking_habit_strict
    if basic_partner_preferenes.drinking_habit is not None:
        if len(basic_partner_preferenes.drinking_habit["drinking_habit"]) > 0:
            cur_bsc_pref.drinking_habit = basic_partner_preferenes.drinking_habit
        else:
            cur_bsc_pref.drinking_habit = {"drinking_habit":["Doesn't matter"]}
    if basic_partner_preferenes.drinking_habit_strict is not None:
        cur_bsc_pref.drinking_habit_strict = basic_partner_preferenes.drinking_habit_strict
    if basic_partner_preferenes.mother_tounge is not None:
        if len(basic_partner_preferenes.mother_tounge["mother_tounge"]) > 0:
            cur_bsc_pref.mother_tounge = basic_partner_preferenes.mother_tounge
        else:
            cur_bsc_pref.mother_tounge = {"mother_tounge":["Any"]}
    if basic_partner_preferenes.mother_tounge_strict is not None:
        cur_bsc_pref.mother_tounge_strict = basic_partner_preferenes.mother_tounge_strict
# if None ******
    # if basic_partner_preferenes.physical_status is None:
    #     cur_bsc_pref.physical_status = {"physical_status":["Any"]}
    # if basic_partner_preferenes.marital_status is None:
    #     cur_bsc_pref.marital_status = {"marital_status":["Any"]}
    # if basic_partner_preferenes.having_child is None:
    #     cur_bsc_pref.having_child = {"having_child":["Doesn't matter"]}   
    # if basic_partner_preferenes.eating_habit is None:
    #     cur_bsc_pref.eating_habit = {"eating_habit":["Any"]}
    # if basic_partner_preferenes.smoking_habit is None:
    #     cur_bsc_pref.smoking_habit = {"smoking_habit":["Doesn't matter"]}
    # if basic_partner_preferenes.drinking_habit is None:
    #     cur_bsc_pref.drinking_habit = {"drinking_habit":["Doesn't matter"]}
    # if basic_partner_preferenes.mother_tounge is None:
    #     cur_bsc_pref.mother_tounge = {"mother_tounge":["Any"]}
    db.commit()
    


def get_user_professional_preference_details(db: Session, id: int):
    return db.query(ProffesionalPartnerPreferences).filter_by(user_id=id).first()


def add_user_proffesion_preferenes(db: Session, proffesional_preferences: ProffesionalPreferencesDetails, user_id: int):
    cur_prof_pref = get_user_professional_preference_details(db, id=user_id)
    if proffesional_preferences.education is not None:
        if len(proffesional_preferences.education["education"]) > 0:
            cur_prof_pref.education = proffesional_preferences.education
        else:
            cur_prof_pref.education = {"education":["Any"]}
    if proffesional_preferences.education_strict is not None:
        cur_prof_pref.education_strict = proffesional_preferences.education_strict
    if proffesional_preferences.employed_in is not None:
        if len(proffesional_preferences.employed_in["employed_in"]) > 0:
            cur_prof_pref.employed_in = proffesional_preferences.employed_in
        else:
            cur_prof_pref.employed_in = {"employed_in":["Any"]}
    if proffesional_preferences.employed_in_strict is not None:
        cur_prof_pref.employed_in_strict = proffesional_preferences.employed_in_strict
    if proffesional_preferences.occupation is not None:
        if len(proffesional_preferences.occupation["occupation"]) > 0:
            cur_prof_pref.occupation = proffesional_preferences.occupation
        else:
            cur_prof_pref.occupation = {"occupation":["Any"]}      
    if proffesional_preferences.occupation_strict is not None:
        cur_prof_pref.occupation_strict = proffesional_preferences.occupation_strict
    if proffesional_preferences.annual_income is not None:
        cur_prof_pref.annual_income = proffesional_preferences.annual_income
    if proffesional_preferences.annual_income_strict is not None:
        cur_prof_pref.annual_income_strict = proffesional_preferences.annual_income_strict
    if proffesional_preferences.income_currency is not None:
        cur_prof_pref.income_currency = proffesional_preferences.income_currency
# if None ******
    # if proffesional_preferences.education is None:
    #     cur_prof_pref.education = {"education":["Any"]}
    # if proffesional_preferences.employed_in is None:
    #     cur_prof_pref.employed_in = {"employed_in":["Any"]}
    # if proffesional_preferences.occupation is None:
    #     cur_prof_pref.occupation = {"occupation":["Any"]}
    if proffesional_preferences.annual_income is None:
        cur_prof_pref.annual_income = "Doesn't matter"
    if proffesional_preferences.income_currency is None:
        cur_prof_pref.income_currency = "INR"
    db.commit()


def get_user_location_preference_details(db: Session, id: int):
    return db.query(LocationPartnerPreferences).filter_by(user_id=id).first()


def add_user_location_preferenes(db: Session, location_preferences: LocationPartnerPreferencesDetails, user_id: int):
    cur_loc_pref = get_user_location_preference_details(db, id=user_id)
    if location_preferences.country is not None:
        if len(location_preferences.country["country"]) > 0:
            cur_loc_pref.country = location_preferences.country
        else:
            cur_loc_pref.country = {"country":["Any"]}  
    if location_preferences.country_strict is not None:
        cur_loc_pref.country_strict = location_preferences.country_strict
    if location_preferences.residing_city is not None:
        if len(location_preferences.residing_city["residing_city"]) > 0:
            cur_loc_pref.residing_city = location_preferences.residing_city
        else:
            cur_loc_pref.residing_city =  {"residing_city":["Any"]}
    if location_preferences.residing_city_strict is not None:
        cur_loc_pref.residing_city_strict = location_preferences.residing_city_strict
    if location_preferences.residing_state is not None:
        if len(location_preferences.residing_state["residing_state"]) > 0:
            cur_loc_pref.residing_state = location_preferences.residing_state
        else:
            cur_loc_pref.residing_state = {"residing_state":["Any"]}
    if location_preferences.residing_state_strict is not None:
        cur_loc_pref.residing_state_strict = location_preferences.residing_state_strict
    if location_preferences.citizenship is not None:
        if len(location_preferences.citizenship["citizenship"]) > 0:
            cur_loc_pref.citizenship = location_preferences.citizenship
        else:
            cur_loc_pref.citizenship =  {"citizenship":["Any"]}
    if location_preferences.citizenship_strict is not None:
        cur_loc_pref.citizenship_strict = location_preferences.citizenship_strict   
# if None ******
    # if location_preferences.country is None:
    #     cur_loc_pref.country = {"country":["Any"]}
    # if location_preferences.residing_city is None:
    #     cur_loc_pref.residing_city =  {"residing_city":["Any"]}
    # if location_preferences.residing_state is None:
    #     cur_loc_pref.residing_state = {"residing_state":["Any"]}
    # if location_preferences.citizenship is None:
    #     cur_loc_pref.citizenship = {"citizenship":["Any"]}
    db.commit()


def get_user_religion_preference_details(db: Session, id: int):
    return db.query(ReligiousPartnerPreferences).filter_by(user_id=id).first()


def add_user_religion_preferenes(db: Session, religion_preferences: ReligiousPartnerPreferencesDetails, user_id: int):
    cur_rel_pref = get_user_religion_preference_details(db, id=user_id)
    if religion_preferences.level_one is not None:
        cur_rel_pref.level_one = religion_preferences.level_one
    if religion_preferences.level_one_strict is not None:
        cur_rel_pref.level_one_strict = religion_preferences.level_one_strict
    if religion_preferences.level_two is not None:
        if len(religion_preferences.level_two["level_two"]) > 0:
            cur_rel_pref.level_two = religion_preferences.level_two
        else:
            cur_rel_pref.level_two = {"level_two":["Any"]}
    if religion_preferences.level_two_strict is not None:
        cur_rel_pref.level_two_strict = religion_preferences.level_two_strict
    if religion_preferences.level_three is not None:
        if len(religion_preferences.level_three["level_three"]) > 0:
            cur_rel_pref.level_three = religion_preferences.level_three
        else:
            cur_rel_pref.level_three = {"level_three":["Any"]}     
    if religion_preferences.level_three_strict is not None:
        cur_rel_pref.level_three_strict = religion_preferences.level_three_strict
    if religion_preferences.star is not None:
        if len(religion_preferences.star["star"]) > 0:
            cur_rel_pref.star = religion_preferences.star
        else:
            cur_rel_pref.star = {"star":["Any"]}   
    if religion_preferences.star_strict is not None:
        cur_rel_pref.star_strict = religion_preferences.star_strict
    if religion_preferences.dosham is not None:
        cur_rel_pref.dosham = religion_preferences.dosham
    if religion_preferences.dosham_strict is not None:
        cur_rel_pref.dosham_strict = religion_preferences.dosham_strict
# if None ******
    # if religion_preferences.level_two is None:
    #     cur_rel_pref.level_two = {"level_two":["Any"]}
    # if religion_preferences.level_three is None:
    #     cur_rel_pref.level_three = {"level_three":["Any"]}   
    # if religion_preferences.star is None:
    #     cur_rel_pref.star = {"star":["Any"]}
    if religion_preferences.dosham is None:
        cur_rel_pref.dosham = "Doesn't matter"
    db.commit()


# ///

# pref_edit_datas***
def get_user_by_id(db: Session, id: int):
    return db.query(User).get(id)
    

def get_pref_edit_data(db: Session, user_id: int):
    user = get_user_by_id(db, user_id)
    profile=db.query(User).filter(User.id==user_id).first()
    resp = []
    if profile.basic_preferences_table == None:
        gender,age_upperLimit,age_lowerLimit,age_strict,height_upperLimit,height_lowerLimit,height_strict,physical_status,physical_status_strict,marital_status, marital_status_strict,having_child,having_child_strict,eating_habit,eating_habit_strict, smoking_habit,smoking_habit_strict,drinking_habit,drinking_habit_strict, mother_tounge,mother_tounge_strict = None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None
    else:
        gender=profile.basic_preferences_table.gender
        age_upperLimit=profile.basic_preferences_table.age_upperLimit
        age_lowerLimit=profile.basic_preferences_table.age_lowerLimit
        age_strict=profile.basic_preferences_table.age_strict
        height_upperLimit=profile.basic_preferences_table.height_upperLimit
        height_lowerLimit=profile.basic_preferences_table.height_lowerLimit
        height_strict=profile.basic_preferences_table.height_strict
        physical_status=profile.basic_preferences_table.physical_status
        physical_status_strict=profile.basic_preferences_table.physical_status_strict
        marital_status=profile.basic_preferences_table.marital_status
        marital_status_strict=profile.basic_preferences_table.marital_status_strict
        having_child=profile.basic_preferences_table.having_child
        having_child_strict=profile.basic_preferences_table.having_child_strict
        eating_habit=profile.basic_preferences_table.eating_habit
        eating_habit_strict=profile.basic_preferences_table.eating_habit_strict
        smoking_habit=profile.basic_preferences_table.smoking_habit
        smoking_habit_strict=profile.basic_preferences_table.smoking_habit_strict
        drinking_habit=profile.basic_preferences_table.drinking_habit
        drinking_habit_strict=profile.basic_preferences_table.drinking_habit_strict
        mother_tounge=profile.basic_preferences_table.mother_tounge
        mother_tounge_strict=profile.basic_preferences_table.mother_tounge_strict


    if profile.proffesional_preferences_table==None:
        education,education_strict,employed_in,employed_in_strict,occupation,occupation_strict,annual_income,annual_income_strict,income_currency=None,None,None,None,None,None,None,None,None
    else:
        education= profile.proffesional_preferences_table.education
        education_strict=profile.proffesional_preferences_table.education_strict
        employed_in= profile.proffesional_preferences_table.employed_in
        employed_in_strict= profile.proffesional_preferences_table.employed_in_strict
        occupation=profile.proffesional_preferences_table.occupation
        occupation_strict=profile.proffesional_preferences_table.occupation_strict
        annual_income=profile.proffesional_preferences_table.annual_income
        annual_income_strict=profile.proffesional_preferences_table.annual_income_strict
        income_currency=profile.proffesional_preferences_table.income_currency


    if profile.location_preferences_table==None:
        country,country_strict,residing_city,residing_city_strict,residing_state,residing_state_strict,citizenship,citizenship_strict=None,None,None,None,None,None,None,None
    else:
        country=profile.location_preferences_table.country
        country_strict=profile.location_preferences_table.country_strict
        residing_city= profile.location_preferences_table.residing_city
        residing_city_strict= profile.location_preferences_table.residing_city_strict
        residing_state=profile.location_preferences_table.residing_state
        residing_state_strict=profile.location_preferences_table.residing_state_strict
        citizenship=profile.location_preferences_table.citizenship
        citizenship_strict=profile.location_preferences_table.citizenship_strict

        
    if profile.religious_preferences_table==None:
        level_one,level_one_strict,level_two,level_two_strict,level_three,level_three_strict,star,star_strict,dosham, dosham_strict=None,None,None,None,None,None,None,None,None,None
    else:
        level_one=profile.religious_preferences_table.level_one
        level_one_strict=profile.religious_preferences_table.level_one_strict
        level_two= profile.religious_preferences_table.level_two
        level_two_strict= profile.religious_preferences_table.level_two_strict
        level_three=profile.religious_preferences_table.level_three
        level_three_strict=profile.religious_preferences_table.level_three_strict
        star=profile.religious_preferences_table.star
        star_strict=profile.religious_preferences_table.star_strict
        dosham=profile.religious_preferences_table.dosham
        dosham_strict=profile.religious_preferences_table.dosham_strict
      

    pr ={
    "user_main": {
        "name":profile.name,
        "email": profile.email,
        "gender": profile.gender,
        "dob": profile.dob,
    },
    "basic_partner_preferenes": {
        "gender": gender,
        "age_upperLimit": age_upperLimit,
        "age_lowerLimit": age_lowerLimit,
        "age_strict": age_strict,
        "height_upperLimit":height_upperLimit,
        "height_lowerLimit": height_lowerLimit,
        "height_strict": height_strict,
        "physical_status": physical_status,
        "physical_status_strict": physical_status_strict,
        "marital_status":marital_status,
        "marital_status_strict": marital_status_strict,
        "having_child": having_child,
        "having_child_strict": having_child_strict,
        "eating_habit": eating_habit,
        "eating_habit_strict":eating_habit_strict,
        "smoking_habit": smoking_habit,
        "smoking_habit_strict": smoking_habit_strict,
        "drinking_habit": drinking_habit,
        "drinking_habit_strict": drinking_habit_strict,
        "mother_tounge":mother_tounge,
        "mother_tounge_strict": mother_tounge_strict,
    },
    "proffesional_preferences": {
        "education": education,
        "education_strict": education_strict,
        "employed_in":employed_in,
        "employed_in_strict": employed_in_strict,
        "occupation": occupation,
        "occupation_strict": occupation_strict,
        "annual_income": annual_income.value,
        "annual_income_strict": annual_income_strict,
        "income_currency": income_currency,
    },
    "location_preferences": {
        "country": country,
        "country_strict": country_strict,
        "residing_city":residing_city,
        "residing_city_strict": residing_city_strict,
        "residing_state": residing_state,
        "residing_state_strict": residing_state_strict,
        "citizenship": citizenship,
        "citizenship_strict": citizenship_strict,
    },
    "religion_preferences": {
        "level_one": level_one,
        "level_one_strict": level_one_strict,
        "level_two": level_two,
        "level_two_strict": level_two_strict,
        "level_three":level_three,
        "level_three_strict": level_three_strict,
        "star": star,
        "star_strict": star_strict,
        "dosham": dosham.value,
        "dosham_strict": dosham_strict,
    }
}
    resp.append(pr)
    return resp


def get_professional_preference_match_users(db: Session, user_id: int):
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))
    user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
    main_list=[]
    if (user_profession_preff == None):
        print("profession preference NOT provided........")
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
    else:
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)

        user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
        education_strict = user_profession_preff.education_strict
        education = user_profession_preff.education
        employed_in_strict= user_profession_preff.employed_in_strict
        employed_in=user_profession_preff.employed_in
        occupation_strict = user_profession_preff.occupation_strict
        occupation = user_profession_preff.occupation

        annual_income=user_profession_preff.annual_income
        annual_income_lower_limit_inr = user_profession_preff.annual_income_lower_limit_inr
        annual_income_upper_limit_inr = user_profession_preff.annual_income_upper_limit_inr

        if education_strict==True and education != None:
            Edupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.highest_education_id.in_(education['education'])).all()
            edu_list=[i.id for i in Edupref]
            filtered_users=list(set(active_genderpreff_users) & set(edu_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=active_genderpreff_users
            main_list.extend(filtered_users)

        if employed_in_strict==True and employed_in != None:
            Emppref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.employed_in.in_(employed_in['employed_in'])).all()
            empIn_list=[i.id for i in Emppref]
            filtered_users=list(set(filtered_users) & set(empIn_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)

        if occupation_strict==True and occupation != None:
            Occupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.occupation_id.in_(occupation['occupation'])).all()
            occu_list=[i.id for i in Occupref]
            filtered_users=list(set(filtered_users) & set(occu_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)

        if annual_income !="doesn't matter":
            Incmpref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.annual_income_lower_limit_inr>=annual_income_lower_limit_inr,UserProfessionalDetails.annual_income_upper_limit_inr<=annual_income_upper_limit_inr).all()
            Incm_list=[i.id for i in Incmpref]
            filtered_users=list(set(filtered_users) & set(Incm_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)

    frequency = collections.Counter(main_list)
    sorted_d = list(dict( sorted(dict(frequency).items(), key=operator.itemgetter(1),reverse=True)).keys())
    final_list=list(set(filtered_users) & set(sorted_d))
    if user_id in final_list: final_list.remove(user_id)
    final_users=[db.query(User).filter(User.id==id).one() for id in final_list]
    return final_users


def get_Education_preference_only(db: Session, user_id: int):
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))
    user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
    main_list=[]
    if (user_profession_preff == None):
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
    else:
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)

        user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
        education_strict = user_profession_preff.education_strict
        education = user_profession_preff.education

        if education_strict==True and education != None:
            Edupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.highest_education_id.in_(education['education'])).all()
            edu_list=[i.id for i in Edupref]
            filtered_users=list(set(active_genderpreff_users) & set(edu_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=active_genderpreff_users
            main_list.extend(filtered_users)

    frequency = collections.Counter(main_list)
    sorted_d = list(dict( sorted(dict(frequency).items(), key=operator.itemgetter(1),reverse=True)).keys())
    final_list=list(set(filtered_users) & set(sorted_d))
    if user_id in final_list: final_list.remove(user_id)
    final_users=[db.query(User).filter(User.id==id).one() for id in final_list]
    return final_users


# def add_user_proffesion_preferenes(db: Session, proffesional_preferences: ProffesionalPreferencesDetails, user_id: int):
#     if proffesional_preferences.id is not None:
#         cur_prof_pref = get_user_professional_preference_details(db, proffesional_preferences.id)
#         cur_prof_pref.education = proffesional_preferences.education
#         cur_prof_pref.education_strict = proffesional_preferences.education_strict
#         cur_prof_pref.employed_in = proffesional_preferences.employed_in
#         cur_prof_pref.employed_in_strict = proffesional_preferences.employed_in_strict
#         cur_prof_pref.occupation = proffesional_preferences.occupation
#         cur_prof_pref.occupation_strict = proffesional_preferences.occupation_strict
#         cur_prof_pref.annual_income = proffesional_preferences.annual_income
#         cur_prof_pref.annual_income_strict = proffesional_preferences.annual_income_strict
#         cur_prof_pref.income_currency = proffesional_preferences.income_currency
#         db.commit()
#     else:
#         proffesional_preferences = ProffesionalPartnerPreferences(education = proffesional_preferences.education,
#                 education_strict = proffesional_preferences.education_strict, employed_in = proffesional_preferences.employed_in,
#                 employed_in_strict = proffesional_preferences.employed_in_strict, annual_income = proffesional_preferences.annual_income,
#                 occupation = proffesional_preferences.occupation, occupation_strict = proffesional_preferences.occupation_strict,
#                 annual_income_strict = proffesional_preferences.annual_income_strict, income_currency = proffesional_preferences.income_currency,
#                 user_id = user_id)
#         db.add(proffesional_preferences)
#         db.commit()
#         db.refresh(proffesional_preferences)



# def add_user_location_preferenes(db: Session, location_preferences: LocationPartnerPreferencesDetails, user_id: int):
#     if location_preferences.id is not None:
#         cur_loc_pref = get_user_location_preference_details(db, location_preferences.id)
#         cur_loc_pref.country = location_preferences.country
#         cur_loc_pref.country_strict = location_preferences.country_strict
#         cur_loc_pref.residing_city = location_preferences.residing_city
#         cur_loc_pref.residing_city_strict = location_preferences.residing_city_strict
#         cur_loc_pref.residing_state = location_preferences.residing_state
#         cur_loc_pref.residing_state_strict = location_preferences.residing_state_strict
#         cur_loc_pref.citizenship = location_preferences.citizenship
#         cur_loc_pref.citizenship_strict = location_preferences.citizenship_strict
#         db.commit()
#     else:
#         location_preferences = LocationPartnerPreferences(country = location_preferences.country, country_strict = location_preferences.country_strict,
#                 residing_city = location_preferences.residing_city, residing_city_strict = location_preferences.residing_city_strict,
#                 residing_state = location_preferences.residing_state, residing_state_strict = location_preferences.residing_state_strict,
#                 citizenship = location_preferences.citizenship, citizenship_strict = location_preferences.citizenship_strict,
#                 user_id = user_id)
#         db.add(location_preferences)
#         db.commit()
#         db.refresh(location_preferences)



# def add_user_religion_preferenes(db: Session, religion_preferences: ReligiousPartnerPreferencesDetails, user_id: int):
#     if religion_preferences.id is not None:
#         cur_rel_pref = get_user_religion_preference_details(db, religion_preferences.id)
#         cur_rel_pref.religion = religion_preferences.religion
#         cur_rel_pref.religion_strict = religion_preferences.religion_strict
#         cur_rel_pref.caste = religion_preferences.caste
#         cur_rel_pref.caste_strict = religion_preferences.caste_strict
#         cur_rel_pref.sub_caste = religion_preferences.sub_caste
#         cur_rel_pref.sub_caste_strict = religion_preferences.sub_caste_strict
#         cur_rel_pref.divisions = religion_preferences.divisions
#         cur_rel_pref.divisions_strict = religion_preferences.divisions_strict
#         cur_rel_pref.star = religion_preferences.star
#         cur_rel_pref.star_strict = religion_preferences.star_strict
#         cur_rel_pref.dosham = religion_preferences.dosham
#         cur_rel_pref.dosham_strict = religion_preferences.dosham_strict
#         db.commit()
#     else:
#         religion_preferences = ReligiousPartnerPreferences(religion = religion_preferences.religion, religion_strict = religion_preferences.religion_strict,
#                 caste = religion_preferences.caste, caste_strict = religion_preferences.caste_strict, sub_caste = religion_preferences.sub_caste,
#                 sub_caste_strict = religion_preferences.sub_caste_strict, divisions = religion_preferences.divisions, divisions_strict = religion_preferences.divisions_strict,
#                 star = religion_preferences.star, star_strict = religion_preferences.star_strict, dosham = religion_preferences.dosham,
#                 dosham_strict = religion_preferences.dosham_strict,
#                 user_id = user_id)
#         db.add(religion_preferences)
#         db.commit()
#         db.refresh(religion_preferences)


# ///

def get_all_education_datas(db: Session):
    result = db.query(Education.qualification.label('label'),Education.id.label('value')).filter(Education.is_deleted==False).all()
    return result

def get_all_education_datas_user_reg(db: Session):
    result = db.query(Education.qualification.label('label'),Education.id.label('value')).filter(Education.is_deleted==False).all()
    return result


def get_area_of_specialization_by_education(db: Session, education_id: int):
    result = db.query(AreaOfSpecialization.area_of_specialization.label('label'),AreaOfSpecialization.id.label('value')).filter(AreaOfSpecialization.is_deleted==False, AreaOfSpecialization.education_id==education_id).all()
    return result


def get_all_occupation_datas(db: Session):
    result = db.query(Occupation.occupation_name.label('label'),Occupation.id.label('value')).filter(Occupation.is_deleted==False).all()
    return result


def get_all_occupation_datas_user_reg(db: Session):
    result = db.query(Occupation.occupation_name.label('label'),Occupation.id.label('value')).filter(Occupation.is_deleted==False).all()
    return result



# ///

def get_all_religion_datas(db: Session):
    result = db.query(Religion.level_one.label('label'),Religion.level_one.label('value'), Religion.id.label('religion_id')).distinct(Religion.level_one).filter(Religion.is_deleted==False).all()
    return result


def get_all_religion_datas_user_reg(db: Session):
    result = db.query(Religion.level_one.label('label'),Religion.level_one.label('value'), Religion.id.label('religion_id')).distinct(Religion.level_one).filter(Religion.is_deleted==False).all()
    return result


def get_all_caste_datas(db: Session):
    result = db.query(Religion.level_two.label('label'),Religion.level_two.label('value'), Religion.id.label('religion_id')).distinct(Religion.level_two).filter(Religion.is_deleted==False).all()
    return result


def get_all_caste_datas_user_reg(db: Session):
    result = db.query(Religion.level_two.label('label'),Religion.level_two.label('value'), Religion.id.label('religion_id')).distinct(Religion.level_two).filter(Religion.is_deleted==False).all()
    return result


def get_all_subcaste_datas(db: Session):
    result = db.query(Religion.level_three.label('label'),Religion.level_three.label('value'), Religion.id.label('religion_id')).distinct(Religion.level_three).filter(Religion.is_deleted==False).all()
    return result


def get_all_star_datas(db: Session):
    result = db.query(Star.star_name.label('label'),Star.id.label('value')).filter(Star.is_deleted==False).all()
    return result
