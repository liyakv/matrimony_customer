import datetime as dt
from pytz import timezone
from sqlalchemy.orm import Session
from app.models.model import *
from app.schemas.preferences.preference import *
import config


def get_gender_matched_partners(db: Session, user: User):
    return db.query(User).filter(User.gender==user.basic_preferences_table.gender, User.id!=user.id).all()


def get_match_score(db: Session, user: User, partner: User):

    total_preferences = 0
    score = 0
    prefence_details = {}
#basic preference
    basic_pref_details = []
    if user.basic_preferences_table:
        #age
        partner_age = config.calculate_age(partner.dob)
        if user.basic_preferences_table.age_upperLimit is not None:
            total_preferences += 1
            gdr ="Groom's"
            if user.basic_preferences_table.gender == "female":
                gdr = "Bride's"
            age_pref = {"matching_status": False,"label": gdr+" Age", "value": str(user.basic_preferences_table.age_lowerLimit) + " - " + str(user.basic_preferences_table.age_upperLimit) + " yrs", "is_strict": user.basic_preferences_table.age_strict }
            if (user.basic_preferences_table.age_upperLimit >= partner_age) and (user.basic_preferences_table.age_lowerLimit <= partner_age):
                score += 1
                age_pref["matching_status"] = True
            basic_pref_details.append(age_pref)
        #height
        if user.basic_preferences_table.height_upperLimit is not None:
            total_preferences += 1
            height_pref = {"matching_status": False,"label": "Height", "value": config.cm_to_feet(user.basic_preferences_table.height_lowerLimit) + " - " + config.cm_to_feet(user.basic_preferences_table.height_upperLimit) + " yrs", "is_strict": user.basic_preferences_table.height_strict }
            if (user.basic_preferences_table.height_upperLimit >= partner.user_basic_details.height_in_cm) and (user.basic_preferences_table.height_lowerLimit <= partner.user_basic_details.height_in_cm):
                score += 1
                height_pref["matching_status"] = True
            basic_pref_details.append(height_pref)
        #physical status
        if user.basic_preferences_table.physical_status is not None:
            total_preferences += 1
            physical_status = user.basic_preferences_table.physical_status["physical_status"]
            phcl_pref = {"matching_status": False,"label": "Physical Status", "value": ','.join(physical_status), "is_strict": user.basic_preferences_table.physical_status_strict }
            if "Any" in physical_status:
                score += 1
                phcl_pref["matching_status"] = True
            elif partner.user_basic_details.physical_status in physical_status:
                score += 1
                phcl_pref["matching_status"] = True
            basic_pref_details.append(phcl_pref)
        #marital status
        if user.basic_preferences_table.marital_status is not None:
            total_preferences += 1
            marital_status = user.basic_preferences_table.marital_status["marital_status"]
            martl_pref = {"matching_status": False,"label": "Marital Status", "value": ','.join(marital_status), "is_strict": user.basic_preferences_table.marital_status_strict }
            if "Any" in marital_status:
                score += 1
                martl_pref["matching_status"] = True
            elif partner.user_basic_details.marital_status in marital_status:
                score += 1
                martl_pref["matching_status"] = True
            basic_pref_details.append(martl_pref)
        #having child
        if user.basic_preferences_table.having_child is not None:
            total_preferences += 1
            having_child = user.basic_preferences_table.having_child["having_child"]
            child_pref = {"matching_status": False,"label": "Having Child", "value": ','.join(having_child), "is_strict": user.basic_preferences_table.having_child_strict }
            if "Doesn't matter" in having_child:
                score += 1
                child_pref["matching_status"] = True
            if partner.user_basic_details.have_child in having_child:
                score += 1
                child_pref["matching_status"] = True
            basic_pref_details.append(child_pref)
        if partner.user_lifestyle_details:
            #eating habit
            if user.basic_preferences_table.eating_habit is not None:
                total_preferences += 1
                eating_habit = user.basic_preferences_table.eating_habit["eating_habit"]
                eating_pref = {"matching_status": False,"label": "Eating Habits", "value": ','.join(eating_habit), "is_strict": user.basic_preferences_table.eating_habit_strict }
                if "Any" in eating_habit:
                    score += 1
                    eating_pref["matching_status"] = True
                if partner.user_lifestyle_details.food_habits in eating_habit:
                    score += 1
                    eating_pref["matching_status"] = True
                basic_pref_details.append(eating_pref)
            #smoking habit
            if user.basic_preferences_table.smoking_habit is not None:
                total_preferences += 1
                smoking_habit = user.basic_preferences_table.smoking_habit["smoking_habit"]
                smoking_pref = {"matching_status": False,"label": "Smoking Habits", "value": ','.join(smoking_habit), "is_strict": user.basic_preferences_table.smoking_habit_strict }
                if "Doesn't matter" in smoking_habit:
                    score += 1
                    smoking_pref["matching_status"] = True
                if partner.user_lifestyle_details.smoking_habits in smoking_habit:
                    score += 1
                    smoking_pref["matching_status"] = True
                basic_pref_details.append(smoking_pref)
            #drinking habit
            if user.basic_preferences_table.drinking_habit is not None:
                total_preferences += 1
                drinking_habit = user.basic_preferences_table.drinking_habit["drinking_habit"]
                drinking_pref = {"matching_status": False,"label": "Drinking Habits", "value": ','.join(drinking_habit), "is_strict": user.basic_preferences_table.drinking_habit_strict }
                if "Doesn't matter" in drinking_habit:
                    score += 1
                    drinking_pref["matching_status"] = True
                if partner.user_lifestyle_details.drinking_habits in drinking_habit:
                    score += 1
                    drinking_pref["matching_status"] = True
                basic_pref_details.append(drinking_pref)
        #mother tongue
        if user.basic_preferences_table.mother_tounge is not None:
            total_preferences += 1
            mother_tounge = user.basic_preferences_table.mother_tounge["mother_tounge"]
            mtr_pref = {"matching_status": False,"label": "Mother Tongue", "value": ','.join(mother_tounge), "is_strict": user.basic_preferences_table.mother_tounge_strict }
            if "Any" in mother_tounge:
                score += 1
                mtr_pref["matching_status"] = True
            if partner.user_basic_details.mother_tongue in mother_tounge:
                score += 1
                mtr_pref["matching_status"] = True
            basic_pref_details.append(mtr_pref)
#professional preference
    professional_pref_details = []
    if user.proffesional_preferences_table is not None:
        #education
        if user.proffesional_preferences_table.education is not None:
            total_preferences += 1
            education = user.proffesional_preferences_table.education["education"]
            education_pref = {"matching_status": False,"label": "Education", "value": "Any", "is_strict": user.proffesional_preferences_table.education_strict }
            if partner.user_professional_details:
                if "Any" in education:
                    score += 1
                    education_pref["matching_status"] = True
                if partner.user_professional_details.highest_education_id in education:
                    score += 1
                    educations = [edn for edn, in db.query(Education.qualification).filter(Education.id.in_(education)).all()]
                    education_pref["matching_status"] = True
                    education_pref["value"] = ','.join(educations)
            professional_pref_details.append(education_pref)
        #employed in
        if user.proffesional_preferences_table.employed_in is not None:
            total_preferences += 1
            employed_in = user.proffesional_preferences_table.employed_in["employed_in"]
            employed_in_pref = {"matching_status": False,"label": "Employed In", "value": ','.join(employed_in), "is_strict": user.proffesional_preferences_table.employed_in_strict }
            if partner.user_professional_details:
                if "Any" in employed_in:
                    score += 1
                    employed_in_pref["matching_status"] = True
                if partner.user_professional_details.employed_in in employed_in:
                    score += 1
                    employed_in_pref["matching_status"] = True
            professional_pref_details.append(employed_in_pref)
        #occupation
        if user.proffesional_preferences_table.occupation is not None:
            total_preferences += 1
            occupation = user.proffesional_preferences_table.occupation["occupation"]
            occupation_pref = {"matching_status": False,"label": "Occupation", "value": "Any", "is_strict": user.proffesional_preferences_table.occupation_strict }
            if partner.user_professional_details:
                if "Any" in occupation:
                    score += 1
                    occupation_pref["matching_status"] = True
                if partner.user_professional_details.occupation_id in occupation:
                    score += 1
                    occupations = [ocpn for ocpn, in db.query(Occupation.occupation_name).filter(Occupation.id.in_(occupation)).all()]
                    occupation_pref["matching_status"] = True
                    occupation_pref["value"] = ','.join(occupations)
            professional_pref_details.append(occupation_pref)
        #annual income
        if user.proffesional_preferences_table.annual_income is not None:
            total_preferences += 1
            annual_income = user.proffesional_preferences_table.annual_income
            income_pref = {"matching_status": False,"label": "Annual Income", "value": annual_income, "is_strict": user.proffesional_preferences_table.annual_income_strict }
            if partner.user_professional_details:
                if annual_income == "Doesn't matter":
                    score += 1
                    income_pref["matching_status"] = True
                elif ((partner.user_professional_details.annual_income_lower_limit_inr >= user.proffesional_preferences_table.annual_income_lower_limit_inr) and (partner.user_professional_details.annual_income_lower_limit_inr <= user.proffesional_preferences_table.annual_income_upper_limit_inr)) or ((partner.user_professional_details.annual_income_upper_limit_inr >= user.proffesional_preferences_table.annual_income_lower_limit_inr) and (partner.user_professional_details.annual_income_upper_limit_inr <= user.proffesional_preferences_table.annual_income_upper_limit_inr)):
                    score += 1
                    income_pref["matching_status"] = True
                    income_pref["value"] = str(user.proffesional_preferences_table.income_currency) +" " + str(annual_income)
            professional_pref_details.append(income_pref)
#location preference
    location_pref_details = []
    if user.location_preferences_table is not None:
        #country
        if user.location_preferences_table.country is not None:
            total_preferences += 1
            country = user.location_preferences_table.country["country"]
            country_pref = {"matching_status": False,"label": "Country", "value": ','.join(country), "is_strict": user.location_preferences_table.country_strict }
            if partner.user_location_details:
                if "Any" in country:
                    score += 1
                    country_pref["matching_status"] = True
                if partner.user_location_details.country in country:
                    score += 1
                    country_pref["matching_status"] = True
            location_pref_details.append(country_pref)
        #residing city
        if user.location_preferences_table.residing_city is not None:
            total_preferences += 1
            residing_city = user.location_preferences_table.residing_city["residing_city"]
            city_pref = {"matching_status": False,"label": "Residing City", "value": ','.join(residing_city), "is_strict": user.location_preferences_table.residing_city_strict }
            if partner.user_location_details:
                if "Any" in residing_city:
                    score += 1
                    city_pref["matching_status"] = True
                if partner.user_location_details.city in residing_city:
                    score += 1
                    city_pref["matching_status"] = True
            location_pref_details.append(city_pref)
        #residing state
        if user.location_preferences_table.residing_state is not None:
            total_preferences += 1
            residing_state = user.location_preferences_table.residing_state["residing_state"]
            state_pref = {"matching_status": False,"label": "Residing State", "value": ','.join(residing_state), "is_strict": user.location_preferences_table.residing_state_strict }
            if partner.user_location_details:
                if "Any" in residing_state:
                    score += 1
                    state_pref["matching_status"] = True
                if partner.user_location_details.state in residing_state:
                    score += 1
                    state_pref["matching_status"] = True
            location_pref_details.append(state_pref)
        #citizenship
        if user.location_preferences_table.citizenship is not None:
            total_preferences += 1
            citizenship = user.location_preferences_table.citizenship["citizenship"]
            ctzip_pref = {"matching_status": False,"label": "Citizenship", "value": ','.join(citizenship), "is_strict": user.location_preferences_table.citizenship_strict }
            if partner.user_location_details:
                if "Any" in citizenship:
                    score += 1
                    ctzip_pref["matching_status"] = True
                if partner.user_location_details.citizenship:
                    if partner.user_location_details.citizenship in citizenship:
                        score += 1
                        ctzip_pref["matching_status"] = True
            location_pref_details.append(ctzip_pref)
#religious preference
    religious_pref_details = []
    if user.religious_preferences_table is not None:
        #religion - level one
        if user.religious_preferences_table.level_one is not None:
            total_preferences += 1
            level_one = user.religious_preferences_table.level_one
            relgn_pref = {"matching_status": False,"label": "level_one", "value": level_one, "is_strict": user.religious_preferences_table.level_one_strict }
            if partner.user_religion_details:
                if level_one == "Any":
                    score += 1
                    relgn_pref["matching_status"] = True
                elif partner.user_religion_details.religion.level_one == level_one:
                    score += 1
                    relgn_pref["matching_status"] = True
            religious_pref_details.append(relgn_pref)
        #level two - caste, division
        if user.religious_preferences_table.level_two is not None:
            total_preferences += 1
            level_two = user.religious_preferences_table.level_two["level_two"]
            level_two_pref = {"matching_status": False,"label": "level_two", "value": ','.join(level_two), "is_strict": user.religious_preferences_table.level_two_strict }
            if partner.user_religion_details:
                if "Any" in level_two:
                    score += 1
                    level_two_pref["matching_status"] = True
                elif partner.user_religion_details.religion.level_two in level_two:
                    score += 1
                    level_two_pref["matching_status"] = True
            religious_pref_details.append(level_two_pref)
        #level three - caste, subcaste
        if user.religious_preferences_table.level_three is not None:
            total_preferences += 1
            level_three = user.religious_preferences_table.level_three["level_three"]
            level_three_pref = {"matching_status": False,"label": "level_three", "value": ','.join(level_three), "is_strict": user.religious_preferences_table.level_three_strict }
            if partner.user_religion_details:
                if "Any" in level_three:
                    score += 1
                    level_three_pref["matching_status"] = True
                elif partner.user_religion_details.religion.level_three in level_three:
                    score += 1
                    level_three_pref["matching_status"] = True
            religious_pref_details.append(level_three_pref)
        #star
        if user.religious_preferences_table.star is not None:
            total_preferences += 1
            star = user.religious_preferences_table.star["star"]

            star_pref = {"matching_status": False,"label": "Star", "value": "Any", "is_strict": user.religious_preferences_table.star_strict }
            if partner.user_religion_details:
                if "Any" in star:
                    score += 1
                    star_pref["matching_status"] = True
                if partner.user_religion_details.star_id in star:
                    score += 1
                    star_names = [star_name for star_name, in db.query(Star.star_name).filter(Star.id.in_(star)).all()]
                    star_pref["matching_status"] = True
                    star_pref["value"] = ','.join(star_names)
            religious_pref_details.append(star_pref)
        #dosham
        if user.religious_preferences_table.dosham is not None:
            total_preferences += 1
            dosham = user.religious_preferences_table.dosham
            # dosham_pref = {"matching_status": False,"label": "Dosham", "value": ','.join(dosham), "is_strict": user.religious_preferences_table.dosham_strict }
            dosham_pref = {"matching_status": False,"label": "Dosham", "value": dosham, "is_strict": user.religious_preferences_table.dosham_strict }
            if partner.user_religion_details:
                if "Doesn't matter" == dosham:
                    score += 1
                    dosham_pref["matching_status"] = True
                if partner.user_religion_details.dosham == dosham:
                    score += 1
                    dosham_pref["matching_status"] = True
            religious_pref_details.append(dosham_pref)

    prefence_details["basic_preference"] = basic_pref_details
    prefence_details["professional_preference"] = professional_pref_details
    prefence_details["location_preference"] = location_pref_details
    prefence_details["religious_preference"] = religious_pref_details
    return prefence_details,total_preferences,score

