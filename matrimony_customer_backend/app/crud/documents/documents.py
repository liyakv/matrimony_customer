from distutils.command.upload import upload
import profile
from fastapi import UploadFile
from sqlalchemy.orm import Session
from app.models.model import *
from sqlalchemy import or_
        
        
def add_user_documents(db: Session,  user_id: int, image_url: str, docmnt_type:str, public_id:str):
    db_documents = Documents(user_id=user_id, document=image_url, doc_type=docmnt_type,
                            cldnry_public_id=public_id, status='approved')
    db.add(db_documents)
    db.commit()
    db.refresh(db_documents)
    return db_documents



def get_user_documents(db: Session, user_id: int):
    return db.query(Documents).filter(Documents.is_deleted==False, Documents.user_id==user_id).all()



def check_docs_approved_or_pending(db: Session, user_id: int):
    return db.query(Documents).filter(Documents.is_deleted==False, Documents.user_id==user_id,
                                       or_(Documents.status=='approved', Documents.status=='pending')).all()

                                     
def check_docs_already_send(db: Session, user_id: int,  docmnt_type:str):
    return db.query(Documents).filter(Documents.is_deleted==False, Documents.user_id==user_id,
                                     Documents.doc_type==docmnt_type).all()

    
def get_docs_details_by_id(db: Session,user_id: int,  docmnt_type:str):
    return db.query(Documents).filter_by(doc_type=docmnt_type, user_id=user_id).first()


def update_docs_upload(db: Session, user_id: int, image_url: str, docmnt_type:str, public_id:str):
    cur_request = get_docs_details_by_id(db,user_id=user_id, docmnt_type=docmnt_type)
    cur_request.document=image_url
    cur_request.cldnry_public_id=public_id
    db.commit()
    return cur_request
