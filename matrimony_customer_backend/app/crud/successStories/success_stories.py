from sqlalchemy.orm import Session
from app.models.model import *
from sqlalchemy import desc


def get_all_success_stories(db: Session):
    return db.query(SuccessStories).filter(SuccessStories.is_deleted==False).order_by(desc(SuccessStories.id)).all()