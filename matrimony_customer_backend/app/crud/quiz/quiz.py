from sqlalchemy.orm import Session
from app.models.model import *
from app.schemas.quiz.quiz import *
from sqlalchemy import desc


def add_quiz_question(db: Session, quiz_schema: QuizQuestionCreate, user_id: int):

    user_quiz_schema = quiz_schema.userQuestions
    admin_quiz_schema = quiz_schema.adminQuestions
    removed_questions = quiz_schema.removedQuestions

    for question in user_quiz_schema:
        if question.question != "":
            if question.id is not None:
                choices = db.query(QuizQuestionChoices).filter(QuizQuestionChoices.question_id==question.question_id)
                choices.delete()
                db.commit()

                qz_qutsn = db.query(QuizQuestions).get(question.question_id)
                qz_qutsn.question_type = QuizQuestions.QUESTION_TYPE[question.question_type -1][0]
                qz_qutsn.question = question.question
                db.commit()

                db_quiz_question_log = QuizQuestionsLog(user_id=qz_qutsn.user_id,quiz_question_id=question.question_id, question_type=QuizQuestions.QUESTION_TYPE[question.question_type -1][0], question=question.question, created_by='user')
                db.add(db_quiz_question_log)
                db.commit()
                db.refresh(db_quiz_question_log)
                for choice in question.choices:
                    db_quiz_question_choice = QuizQuestionChoices(choice=choice,question_id=question.question_id)
                    db.add(db_quiz_question_choice)
                    db.commit()
                    db.refresh(db_quiz_question_choice)

                    db_quiz_question_choice_log = QuizQuestionChoicesLog(choice=choice,question_log_id=db_quiz_question_log.id)
                    db.add(db_quiz_question_choice_log)
                    db.commit()
                    db.refresh(db_quiz_question_choice_log)

            else:
                q_type = QuizQuestions.QUESTION_TYPE[question.question_type -1][0]
                db_quiz_question = QuizQuestions(user_id=user_id, question_type=q_type, question=question.question,created_by='user')
                db.add(db_quiz_question)
                db.commit()
                db.refresh(db_quiz_question)

                db_quiz_question_log = QuizQuestionsLog(user_id=user_id,quiz_question_id=db_quiz_question.id, question_type=q_type, question=question.question, created_by='user')
                db.add(db_quiz_question_log)
                db.commit()
                db.refresh(db_quiz_question_log)

                for choice in question.choices:
                    db_quiz_question_choice = QuizQuestionChoices(choice=choice,question_id=db_quiz_question.id)
                    db.add(db_quiz_question_choice)
                    db.commit()
                    db.refresh(db_quiz_question_choice)

                    db_quiz_question_choice_log = QuizQuestionChoicesLog(choice=choice,question_log_id=db_quiz_question_log.id)
                    db.add(db_quiz_question_choice_log)
                    db.commit()
                    db.refresh(db_quiz_question_choice_log)

                db_user_question = UserQuizQuestions(user_id=user_id, question_id=db_quiz_question.id)
                db.add(db_user_question)
                db.commit()
                db.refresh(db_user_question)


    last_inserted_admin_questions = db.query(UserQuizQuestions).join(QuizQuestions).filter(UserQuizQuestions.user_id==user_id,QuizQuestions.created_by=='admin',UserQuizQuestions.is_deleted==False,QuizQuestions.is_deleted==False).all()

    already_added = []
    for adm_qtn in last_inserted_admin_questions:
        if adm_qtn.question_id not in admin_quiz_schema:
            adm_qtn.is_deleted = True
            db.commit()
        already_added.append(adm_qtn.question_id)
    for admin_question in admin_quiz_schema:
        if admin_question not in already_added:
            db_user_question = UserQuizQuestions(user_id=user_id, question_id=admin_question)
            db.add(db_user_question)
            db.commit()
            db.refresh(db_user_question)
    for question_id in removed_questions:
        db_user_question = db.query(UserQuizQuestions).get(question_id)
        db_user_question.is_deleted = True
        db.commit()


def get_quiz(db: Session, user_id: int):
    return db.query(UserQuizQuestions).filter(UserQuizQuestions.is_deleted==False, UserQuizQuestions.user_id==user_id).all()

def get_partner_added_answers(db: Session, user_id: int, partner_user_id: int):
    query = f"""
    select matrimony_user_quiz_answers.id,matrimony_user_quiz_answers.answer,matrimony_quiz_questions.question,
    matrimony_quiz_questions.question_type
    from matrimony_user_quiz_answers inner join matrimony_user_quiz_questions on
    matrimony_user_quiz_answers.question_id=matrimony_user_quiz_questions.id
    inner join matrimony_quiz_questions on matrimony_user_quiz_questions.question_id=matrimony_quiz_questions.id
    and matrimony_user_quiz_questions.user_id='{user_id}'and matrimony_user_quiz_questions.is_deleted='0'
    and matrimony_quiz_questions.is_deleted='0' and matrimony_user_quiz_answers.is_deleted='0'
    where matrimony_user_quiz_answers.user_id='{partner_user_id}'
    """
    return db.execute(query).all()




def get_users_added_answers(db: Session, user_id: int, partner_user_id: int):
    query = f"""
    select matrimony_user_quiz_answers.id,matrimony_user_quiz_answers.answer,matrimony_quiz_questions.question,
    matrimony_quiz_questions.question_type,matrimony_quiz_questions.choices
    from matrimony_user_quiz_answers inner join matrimony_user_quiz_questions on
    matrimony_user_quiz_answers.question_id=matrimony_user_quiz_questions.id
    inner join matrimony_quiz_questions on matrimony_user_quiz_questions.question_id=matrimony_quiz_questions.id
    and matrimony_user_quiz_questions.user_id='{partner_user_id}'and matrimony_user_quiz_questions.is_deleted='0'
    and matrimony_quiz_questions.is_deleted='0' and matrimony_user_quiz_answers.is_deleted='0'
    where matrimony_user_quiz_answers.user_id='{user_id}'
    """
    return db.execute(query).all()


def get_quiz_answer_by_id(db: Session, answer_id: int):
    query = f"""
    select matrimony_user_quiz_answers.id,matrimony_user_quiz_answers.answer,matrimony_quiz_questions.question
    from matrimony_user_quiz_answers inner join matrimony_user_quiz_questions on
    matrimony_user_quiz_answers.question_id=matrimony_user_quiz_questions.id
    inner join matrimony_quiz_questions on matrimony_user_quiz_questions.question_id=matrimony_quiz_questions.id
    and matrimony_user_quiz_answers.id='{answer_id}'and matrimony_user_quiz_questions.is_deleted='0'
    and matrimony_quiz_questions.is_deleted='0' and matrimony_user_quiz_answers.is_deleted='0'
    """
    return db.execute(query).all()


def get_admin_created_questions(db: Session):
    return db.query(QuizQuestions).filter(QuizQuestions.is_deleted==False, QuizQuestions.created_by=='admin').all()


def add_quiz_answers(db: Session, quiz_answer_schema_list: List[QuizQuestionAnswer], user_id: int):
    to_id = None
    for quiz_answer in quiz_answer_schema_list:
        user_question = db.query(UserQuizQuestions).get(quiz_answer.question_id)
        to_id = user_question.user_id
        user_question_log = db.query(QuizQuestionsLog).filter(QuizQuestionsLog.quiz_question_id==user_question.question_id).order_by(desc(QuizQuestionsLog.id)).first()
        db_quiz_ans = UserQuizAnswers(user_id=user_id, question_id=user_question.id, question_log_id=user_question_log.id,answer=quiz_answer.answer)
        db.add(db_quiz_ans)
        db.commit()
        db.refresh(db_quiz_ans)
    if to_id:
        db_interest_request = PartnerInterests(from_user_id=user_id, to_user_id=to_id)
        db.add(db_interest_request)
        db.commit()
        db.refresh(db_interest_request)
    return db_interest_request

def get_admin_created_questions(db: Session):
    return db.query(QuizQuestions).filter(QuizQuestions.is_deleted==False, QuizQuestions.created_by=='admin').all()

def question_add_status(db: Session,user: User):
    question_add=db.query(UserQuizQuestions).filter(UserQuizQuestions.is_deleted==False, UserQuizQuestions.user_id==user.id).all()
    status=False
    if len(question_add)>0:
        status=True
    print(status)
    return status