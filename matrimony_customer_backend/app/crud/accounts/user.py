from app.schemas.accounts.user import *
from app.schemas.profile.profile import *
from app.models.model import *
from datetime import datetime, timedelta
from sqlalchemy.orm import Session


def resend_otp_crud(db: Session, customer: UserResendOTP, otp: int):
    user = db.query(User).filter(User.phone_number ==
                                  customer.phone_number).first()

    deactivated_user = None
    deact_user = db.query(User).filter(User.phone_number == customer.phone_number, User.is_active == False).first()
    
    if deact_user is not None:
        deactivated_user = True
    if deact_user is None:
        deactivated_user = False
                      
    if user:
        generated_otp = OtpVerification(
            otp=otp, phone_number=customer.phone_number, user=user)
        db.add(generated_otp)
        db.commit()
        result = {"id": user.id,
                  "phone_number": user.phone_number, "otp": otp, 
                  "is_user_deactivated": deactivated_user}
        return result
    return None


def verify_otp(db: Session, credential: AuthDetails):
    user = db.query(User).filter(
        User.phone_number == credential.mobile).first()
    if user is None:
        return False, user
    five_minute = timedelta(minutes=5)
    previous_time = datetime.now()-five_minute

    otp = db.query(OtpVerification).filter(OtpVerification.created_at.between(
        previous_time,
        datetime.now()
    ), OtpVerification.user == user, OtpVerification.otp == credential.otp).first()

    if otp is None:
        return False, user
    otp.is_verified = True
    db.commit()
    return True, user


def create_initial_user_crud(db: Session, user: UserInitialRegister):
    users = db.query(User).filter(User.phone_number == user.phone_number)

    deactivated_users = None
    deact_user = db.query(User).filter(User.phone_number == user.phone_number,
                                       User.is_active == False).first()
    print(deact_user,'--------------------------------')

    if deact_user is not None:
        deactivated_users = True
    if deact_user is None:
        deactivated_users = False
    
    count = users.count()
    is_already_registered = False
    if count == 0:
        db_user = User(phone_number=user.phone_number, name=user.name, username=user.phone_number, date_joined=datetime.now(), is_customer=True, is_active=True)
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        db_userr = UserBasicDetails(profile_created_for = user.profile_created_for, user_id = db_user.id)
        db.add(db_userr)
        db.commit()
        db.refresh(db_userr)
    else:
        db_user = users.first()
        is_already_registered = True

    return {"id": db_user.id,
              "phone_number": db_user.phone_number, "is_already_registered": is_already_registered,
              "is_user_deactivated": deactivated_users}



# /////profile completeness percentage/////
def get_profile_percentage(db: Session, user_id:int):
    user = db.query(User).filter(User.id == user_id).first()
    fields = 0
    total_count = 67
# user-main-details***
    if user.name is not None:
        fields += 1
    if user.email is not None:
        fields += 1
    if user.gender is not None:
        fields += 1
    if user.phone_number is not None:
        fields += 1
    if user.dob is not None:
        fields += 1
    if user.is_photos_verified == True:
        fields += 1
    if user.is_documents_verified == True:
        fields += 1
# user-religion-details***
    if user.user_religion_details:
        # if religion is hindu***
        if user.user_religion_details.religion:
            if user.user_religion_details.religion.level_one == "Hindu":
                total_count += 4
                if user.user_religion_details.dosham is not None:
                    fields += 1
                if user.user_religion_details.raasi is not None:
                    fields += 1
                if user.user_religion_details.gothram is not None:
                    fields += 1
                if user.user_religion_details.star_id is not None:
                    fields += 1
        if user.user_religion_details.religion_id is not None:
            fields += 1
        if user.user_religion_details.place_of_birth is not None:
            fields += 1
        if user.user_religion_details.time_of_birth is not None:
            fields += 1  
# user-basic-details***
    if user.user_basic_details:
        if user.user_basic_details.profile_created_for is not None:
            fields += 1
        if user.user_basic_details.mother_tongue is not None:
            fields += 1
        if user.user_basic_details.body_type is not None:
            fields += 1
        if user.user_basic_details.physical_status is not None:
            fields += 1
        if user.user_basic_details.skin_color is not None:
            fields += 1
        if user.user_basic_details.height_in_cm is not None:
            fields += 1
        if user.user_basic_details.weight is not None:
            fields += 1
        if user.user_basic_details.marital_status is not None:
            fields += 1
        if user.user_basic_details.get_married_in is not None:
            fields += 1
        if user.user_basic_details.about_me is not None:
            fields += 1
        if user.user_basic_details.have_child is not None:
            fields += 1
# user-lifestyle-details***
    if user.user_lifestyle_details:
        if user.user_lifestyle_details.smoking_habits is not None:
            fields += 1
        if user.user_lifestyle_details.drinking_habits is not None:
            fields += 1
        if user.user_lifestyle_details.food_habits is not None:
            fields += 1
        if user.user_lifestyle_details.hobbies is not None:
            fields += 1
        if user.user_lifestyle_details.interests is not None:
            fields += 1
        if user.user_lifestyle_details.favourite_music is not None:
            fields += 1
        if user.user_lifestyle_details.favourite_books is not None:
            fields += 1
        if user.user_lifestyle_details.preffered_movies is not None:
            fields += 1
        if user.user_lifestyle_details.sports_activities is not None:
            fields += 1
        if user.user_lifestyle_details.fitness_activities is not None:
            fields += 1
        if user.user_lifestyle_details.favourite_cuisine is not None:
            fields += 1
        if user.user_lifestyle_details.preferred_dress_style is not None:
            fields += 1
        if user.user_lifestyle_details.spoken_languages is not None:
            fields += 1     
# user-socialmedia-profile-details***
    social_media = db.query(UserSocialMediaProfiles).filter(UserSocialMediaProfiles.user_id==user_id, UserSocialMediaProfiles.is_deleted==False).all()
    if social_media is not None:
        fields += len(social_media)
# user-family-details***
    if user.user_family_details:
        if user.user_family_details.family_type is not None:
            fields += 1
        if user.user_family_details.family_values is not None:
            fields += 1
        if user.user_family_details.family_status is not None:
            fields += 1
        if user.user_family_details.living_status is not None:
            fields += 1
        if user.user_family_details.description is not None:
            fields += 1
        if user.user_family_details.father_name is not None:
            fields += 1
        if user.user_family_details.father_occupation_id is not None:
            fields += 1
        if user.user_family_details.mother_name is not None:
            fields += 1
        if user.user_family_details.mother_occupation_id is not None:
            fields += 1
# user-profession-details***
    if user.user_professional_details:
        if user.user_professional_details.highest_education_id is not None:
            fields += 1
        if user.user_professional_details.institutions is not None:
            fields += 1
        if user.user_professional_details.area_of_specialization_id is not None:
            fields += 1
        if user.user_professional_details.employed_in is not None:
            fields += 1
        if user.user_professional_details.occupation_id is not None:
            fields += 1
        if user.user_professional_details.organization is not None:
            fields += 1
        if user.user_professional_details.role is not None:
            fields += 1
        if user.user_professional_details.currency_type is not None:
            fields += 1
        if user.user_professional_details.annual_income is not None:
            fields += 1
        if user.user_professional_details.work_location is not None:
            fields += 1
        if user.user_professional_details.state is not None:
            fields += 1
        if user.user_professional_details.city is not None:
            fields += 1       
        if user.user_professional_details.citizenship is not None:
            fields += 1       
        if user.user_professional_details.residential_status is not None:
            fields += 1       
# user-location-details***
    if user.user_location_details:
        if user.user_location_details.country is not None:
            fields += 1
        if user.user_location_details.state is not None:
            fields += 1
        if user.user_location_details.city is not None:
            fields += 1
        if user.user_location_details.district is not None:
            fields += 1
        if user.user_location_details.citizenship is not None:
            fields += 1
        # if user.user_location_details.ancestral_origin is not None:
        #     fields += 1
# user-profile-questions-details***
    usr_profile_qstns = db.query(UserProfileQuestions).filter(UserProfileQuestions.user_id==user_id, UserProfileQuestions.is_deleted==False).all()
    if usr_profile_qstns is not None:
        fields += 1
    percentage = 0
    percentage = round((fields*100)/total_count)
    return percentage
