from sqlalchemy.orm import Session
from app.models.model import UserImage, AdditionalImage

def add_user_image(db: Session, user_id: int, watermarked_image_url: str, public_id: str, thumbnail_image_url: str, blurred_image_url: str):
    db_image = db.query(UserImage).filter(UserImage.user_id==user_id, UserImage.is_deleted==False, UserImage.is_main==True).all()
    is_main = False
    if len(db_image) == 0:
        is_main = True
    db_images = UserImage(user_id=user_id, image=watermarked_image_url, cldnry_public_id=public_id, watermarked_image=watermarked_image_url, thumbnail_image=thumbnail_image_url, blurred_image=blurred_image_url, is_verified=True, is_main=is_main)
    db.add(db_images)
    db.commit()
    db.refresh(db_images)
    return db_images


def add_user_house_image(db: Session, user_id: int, image_url: str, public_id:str):
    db_images = AdditionalImage(user_id = user_id, image = image_url, cldnry_public_id = public_id, section = "house photo", is_verified=True)
    db.add(db_images)
    db.commit()
    db.refresh(db_images)
    return db_images


def add_user_vehicle_image(db: Session, user_id: int, image_url: str, public_id: str):
    db_images = AdditionalImage(user_id = user_id, image = image_url, cldnry_public_id = public_id, section = "vehicle photo", is_verified=True)
    db.add(db_images)
    db.commit()
    db.refresh(db_images)
    return db_images


def get_user_default_image(db: Session, user_id: int):
    db_images = db.query(UserImage).filter(UserImage.user_id==user_id, UserImage.is_deleted==False, UserImage.is_main==True).first()
    return db_images


def get_user_images(db: Session, user_id: int):
    db_images = db.query(UserImage).filter(UserImage.user_id == user_id,UserImage.is_deleted==False).all()
    return db_images


def get_user_home_images(db: Session, user_id: int):
    db_images = db.query(AdditionalImage).filter(AdditionalImage.user_id == user_id, AdditionalImage.is_deleted == False, AdditionalImage.section == "house photo").all()
    return db_images


def get_user_vehichle_images(db: Session, user_id: int):
    db_images = db.query(AdditionalImage).filter(AdditionalImage.user_id == user_id, AdditionalImage.is_deleted == False, AdditionalImage.section == "vehicle photo").all()
    return db_images


def remove_user_image(db: Session, user_id: int, image_id: int):
    db_images = db.query(UserImage).filter(UserImage.user_id == user_id, UserImage.id == image_id).first()
    db_images.is_deleted = True
    db.commit()
    return db_images


def remove_user_home_image(db: Session, user_id: int, image_id: int):
    db_images = db.query(AdditionalImage).filter(AdditionalImage.user_id == user_id, AdditionalImage.id == image_id, AdditionalImage.section == "house photo").first()
    db_images.is_deleted = True
    db.commit()
    return db_images


def remove_user_vehicle_image(db: Session, user_id: int, image_id: int):
    db_images = db.query(AdditionalImage).filter(AdditionalImage.user_id == user_id, AdditionalImage.id == image_id, AdditionalImage.section == "vehicle photo").first()
    db_images.is_deleted = True
    db_images.is_main = False
    db.commit()
    return db_images


def set_user_profile(db: Session, user_id: int, image_id: int):
    db_image = db.query(UserImage).filter(UserImage.id == image_id).first()
    is_profile = db.query(UserImage).filter(UserImage.user_id == user_id,UserImage.is_main == True).first()
    print(is_profile,'is_profile')
    if (is_profile == None):
        db_image.is_main = True
        db.commit()
    else:
        is_profile.is_main = False
        db_image.is_main = True
        db.commit()
    return db_image