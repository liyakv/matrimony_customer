from fastapi import FastAPI
from sqlalchemy.orm import Session
from app.models.model import *
from app.schemas.profile.partnerInterests import *


def check_partner_interest_already_send(db: Session, to_user_id: int, from_user_id: int):
    return db.query(PartnerInterests).filter(PartnerInterests.is_deleted==False, PartnerInterests.from_user_id==from_user_id, PartnerInterests.to_user_id==to_user_id).all()


def check_partner_interest_recently_send(db: Session, to_user_id: int, from_user_id: int):
    return db.query(PartnerInterests).filter(PartnerInterests.is_deleted==False, PartnerInterests.from_user_id==from_user_id, PartnerInterests.to_user_id==to_user_id).first()


def delete_partner_interest(db: Session, cur_partner_interest: PartnerInterests):
    cur_partner_interest.is_deleted = True
    cur_partner_interest.deleted_on = datetime.now()
    db.commit()

    useranswerattend = db.query(UserQuizAnswers).join(UserQuizQuestions).filter(UserQuizQuestions.user_id==cur_partner_interest.to_user_id,UserQuizAnswers.user_id==cur_partner_interest.from_user_id,UserQuizQuestions.is_deleted==False,UserQuizAnswers.is_deleted==False).with_entities(UserQuizAnswers.id).all()
    retVal = [row[0] for row in useranswerattend]
    db.query(UserQuizAnswers).filter(UserQuizAnswers.id.in_(retVal)).update({"is_deleted":True})
    db.commit()
    return cur_partner_interest


def send_partner_interest(db: Session, interest_request: PartnerInterestCreate, from_user_id: int):
    db_interest_request = PartnerInterests(from_user_id=from_user_id, to_user_id=interest_request.user_id)
    db.add(db_interest_request)
    db.commit()
    db.refresh(db_interest_request)
    return db_interest_request


def get_partner_interest_by_id(db: Session, id: int):
    return db.query(PartnerInterests).get(id)


def update_partner_interest(db: Session, partner_request: PartnerInterestUpdate, cur_request: PartnerInterests):
    cur_request.status = partner_request.status
    db.commit()
    return cur_request


def get_interest_sent_details_by_user(db: Session, user: int):
    query = f"""
        select matrimony_partner_interests.id as interest_id,matrimony_user.id as user_id,matrimony_user.name, matrimony_user.gender, matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
        date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
        matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
        matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_religion.level_three,matrimony_star.star_name,
        matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
        matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,matrimony_partner_interests.is_viewed, matrimony_partner_interests.updated_on,matrimony_partner_interests.status,matrimony_partner_interests.created_on, matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified
        from matrimony_partner_interests inner join matrimony_user on matrimony_partner_interests.to_user_id=matrimony_user.id
        and matrimony_user.is_active='1' left outer join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
        left outer join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
        left outer join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
        left outer join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
        left outer join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
        left outer join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
        left outer join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
        left outer join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
        where matrimony_partner_interests.from_user_id='{user}' and matrimony_partner_interests.is_deleted='0' order by matrimony_partner_interests.id desc
    """
    return db.execute(query).all()


def get_new_interest_sent_details_by_user(db: Session, user: int):
    query = f"""
        select matrimony_partner_interests.id as interest_id,matrimony_user.id as user_id,matrimony_user.name, matrimony_user.gender, matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
        date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
        matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
        matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_religion.level_three,matrimony_star.star_name,
        matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
        matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,matrimony_partner_interests.is_viewed, matrimony_partner_interests.updated_on,matrimony_partner_interests.status,matrimony_partner_interests.created_on, matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified
        from matrimony_partner_interests inner join matrimony_user on matrimony_partner_interests.to_user_id=matrimony_user.id
        and matrimony_user.is_active='1' left outer join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
        left outer join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
        left outer join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
        left outer join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
        left outer join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
        left outer join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
        left outer join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
        left outer join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
        where matrimony_partner_interests.from_user_id='{user}' and matrimony_partner_interests.is_viewed='0' and matrimony_partner_interests.is_deleted='0' order by matrimony_partner_interests.id desc
    """
    return db.execute(query).all()


def get_interest_received_details_by_user(db: Session, user: int):
    query = f"""
        select matrimony_partner_interests.id as interest_id,matrimony_user.id as user_id, matrimony_user.name, matrimony_user.gender, matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
        date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
        matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
        matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_religion.level_three,matrimony_star.star_name,
        matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
        matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,matrimony_partner_interests.is_viewed, matrimony_partner_interests.updated_on,matrimony_partner_interests.status,matrimony_partner_interests.created_on, matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified
        from matrimony_partner_interests inner join matrimony_user on matrimony_partner_interests.from_user_id=matrimony_user.id
        and matrimony_user.is_active='1' left outer join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
        left outer join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
        left outer join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
        left outer join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
        left outer join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
        left outer join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
        left outer join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
        left outer join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
        where matrimony_partner_interests.to_user_id='{user}' and matrimony_partner_interests.is_deleted='0' order by matrimony_partner_interests.id desc
    """
    return db.execute(query).all()


def get_new_interest_received_details_by_user(db: Session, user: int):
    query = f"""
        select matrimony_partner_interests.id as interest_id,matrimony_user.id as user_id, matrimony_user.name, matrimony_user.gender, matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
        date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
        matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
        matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_religion.level_three,matrimony_star.star_name,
        matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
        matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,matrimony_partner_interests.is_viewed, matrimony_partner_interests.updated_on,matrimony_partner_interests.status,matrimony_partner_interests.created_on, matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified
        from matrimony_partner_interests inner join matrimony_user on matrimony_partner_interests.from_user_id=matrimony_user.id
        and matrimony_user.is_active='1' left outer join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
        left outer join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
        left outer join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
        left outer join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
        left outer join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
        left outer join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
        left outer join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
        left outer join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
        where matrimony_partner_interests.to_user_id='{user}' and matrimony_partner_interests.is_viewed='0' and matrimony_partner_interests.is_deleted='0' order by matrimony_partner_interests.id desc
    """
    return db.execute(query).all()