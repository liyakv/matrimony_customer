from distutils.command.config import config
import email
from genericpath import exists
from multiprocessing import queues
from app.crud.preferences.matches import get_match_score
from app.crud.profile.partnerInterests import check_partner_interest_already_send
from app.crud.profile.profile_manage import get_current_user_all_images, get_partner_all_images
from app.crud.quiz.quiz import question_add_status
from app.schemas.accounts.user import *
from app.schemas.profile.profile import *
from app.models.model import *
from sqlalchemy.orm import Session
from app.schemas.profile.profileVisits import ProfileVisitCreate
from app.schemas.profile.profile_manage import Subcastelist, UserBasicCreate, UserReligionCreate
# from app.crud.home.home import get_gender_preference
from config import *
import config

def get_user_details(db: Session, phone_number: str):
    user = db.query(User).filter(
        User.phone_number == phone_number).first()
    return user


def get_user_default_image(db: Session, user_id: int):
    user_image = db.query(UserImage).filter(
        UserImage.user_id == user_id, UserImage.is_main==True).first()
    return user_image



def get_profile_viewed_details_by_user(db: Session, user: int):
    query = f"""
        select matrimony_user.id as user_id,matrimony_user.name, matrimony_user.gender, matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
        date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
        matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
        matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_religion.level_three,matrimony_star.star_name,
        matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
        matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,
		matrimony_profile_visits.is_viewed, matrimony_profile_visits.id, matrimony_profile_visits.visit_count, matrimony_profile_visits.created_on,  matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified,matrimony_profile_visits.updated_on
        from matrimony_profile_visits inner join matrimony_user on matrimony_profile_visits.from_user_id=matrimony_user.id
        and matrimony_user.is_active='1' left outer join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
        left outer join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
        left outer join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
        left outer join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
        left outer join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
        left outer join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
        left outer join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
        left outer join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
        where matrimony_profile_visits.to_user_id='{user}' order by matrimony_profile_visits.id desc
    """
    return db.execute(query).all()



def get_new_profile_viewed_details_by_user(db: Session, user: int):
    query = f"""
        select matrimony_user.id as user_id,matrimony_user.name, matrimony_user.gender, matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
        date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
        matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
        matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_religion.level_three,matrimony_star.star_name,
        matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
        matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,
		matrimony_profile_visits.is_viewed, matrimony_profile_visits.id, matrimony_profile_visits.visit_count, matrimony_profile_visits.created_on,  matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified,matrimony_profile_visits.updated_on
        from matrimony_profile_visits inner join matrimony_user on matrimony_profile_visits.from_user_id=matrimony_user.id
        and matrimony_user.is_active='1' left join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
        left join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
        left join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
        left join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
        left join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
        left join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
        left join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
        left join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
        where matrimony_profile_visits.to_user_id='{user}' and matrimony_profile_visits.is_viewed='0' order by matrimony_profile_visits.id desc
    """
    return db.execute(query).all()


def get_profile_viewed_details_by_user_home(db: Session, user: int):
    query = f"""
        select matrimony_user.id as user_id,matrimony_user.name, matrimony_user.gender, matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
        date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
        matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
        matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_religion.level_three,matrimony_star.star_name,
        matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
        matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,
		matrimony_profile_visits.is_viewed, matrimony_profile_visits.id, matrimony_profile_visits.visit_count, matrimony_profile_visits.created_on,  matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified,matrimony_profile_visits.updated_on
        from matrimony_profile_visits inner join matrimony_user on matrimony_profile_visits.from_user_id=matrimony_user.id
        and matrimony_user.is_active='1' left join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
        left join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
        left join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
        left join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
        left join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
        left join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
        left join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
        left join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
        where matrimony_profile_visits.to_user_id='{user}' order by matrimony_profile_visits.id desc limit 8
    """
    return db.execute(query).all()


def get_user_by_id(db: Session, id: int):
    return db.query(User).get(id)


def update_user_email(db: Session,  email_updt: EmailUpdate, user_id: int):
    updt_email = get_user_by_id(db, id=user_id)
    updt_email.email = email_updt.email
    db.commit()
    return updt_email


def profile_visit_crud(db: Session, visit_profile: ProfileVisitCreate, from_user_id: int):
    db_visit_profile = ProfileVisits(from_user_id=from_user_id, to_user_id=visit_profile.user_id)
    db.add(db_visit_profile)
    db.commit()
    db.refresh(db_visit_profile)
    return db_visit_profile


def get_prof_vst_by_id(db: Session, id: int, from_user_id=int):
    return db.query(ProfileVisits).filter_by(to_user_id = id, from_user_id = from_user_id).first()


def check_already_visited(db: Session, to_user_id: int, from_user_id: int):
    return db.query(ProfileVisits).filter(ProfileVisits.is_deleted==False, ProfileVisits.from_user_id==from_user_id, ProfileVisits.to_user_id==to_user_id).all()


def update_visit_count(db: Session, to_user_id: int, from_user_id: int):
    db_vst_prof = get_prof_vst_by_id(db, id = to_user_id, from_user_id = from_user_id)
    db_vst_prof.visit_count  += 1
    db.commit()


# ///user_regsitration//
def get_userr_reg_by_id(db: Session, id: int):
    return db.query(User).get(id)

def get_basic_details_by_id(db: Session, id: int):
    return db.query(UserBasicDetails).filter_by(user_id=id).first()


def get_religion_details_by_id(db: Session, id: int):
    return db.query(UserReligionInfo).filter_by(user_id=id).first()

def get_location_details_by_id(db: Session, id: int):
    return db.query(UserLocationDetails).filter_by(user_id=id).first()


def get_profession_details_by_id(db: Session, id: int):
    return db.query(UserProfessionalDetails).filter_by(user_id=id).first()


def get_family_details_by_id(db: Session, id: int):
    return db.query(UserFamilyDetails).filter_by(user_id=id).first()

# ******
def get_basic_pref_frst_reg(db: Session, id: int):
    return db.query(BasicPartnerPreferences).filter_by(user_id=id).first()

def get_prof_pref_frst_reg(db: Session, id: int):
    return db.query(ProffesionalPartnerPreferences).filter_by(user_id=id).first()

def get_loctn_pref_frst_reg(db: Session, id: int):
    return db.query(LocationPartnerPreferences).filter_by(user_id=id).first()

def get_relgn_pref_frst_reg(db: Session, id: int):
    return db.query(ReligiousPartnerPreferences).filter_by(user_id=id).first()
# *****


def user_registration_crud(db: Session, user_registration: UserRegistrationMainBase, user_id: int):
    db_user_main = get_userr_reg_by_id(db, id = user_id)
    db_user_main.gender = user_registration.gender
    db_user_main.dob = user_registration.dob
    db_user_main.email = user_registration.email
    db.commit()

    db_basic_details = get_basic_details_by_id(db, id=user_id)
    height_in_feet = None
    if user_registration.height_in_cm:
        height_in_feet = config.cm_to_feet(user_registration.height_in_cm)
    db_basic_details.marital_status = user_registration.marital_status
    db_basic_details.physical_status = user_registration.physical_status
    db_basic_details.mother_tongue = user_registration.mother_tongue
    db_basic_details.height_in_cm = user_registration.height_in_cm
    db_basic_details.height_in_ft = height_in_feet
    db_basic_details.weight = user_registration.weight
    db_basic_details.about_me = user_registration.about_me
    db.commit()


    db_user_religion_register = get_religion_details_by_id(db, id=user_id)
    relgn = None
    # if user_registration.level_one is not None:
    #     relgn = db.query(Religion).filter_by(level_one=user_registration.level_one)
    # if user_registration.level_two is not None:
    #     relgn = relgn.filter_by(level_one=user_registration.level_one)
    # if user_registration.level_three is not None:
    #     relgn = relgn.filter_by(level_one=user_registration.level_one)
    # if relgn is not None:
    #     relgn = relgn.first()
    #     relgn = relgn.id

    if user_registration.level_one is not None:
        relgn = db.query(Religion).filter_by(level_one=user_registration.level_one)
    if user_registration.level_two is not None:
        relgn = relgn.filter_by(level_two=user_registration.level_two)
    if user_registration.level_three is not None:
        relgn = relgn.filter_by(level_three=user_registration.level_three)
    if relgn is not None:
        relgn = relgn.first()
        relgn = relgn.id
    if db_user_religion_register is None:
        db_user_religion_register = UserReligionInfo(religion_id=relgn, user_id=user_id)
        db.add(db_user_religion_register)
        db.commit()
        db.refresh(db_user_religion_register)


    db_user_location_register = get_location_details_by_id(db, id=user_id)
    if db_user_location_register is None:
        db_user_location_register = UserLocationDetails(country=user_registration.country, state=user_registration.state,
                                        city=user_registration.city, user_id=user_id)
        db.add(db_user_location_register)
        db.commit()
        db.refresh(db_user_location_register)


    db_user_profession_register = get_profession_details_by_id(db, id=user_id)
    if db_user_profession_register is None:
        db_user_profession_register = UserProfessionalDetails(highest_education_id=user_registration.highest_education_id, employed_in=user_registration.employed_in,
                                        occupation_id=user_registration.occupation_id, currency_type=user_registration.currency_type,
                                        annual_income=user_registration.annual_income, user_id=user_id)
        db.add(db_user_profession_register)
        db.commit()
        db.refresh(db_user_profession_register)


    db_user_family_register = get_family_details_by_id(db, id=user_id)
    if db_user_family_register is None:
        db_user_family_register = UserFamilyDetails(family_status=user_registration.family_status, family_type=user_registration.family_type,
                                        family_values=user_registration.family_values, user_id=user_id)
        db.add(db_user_family_register)
        db.commit()
        db.refresh(db_user_family_register)


# /// adding preferences ////
    db_user_basic_pref_frst_register = get_basic_pref_frst_reg(db, id=user_id)
    gender_pref = ''
    if user_registration.gender == 'Male' or  user_registration.gender == 'male':
        gender_pref = "Female"
    elif  user_registration.gender =='Female' or  user_registration.gender == 'female':
        gender_pref = "Male"

    if db_user_basic_pref_frst_register is None:
        basic_preferences  = BasicPartnerPreferences(gender = gender_pref,
                        physical_status = {"physical_status":["Any"]}, marital_status = {"marital_status":["Any"]},
                        having_child = {"having_child":["Doesn't matter"]}, eating_habit ={"eating_habit":["Any"]},
                        smoking_habit = {"smoking_habit":["Doesn't matter"]}, drinking_habit = {"drinking_habit":["Doesn't matter"]},
                        mother_tounge = {"mother_tounge":["Any"]}, user_id = user_id
                        )
        db.add(basic_preferences)
        db.commit()
        db.refresh(basic_preferences)

    db_user_prof_pref_frst_register = get_prof_pref_frst_reg(db, id=user_id)
    if db_user_prof_pref_frst_register is None:
        proffesional_preferences = ProffesionalPartnerPreferences(education = {"education":["Any"]},
                employed_in = {"employed_in":["Any"]}, occupation = {"occupation":["Any"]},
                annual_income =  "Doesn't matter", income_currency =  "INR",
                user_id = user_id)
        db.add(proffesional_preferences)
        db.commit()
        db.refresh(proffesional_preferences)

    db_user_loctn_pref_frst_register = get_loctn_pref_frst_reg(db, id=user_id)
    if db_user_loctn_pref_frst_register is None:
        location_preferences = LocationPartnerPreferences(country = {"country":["Any"]},
                residing_city =  {"residing_city":["Any"]}, residing_state = {"residing_state":["Any"]},
                citizenship = {"citizenship":["Any"]},
                user_id = user_id)
        db.add(location_preferences)
        db.commit()
        db.refresh(location_preferences)

    db_user_relgn_pref_frst_register = get_relgn_pref_frst_reg(db, id=user_id)
    if db_user_relgn_pref_frst_register is None:
        religion_preferences = ReligiousPartnerPreferences(
                level_two = {"level_two":["Any"]}, level_three = {"level_three":["Any"]},
                star = {"star":["Any"]}, dosham = "Doesn't matter",
                user_id = user_id)
        db.add(religion_preferences)
        db.commit()
        db.refresh(religion_preferences)


    return db_user_main, db_basic_details, db_user_religion_register, db_user_location_register, db_user_profession_register, db_user_family_register



def get_profile_by_id_details(db: Session, user_id: int):
    profile=db.query(User).filter(User.id==user_id).first()
    resp = []
    if profile.user_basic_details != None:
        year,month=calculate_age_month(profile.dob)
        agedesc = str(year) + " years and " + str(month) + " months"
        basic_details_dict={
            "about_me" : profile.user_basic_details.about_me,
            "profile_created_for":profile.user_basic_details.profile_created_for.value,
            "body_type":profile.user_basic_details.body_type.value if profile.user_basic_details.body_type != None else None,
            "age":agedesc,
            "physical_status":profile.user_basic_details.physical_status.value if profile.user_basic_details.physical_status != None else None,
            "height_in_ft":profile.user_basic_details.height_in_ft,
            "weight":profile.user_basic_details.weight,
            "mother_tongue": profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue != None else None,
            "marital_status":profile.user_basic_details.marital_status.value if profile.user_basic_details.marital_status != None else None,
        }
    else:
        basic_details_dict={
            "about_me" : None,
            "profile_created_for":None,
            "body_type":None,
            "age":None,
            "physical_status":None,
            "height_in_ft":None,
            "weight":None,
            "mother_tongue":None,
            "marital_status":None,
        }

    if profile.user_religion_details != None:
        user_religion_dict={
            "religion": profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None,
            "sub_caste": profile.user_religion_details.religion.level_three,
            "caste":profile.user_religion_details.religion.level_one if profile.user_religion_details.caste  else None,
            "gothram": profile.user_religion_details.gothram,
            "star_name":profile.user_religion_details.star.star_name if profile.user_religion_details.star else None,
            "raasi":profile.user_religion_details.raasi,
            "place_of_birth": profile.user_religion_details.place_of_birth,
            "time_of_birth": profile.user_religion_details.time_of_birth,
        }
    else:
        user_religion_dict={
            "religion" : None,
            "sub_caste":None,
            "caste":None,
            "gothram":None,
            "star_name":None,
            "raasi":None,
            "place_of_birth": None,
            "time_of_birth": None,
        }
    if profile.user_location_details != None:
        user_location_dict={
            "country":profile.user_location_details.country,
            "city":profile.user_location_details.city,
            "state":profile.user_location_details.state,
            "citizenship":profile.user_location_details.citizenship,
        }
    else:
        user_location_dict={
            "country":None,
            "city":None,
            "state":None,
            "citizenship":None,
        }
    if profile.user_professional_details != None:
        user_professional_dict={
            "education": profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education != None else None,
            "institutions":profile.user_professional_details.institutions,
            "employed_in":profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in != None else None,
            "occupation_name": profile.user_professional_details.occupation.occupation_name,
            "annual_income": profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income != None else None,
            "organization":profile.user_professional_details.organization
        }
    else:
        user_professional_dict={
            "education":None,
            "institutions":None,
            "employed_in":None,
            "occupation_name": None,
            "annual_income":None,
            "organization":None,
        }
    global count_sister_m
    global count_sister_u
    global count_brother_m
    global count_brother_u
    if profile.user_siblings_details != None:
        siblings_details=profile.user_siblings_details
        count_sister_m,count_sister_u,count_brother_m,count_brother_u=0,0,0,0
        for i in siblings_details:
            if i.relation =='Sister' :
                if i.marital_status =='Married':
                    count_sister_m=count_sister_m+i.count
                else:
                    count_sister_u=count_sister_u+i.count
            if i.relation =='Brother':
                if i.marital_status =='Married':
                    count_brother_m=count_brother_m+i.count
                else:
                    count_brother_u=count_brother_u+i.count
        siblings_details_dict={
            "no_of_sisters_married": count_sister_m,
            "no_of_sisters_unmarried":count_sister_u,
            "no_of_brothers_married":count_brother_m,
            "no_of_brothers_unmarried":count_brother_u,
        }
    else:
        siblings_details_dict={
            "no_of_sisters_married":None,
            "no_of_sisters_unmarried":None,
            "no_of_brothers_married":None,
            "no_of_brothers_unmarried": None,
        }
    if profile.user_family_details != None:
        family_details_dict={
            "father_occupation": profile.user_family_details.father_occupation.occupation_name if profile.user_family_details.father_occupation != None else None,
            "mother_occupation":profile.user_family_details.mother_occupation.occupation_name if profile.user_family_details.mother_occupation != None else None,
            "family_values":profile.user_family_details.family_values.value if profile.user_family_details.family_values != None else None,
            "family_type": profile.user_family_details.family_type.value if profile.user_family_details.family_type != None else None,
            "family_status": profile.user_family_details.family_status.value if profile.user_family_details.family_status != None else None,
            "about_my_family": profile.user_family_details.description,
        }
    else:
        family_details_dict={
            "father_occupation":None,
            "mother_occupation":None,
            "family_values":None,
            "family_type": None,
            "family_status":None,
            "about_my_family":None,
        }
    if profile.user_lifestyle_details != None:
        user_lifestyle_dict={
            "eating_habits":profile.user_lifestyle_details.food_habits.value if profile.user_lifestyle_details.food_habits != None else None,
            "drinking_habits":profile.user_lifestyle_details.drinking_habits.value if profile.user_lifestyle_details.drinking_habits != None else None,
            "smoking_habits":profile.user_lifestyle_details.smoking_habits.value if profile.user_lifestyle_details.smoking_habits != None else None,
            "hobbies":profile.user_lifestyle_details.hobbies['hobbies'] if profile.user_lifestyle_details.hobbies != None else None,
            "interests":profile.user_lifestyle_details.interests['interests'] if profile.user_lifestyle_details.interests != None else None,
            "favourite_music":profile.user_lifestyle_details.interests['favourite_music'] if profile.user_lifestyle_details.interests != None else None,
            "favourite_books":profile.user_lifestyle_details.favourite_books['favourite_books'] if profile.user_lifestyle_details.favourite_books != None else None,
            "preffered_movies":profile.user_lifestyle_details.preffered_movies['preffered_movies'] if profile.user_lifestyle_details.preffered_movies != None else None,
            "sports_activities":profile.user_lifestyle_details.sports_activities['sports_activities'] if profile.user_lifestyle_details.sports_activities != None else None,
            "fitness_activities":profile.user_lifestyle_details.fitness_activities['fitness_activities'] if profile.user_lifestyle_details.fitness_activities != None else None,
            "favourite_cuisine":profile.user_lifestyle_details.favourite_cuisine['favourite_cuisine'] if profile.user_lifestyle_details.favourite_cuisine != None else None,
            "preferred_dress_style":profile.user_lifestyle_details.preferred_dress_style['preferred_dress_style'] if profile.user_lifestyle_details.preferred_dress_style != None else None,
            "spoken_languages":profile.user_lifestyle_details.spoken_languages['spoken_languages'] if profile.user_lifestyle_details.spoken_languages != None else None,
        }
    else:
        user_lifestyle_dict={
            "eating_habits" : None,
            "drinking_habits":None,
            "smoking_habits":None,
            "hobbies":None,
            "interests":None,
            "favourite_music":None,
            "favourite_books":None,
            "preffered_movies":None,
            "sports_activities":None,
            "fitness_activities":None,
            "favourite_cuisine":None,
            "preferred_dress_style":None,
            "spoken_languages":None,
        }
    user_data={
            "id": profile.id,
            "name": profile.name,
            "basic_details":basic_details_dict,
            "user_religion":user_religion_dict,
            "user_location":user_location_dict,
            "user_profession":user_professional_dict,
            "siblings_details":siblings_details_dict,
            "family_details":family_details_dict,
            "user_lifestyle":user_lifestyle_dict,
            },

    resp.append(user_data)
    return resp

def get_partner_details_byid(db: Session, partner_id: int, user_id: int):
    user = get_user_by_id(db, partner_id)
    me_user = get_user_by_id(db, user_id)
    resp = []
    if user!=None:
        dobdate=user.dob
        year, age =calculate_age_month(dobdate)
        agedesc = str(year) + " years and " + str(age) + " months"
        social=[]
        if user.user_socialmedia_details == None:
            link=None
            social_media_profile=None
        else:
            social_media = db.query(UserSocialMediaProfiles).filter(UserSocialMediaProfiles.user_id == user.id,UserSocialMediaProfiles.is_deleted == False).all()
            for i in social_media:
                link=i.link
                social_media_profile=i.social_media_profile.value
                social_media_dict={"link":link,"social_media_profile":social_media_profile}
                social.append(social_media_dict)
        questionlist=[]
        if user.user_profile_questions == None:
            questions=None
            answer=None
        else:
            profile_questions = db.query(UserProfileQuestions).filter(UserProfileQuestions.user_id == user.id,UserProfileQuestions.is_deleted == False).all()
            if profile_questions:
                for k in profile_questions:
                    questions=k.question
                    answer=k.answer
                    profile_questions_dict={"question":questions,"answer":answer}
                    questionlist.append(profile_questions_dict)
        if user.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
            body_type=None
        else:
            profile_created_by=user.user_basic_details.profile_created_for.value
            if profile_created_by=="Daughter" or profile_created_by=="Son":
                profile_created_by="parent"
            height_in_ft=user.user_basic_details.height_in_ft
            weight=user.user_basic_details.weight
            mother_tongue=user.user_basic_details.mother_tongue.value if user.user_basic_details.mother_tongue != None else None
            marital_status=user.user_basic_details.marital_status.value if user.user_basic_details.marital_status != None else None
            about_me=user.user_basic_details.about_me
            body_type=user.user_basic_details.body_type.value if user.user_basic_details.body_type != None else None
        if user.user_location_details == None:
            city=None
            district=None
            state=None
            ancestral_origin=None
        else:
            city=user.user_location_details.city
            district=user.user_location_details.district
            state=user.user_location_details.state
            ancestral_origin=user.user_location_details.ancestral_origin
        if user.user_family_details == None:
            family_type=None
            family_values=None
            family_status=None
            living_status=None
            description=None
            father_name=None
            father_occupation=None
            mother_name=None
            mother_occupation=None
        else:
            family_type=user.user_family_details.family_type.value if user.user_family_details.family_type != None else None
            family_values=user.user_family_details.family_values.value if user.user_family_details.family_values != None else None
            family_status=user.user_family_details.family_status.value if user.user_family_details.family_status != None else None
            living_status=user.user_family_details.living_status.value if user.user_family_details.living_status != None else None
            description=user.user_family_details.description
            father_name=user.user_family_details.father_name
            # father_occupation=user.user_family_details.father_occupation.occupation_name
            father_occupation = None
            if user.user_family_details.father_occupation != None:
                father_occupation = user.user_family_details.father_occupation.occupation_name
            mother_name=user.user_family_details.mother_name
            # mother_occupation=user.user_family_details.mother_occupation.occupation_name
            mother_occupation=None
            if user.user_family_details.father_occupation != None:
                mother_occupation = user.user_family_details.mother_occupation.occupation_name
        if user.user_religion_details==None:
            level_one= None
            level_two = None
            level_three = None
            star_name= None
            raasi=None
            dosham= None
            gothram= None
            place_of_birth= None
            time_of_birth= None
        else:
            level_one= user.user_religion_details.religion.level_one if user.user_religion_details.religion.level_one else None
            level_two = user.user_religion_details.religion.level_two if user.user_religion_details.religion.level_two else None
            level_three=user.user_religion_details.religion.level_three if user.user_religion_details.religion.level_three else None
            star_name= user.user_religion_details.star.star_name if user.user_religion_details.star else None
            raasi=user.user_religion_details.raasi
            dosham=user.user_religion_details.dosham.value if user.user_religion_details.dosham else None
            gothram=user.user_religion_details.gothram
            place_of_birth=user.user_religion_details.place_of_birth
            time_of_birth=user.user_religion_details.time_of_birth
        if user.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            organization=None
            annual_income=None
            currency_type=None
            citizenship=None
        else:
            qualification= user.user_professional_details.highest_education.qualification
            employed_in=user.user_professional_details.employed_in.value if user.user_professional_details.employed_in else None
            occupation_name= user.user_professional_details.occupation.occupation_name if user.user_professional_details.occupation else None
            organization=user.user_professional_details.organization
            annual_income= user.user_professional_details.annual_income.value if user.user_professional_details.annual_income else None
            currency_type=user.user_professional_details.currency_type.value if user.user_professional_details.currency_type else None
            citizenship=user.user_professional_details.citizenship
        if user.user_lifestyle_details==None:
            smoking_habits=None
            drinking_habits=None
            food_habits=None
            hobbies=None
            interests=None
            favourite_music=None
            favourite_books=None
            preffered_movies=None
            sports_activities=None
            fitness_activities=None
            favourite_cuisine=None
            preferred_dress_style=None
            spoken_languages=None
        else:
            smoking_habits= user.user_lifestyle_details.smoking_habits.value if user.user_lifestyle_details.smoking_habits != None else None
            drinking_habits=user.user_lifestyle_details.drinking_habits.value if user.user_lifestyle_details.drinking_habits != None else None
            food_habits= user.user_lifestyle_details.food_habits.value if user.user_lifestyle_details.food_habits != None else None
            print(user.user_lifestyle_details.hobbies['hobbies'],"uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu")
            hobbies= user.user_lifestyle_details.hobbies['hobbies'] if user.user_lifestyle_details.hobbies != None else None
            interests= user.user_lifestyle_details.interests['interests'] if user.user_lifestyle_details.interests != None else None
            favourite_music= user.user_lifestyle_details.favourite_music['favourite_music'] if user.user_lifestyle_details.favourite_music != None else None
            favourite_books= user.user_lifestyle_details.favourite_books['favourite_books'] if user.user_lifestyle_details.favourite_books != None else None
            preffered_movies= user.user_lifestyle_details.preffered_movies['preffered_movies'] if user.user_lifestyle_details.preffered_movies != None else None
            sports_activities= user.user_lifestyle_details.sports_activities['sports_activities'] if user.user_lifestyle_details.sports_activities != None else None
            fitness_activities= user.user_lifestyle_details.fitness_activities['fitness_activities'] if user.user_lifestyle_details.fitness_activities != None else None
            favourite_cuisine= user.user_lifestyle_details.favourite_cuisine['favourite_cuisine'] if user.user_lifestyle_details.favourite_cuisine != None else None
            preferred_dress_style= user.user_lifestyle_details.preferred_dress_style['preferred_dress_style'] if user.user_lifestyle_details.preferred_dress_style != None else None
            spoken_languages= user.user_lifestyle_details.spoken_languages['spoken_languages'] if user.user_lifestyle_details.spoken_languages != None else None
        if user.is_documents_verified:
            docs = db.query(Documents).filter(Documents.user_id == user_id,Documents.status == "approved").first()
            if docs!=None:
                doc_type=docs.doc_type.value
                documents_verified="Verified"
            else:
                doc_type="Not uploaded"
                documents_verified="Not verified"
        else:
            doc_type="Not uploaded"
            documents_verified="Not verified"
        siblings=[]
        if user.user_siblings_details==None:
            siblings=None
        else:
            sibling = db.query(UserSiblingsDetails).filter(UserSiblingsDetails.user_id == user_id).all()
            # if siblings:
            for s in sibling:
                siblings_type=s.siblings_type
                relation=s.relation
                marital_status_siblings=s.marital_status
                count=s.count
                siblingsdict={"siblings_type":siblings_type,"relation":relation,"marital_status_siblings":marital_status_siblings,"count":count}
                siblings.append(siblingsdict)
        is_interest_send = False
        if len(check_partner_interest_already_send(db,user.id,me_user.id))>0:
            is_interest_send = True
        match_dict, total,score = get_match_score(db,user,me_user)
        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        premium = False
        try:
            # premium = user.user_subscribed[0].package_log.is_premium
            for pack in user.user_subscribed:
                if pack.expiry_date:
                    if pack.package_log.is_premium == True and pack.expiry_date >= datetime.today() and pack.subscription_status == "completed":
                        premium = True
                        break
        except IndexError:
            premium = False
        db_interest = db.query(PartnerInterests).filter(PartnerInterests.from_user_id==user.id,PartnerInterests.to_user_id==me_user.id,PartnerInterests.is_deleted==False).first()
        if db_interest!=None:
            db_interest.is_viewed=True
            db.add(db_interest)
            db.commit()
            db.refresh(db_interest)
        db_shortlist = db.query(ProfileShortlist).filter(ProfileShortlist.from_user_id==user.id,ProfileShortlist.to_user_id==me_user.id,ProfileShortlist.is_deleted==False).first()
        if db_shortlist!=None:
            db_shortlist.is_viewed=True
            db.add(db_shortlist)
            db.commit()
            db.refresh(db_shortlist)
        db_profilevisit = db.query(ProfileVisits).filter(ProfileVisits.from_user_id==me_user.id,ProfileVisits.to_user_id==user.id).first()
        if db_profilevisit!=None:
            db_profilevisit.visit_count=db_profilevisit.visit_count + 1
            db.add(db_profilevisit)
            db.commit()
            db.refresh(db_profilevisit)
        else:
            db_visit = ProfileVisits(from_user_id = me_user.id, from_user=me_user, to_user_id=user.id, to_user=user, visit_count=1)
            db.add(db_visit)
            db.commit()
            db.refresh(db_visit)
        partner_viewed_date =None
        db_profilevisit_status = db.query(ProfileVisits).filter(ProfileVisits.from_user_id==user.id,ProfileVisits.to_user_id==me_user.id).first()
        if db_profilevisit_status!=None:
            db_profilevisit_status.is_viewed=True
            db.add(db_profilevisit_status)
            db.commit()
            db.refresh(db_profilevisit_status)
            partner_viewed_date = db_profilevisit_status.created_on
        db_interest = db.query(PartnerInterests).filter(PartnerInterests.from_user_id==me_user.id,PartnerInterests.to_user_id==user.id,PartnerInterests.is_deleted==False).order_by(PartnerInterests.id.desc()).first()
        interest_status=None
        if db_interest:
            interest_status=db_interest.status
        image_additional = get_partner_all_images(db, me_user, user)
        question_add=question_add_status(db, user)
        #privacy option
        photo_privacy_option=None
        phonenumber_privacy_option=None
        db_privacy_photo = db.query(UserPrivacyOption).filter(UserPrivacyOption.user_id==user.id,UserPrivacyOption.type=="photo",UserPrivacyOption.is_deleted==False).first()
        db_privacy_phone = db.query(UserPrivacyOption).filter(UserPrivacyOption.user_id==user.id,UserPrivacyOption.type=="phone_number",UserPrivacyOption.is_deleted==False).first()
        if db_privacy_photo:
            photo_privacy_option=db_privacy_photo.privacy_option.value
        if db_privacy_phone:
            phonenumber_privacy_option=db_privacy_phone.privacy_option.value

        #photo and number request
        photo_request=None
        phonenumber_request=None
        db_photo_request = db.query(PhotoRequests).filter(PhotoRequests.from_user_id==me_user.id,PhotoRequests.to_user_id==user.id,PhotoRequests.is_deleted==False).first()
        db_phonenumber_request = db.query(NumberRequests).filter(NumberRequests.from_user_id==me_user.id,NumberRequests.to_user_id==user.id,NumberRequests.is_deleted==False).first()
        print(db_photo_request,"llllllllllll")
        print(db_phonenumber_request,"ooooooooooooooooooo")
        if db_photo_request:
            photo_request_status=db_photo_request.status.value
            print(photo_request_status,"jjjjjjjjjjjjjjjjjjjjjjjjjjj")
            if photo_request_status=="accepted":
                photo_request=True
            else:
                photo_request=False
        if db_phonenumber_request:
            phonenumber_request_status=db_phonenumber_request.status.value
            print(phonenumber_request_status,"uuuuuuuuuuuuuuuuuuuuuuuu")
            if phonenumber_request_status=="accepted":
                phonenumber_request=True
            else:
                phonenumber_request=False

        pr ={
            "id": user.id,
            "name": user.name,
            "gender": user.gender,
            "phone_number": user.phone_number,
            "profile_created_by": profile_created_by,
            "is_interest_send":is_interest_send,
            "interest_status":interest_status,
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":{"doc_type":doc_type,"is_documents_verified": documents_verified}},
            "premium":premium,
            "photo_privacy_option":photo_privacy_option,
            "phonenumber_privacy_option":phonenumber_privacy_option,
            "image_list":image_additional,
            "social_media_details": social,
            "user_profile_questions": questionlist,
            "is_viewed_you": partner_viewed_date,
            "question_add": question_add,
            "photo_request": photo_request,
            "phonenumber_request": phonenumber_request,
            "details": {
                "age": agedesc,
                "height_in_ft": height_in_ft,
                "weight":weight,
                "mother_tongue": mother_tongue,
                "religion_name": level_one,
                "caste_name": level_two,
                "star_name": star_name,
                "raasi": raasi,
                "state":state ,
                "district":district ,
                "city": city,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type,
            },
            "about":{
                    "description":about_me,
                    "details":[
                        {"type":"age","data": agedesc},
                        {"type":"height_in_ft","data":height_in_ft},
                        {"type":"weight","data":weight,},
                        {"type":"body_type","data":body_type},
                        {"type":"marital","data":marital_status},
                        {"type":"state","data":state},
                        {"type":"location","data":district},
                        {"type":"location","data":city},
                        {"type":"ancestral","data":ancestral_origin},
                        {"type":"phone","data":user.phone_number},       # pleaseeee doooooooooooooooo
                        {"type":"smoking","data":smoking_habits},
                        {"type":"drinking","data":drinking_habits},
                        {"type":"food","data":food_habits},
                        {"type":"citizen","data":citizenship},
                    ]
                    },
            "professional_details":{
                        "employed_in": employed_in,
                        "qualification": qualification,
                        "occupation":occupation_name,
                        "organization":organization,
                        "annual_income":annual_income,
                        "currency_type":currency_type,
                        },
            "religious_background":{
                        "level_one": level_one,
                        "level_two": level_two,
                        "star_name":star_name,
                        "raasi":raasi,
                        "dosham":dosham,
                        "level_three":level_three,
                        "gothram":gothram,
                        "place_of_birth":place_of_birth,
                        "time_of_birth":time_of_birth,
                        "place_of_birth":place_of_birth,
                        },
            "family_background":{
                        "description":description,
                        "family_type": family_type,
                        "family_values": family_values,
                        "family_status":family_status,
                        "living_status":living_status,
                        "father_name":father_name,
                        "father_occupation":father_occupation,
                        "mother_name":mother_name,
                        "mother_occupation":mother_occupation,
                        "siblings":siblings,
                    },
            "profile_verification":[
                        {"docs_type_verification": {"doc_type":doc_type,"is_verfied":documents_verified},},
                        {"is_number_verified":user.is_number_verified},
                        {"is_photos_verified":user.is_photos_verified},
                    ],
            "hobbies_interests":[
                        {"title":"Smoking", "value":[smoking_habits]},
                        {"title":"Drinking", "value":[drinking_habits]},
                        {"title":"Food", "value":[food_habits]},
                        {"title":"Hobbies", "value":hobbies},
                        {"title":"Interests", "value":interests},
                        {"title":"Favourite music", "value":favourite_music},
                        {"title":"Favourite books", "value":favourite_books},
                        {"title":"Preffered movies", "value":preffered_movies},
                        {"title":"Sports activities", "value":sports_activities},
                        {"title":"Fitness activities", "value":fitness_activities},
                        {"title":"Favourite cuisine", "value":favourite_cuisine},
                        {"title":"Preferred dress_style", "value":preferred_dress_style},
                        {"title":"Spoken languages", "value":spoken_languages},
                    ],
            "created_on": user.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score
        }
        resp.append(pr)
    return resp


def get_personal_profile(db: Session, user_id: int):
    user = get_user_by_id(db, user_id)
    resp = []
    if user!=None:
        dobdate=user.dob
        year,month=calculate_age_month(dobdate)
        agedesc = str(year) + " years and " + str(month) + " months"
        social=[]
        if user.user_socialmedia_details == None:
            link=None
            social_media_profile=None
        else:
            social_media = db.query(UserSocialMediaProfiles).filter(UserSocialMediaProfiles.user_id == user_id,UserSocialMediaProfiles.is_deleted == False).all()
            if social_media:
                for i in social_media:
                    link=i.link
                    social_media_profile=i.social_media_profile.value  if i.social_media_profile != None else None
                    social_media_dict={"link":link,"social_media_profile":social_media_profile}
                    social.append(social_media_dict)
        questionlist=[]
        if user.user_profile_questions == None:
            question=None
            answer=None
        else:
            profile_questions = db.query(UserProfileQuestions).filter(UserProfileQuestions.user_id == user_id,UserProfileQuestions.is_deleted == False).all()
            if profile_questions:
                for k in profile_questions:
                    questions=k.question
                    answer=k.answer
                    profile_questions_dict={"question":questions,"answer":answer}
                    questionlist.append(profile_questions_dict)
        if user.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
            body_type=None
        else:
            profile_created_by=user.user_basic_details.profile_created_for.value
            if profile_created_by=="Daughter" or profile_created_by=="Son":
                profile_created_by="parent"
            height_in_ft=user.user_basic_details.height_in_ft
            weight=user.user_basic_details.weight
            mother_tongue=user.user_basic_details.mother_tongue.value if user.user_basic_details.mother_tongue != None else None
            marital_status=user.user_basic_details.marital_status.value if user.user_basic_details.marital_status != None else None
            about_me=user.user_basic_details.about_me
            body_type=user.user_basic_details.body_type.value if user.user_basic_details.body_type != None else None
        if user.user_location_details == None:
            city=None
            district=None
            state=None
            ancestral_origin=None
        else:
            city=user.user_location_details.city
            district=user.user_location_details.district
            state=user.user_location_details.state
            ancestral_origin=user.user_location_details.ancestral_origin
        if user.user_family_details == None:
            family_type=None
            family_values=None
            family_status=None
            living_status=None
            description=None
            father_name=None
            father_occupation=None
            mother_name=None
            mother_occupation=None
        else:
            family_type=user.user_family_details.family_type.value if user.user_family_details.family_type != None else None
            family_values=user.user_family_details.family_values.value if user.user_family_details.family_values != None else None
            family_status=user.user_family_details.family_status.value if user.user_family_details.family_status != None else None
            living_status=user.user_family_details.living_status.value if user.user_family_details.living_status != None else None
            description=user.user_family_details.description
            father_name=user.user_family_details.father_name
            father_occupation=user.user_family_details.father_occupation.occupation_name if user.user_family_details.father_occupation != None else None
            mother_name=user.user_family_details.mother_name
            mother_occupation=user.user_family_details.mother_occupation.occupation_name if user.user_family_details.mother_occupation!= None else None
        if user.user_religion_details==None:
            level_one= None
            level_two = None
            level_three = None
            star_name= None
            raasi=None
            dosham= None
            gothram= None
            place_of_birth= None
            time_of_birth= None
        else:
            level_one= user.user_religion_details.religion.level_one if user.user_religion_details.religion.level_one else None
            level_two = user.user_religion_details.religion.level_two if user.user_religion_details.religion.level_two else None
            level_three=user.user_religion_details.religion.level_three if user.user_religion_details.religion.level_three else None
            star_name= user.user_religion_details.star.star_name if user.user_religion_details.star else None
            raasi=user.user_religion_details.raasi
            dosham=user.user_religion_details.dosham.value if user.user_religion_details.dosham else None
            gothram=user.user_religion_details.gothram
            place_of_birth=user.user_religion_details.place_of_birth
            time_of_birth=user.user_religion_details.time_of_birth
        if user.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            organization=None
            annual_income=None
            currency_type=None
            citizenship=None
        else:
            qualification= user.user_professional_details.highest_education.qualification if user.user_professional_details.highest_education != None else None
            employed_in=user.user_professional_details.employed_in.value if user.user_professional_details.employed_in else None
            occupation_name= user.user_professional_details.occupation.occupation_name if user.user_professional_details.occupation else None
            organization=user.user_professional_details.organization
            annual_income= user.user_professional_details.annual_income.value if user.user_professional_details.annual_income else None
            currency_type=user.user_professional_details.currency_type.value if user.user_professional_details.currency_type else None
            citizenship=user.user_professional_details.citizenship
        if user.user_lifestyle_details==None:
            smoking_habits=None
            drinking_habits=None
            food_habits=None
            hobbies=None
            interests=None
            favourite_music=None
            favourite_books=None
            preffered_movies=None
            sports_activities=None
            fitness_activities=None
            favourite_cuisine=None
            preferred_dress_style=None
            spoken_languages=None
        else:
            smoking_habits= user.user_lifestyle_details.smoking_habits.value if user.user_lifestyle_details.smoking_habits != None else None
            drinking_habits=user.user_lifestyle_details.drinking_habits.value if user.user_lifestyle_details.drinking_habits != None else None
            food_habits= user.user_lifestyle_details.food_habits.value if user.user_lifestyle_details.food_habits != None else None
            hobbies= user.user_lifestyle_details.hobbies['hobbies'] if user.user_lifestyle_details.hobbies != None else None
            interests= user.user_lifestyle_details.interests['interests'] if user.user_lifestyle_details.interests != None else None
            favourite_music= user.user_lifestyle_details.favourite_music['favourite_music'] if user.user_lifestyle_details.favourite_music != None else None
            favourite_books= user.user_lifestyle_details.favourite_books['favourite_books'] if user.user_lifestyle_details.favourite_books != None else None
            preffered_movies= user.user_lifestyle_details.preffered_movies['preffered_movies'] if user.user_lifestyle_details.preffered_movies != None else None
            sports_activities= user.user_lifestyle_details.sports_activities['sports_activities'] if user.user_lifestyle_details.sports_activities != None else None
            fitness_activities= user.user_lifestyle_details.fitness_activities['fitness_activities'] if user.user_lifestyle_details.fitness_activities != None else None
            favourite_cuisine= user.user_lifestyle_details.favourite_cuisine['favourite_cuisine'] if user.user_lifestyle_details.favourite_cuisine != None else None
            preferred_dress_style= user.user_lifestyle_details.preferred_dress_style['preferred_dress_style'] if user.user_lifestyle_details.preferred_dress_style != None else None
            spoken_languages= user.user_lifestyle_details.spoken_languages['spoken_languages'] if user.user_lifestyle_details.spoken_languages != None else None
        if user.is_documents_verified:
            docs = db.query(Documents).filter(Documents.user_id == user_id,Documents.status == "approved").first()
            doc_type=docs.doc_type.value
            documents_verified="Verified"
        else:
            doc_type="Not uploaded"
            documents_verified="Not verified"
        siblings=[]
        if user.user_siblings_details==None:
            siblings=None
        else:
            sibling = db.query(UserSiblingsDetails).filter(UserSiblingsDetails.user_id == user_id,UserSiblingsDetails.is_deleted == False).all()
            # if siblings:
            for s in sibling:
                siblings_type=s.siblings_type
                relation=s.relation
                marital_status_siblings=s.marital_status
                count=s.count
                siblingsdict={"siblings_type":siblings_type,"relation":relation,"marital_status_siblings":marital_status_siblings,"count":count}
                siblings.append(siblingsdict)
        premium = False
        image_additional = get_current_user_all_images(user)
        try:
            for pack in user.user_subscribed:
                if pack.expiry_date:
                    import datetime as dd
                    if pack.package_log.is_premium == True and pack.expiry_date >= dd.date.today() and pack.subscription_status == "completed":
                        premium = True
                        break
        except IndexError:
            premium = False
        pr ={
            "id": user.id,
            "name": user.name,
            "gender": user.gender,
            "profile_created_by": profile_created_by,
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":{"doc_type":doc_type,"is_documents_verified": documents_verified}},
            "premium": premium,
            "image_list": image_additional,
            "social_media_details": social,
            "user_profile_questions": questionlist,
            "details": {
                "age": agedesc,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "religion_name": level_one,
                "caste_name": level_two,
                "star_name": star_name,
                "raasi": raasi,
                "state":state ,
                "district":district ,
                "city": city,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type,
            },
            "about":{
                    "description":about_me,
                    "details":[
                        {"type":"age","data": agedesc},
                        {"type":"height_in_ft","data":height_in_ft},
                        {"type":"weight","data":weight,},
                        {"type":"body_type","data":body_type},
                        {"type":"marital","data":marital_status},
                        {"type":"state","data":state},
                        {"type":"location","data":district},
                        {"type":"location","data":city},
                        {"type":"ancestral","data":ancestral_origin},
                        {"type":"phone","data":"phone"},
                        {"type":"smoking","data":smoking_habits},
                        {"type":"drinking","data":drinking_habits},
                        {"type":"food","data":food_habits},
                        {"type":"citizen","data":citizenship},
                    ]
                    },
            "professional_details":{
                        "employed_in": employed_in,
                        "qualification": qualification,
                        "occupation":occupation_name,
                        "organization":organization,
                        "annual_income":annual_income,
                        "currency_type":currency_type,
                        },
            "religious_background":{
                        "level_one": level_one,
                        "level_two": level_two,
                        "star_name":star_name,
                        "raasi":raasi,
                        "dosham":dosham,
                        "level_three":level_three,
                        "gothram":gothram,
                        "place_of_birth":place_of_birth,
                        "time_of_birth":time_of_birth,
                        "place_of_birth":place_of_birth,
                        },
            "family_background":{
                        "description":description,
                        "family_type": family_type,
                        "family_values": family_values,
                        "family_status":family_status,
                        "living_status":living_status,
                        "father_name":father_name,
                        "father_occupation":father_occupation,
                        "mother_name":mother_name,
                        "mother_occupation":mother_occupation,
                        "siblings":siblings,
                    },
            "profile_verification":[
                        {"docs_type_verification": {"doc_type":doc_type,"is_verfied":documents_verified},},
                        {"is_number_verified":user.is_number_verified},
                        {"is_photos_verified":user.is_photos_verified},
                    ],
            "hobbies_interests":[
                        {"title":"Smoking", "value":[smoking_habits]},
                        {"title":"Drinking", "value":[drinking_habits]},
                        {"title":"Food", "value":[food_habits]},
                        {"title":"Hobbies", "value":[hobbies]},
                        {"title":"Interests", "value":[interests]},
                        {"title":"Favourite music", "value":[favourite_music]},
                        {"title":"Favourite books", "value":[favourite_books]},
                        {"title":"Preffered movies", "value":[preffered_movies]},
                        {"title":"Sports activities", "value":[sports_activities]},
                        {"title":"Fitness activities", "value":[fitness_activities]},
                        {"title":"Favourite cuisine", "value":[favourite_cuisine]},
                        {"title":"Preferred dress_style", "value":[preferred_dress_style]},
                        {"title":"Spoken languages", "value":[spoken_languages]},
                    ],
            "created_on": user.date_joined,
        }
        resp.append(pr)
    return resp



def get_level_two_bytype(db: Session, typeid: str):
    leveltwo=db.query(Religion.level_two).distinct(Religion.level_two).filter(Religion.level_one == typeid).all()
    return leveltwo

def get_level_three_byname(db: Session, leveltwoname: Subcastelist):
    levelthree=db.query(Religion.level_three).distinct(Religion.level_three).filter(Religion.level_two.in_(leveltwoname.idsubcaste)).all()
    return levelthree

def get_edit_data(db: Session, user_id: int):
    user = get_user_by_id(db, user_id)
    profile=db.query(User).filter(User.id==user_id).first()
    resp = []
    if profile.user_location_details == None:
        city,district,state,country,citizenship=None,None,None,None,None
    else:
        city=profile.user_location_details.city
        district=profile.user_location_details.district
        state=profile.user_location_details.state
        country=profile.user_location_details.country
        citizenship=profile.user_location_details.citizenship

    if profile.user_religion_details==None:
        level_one,level_two,level_three,star,place_of_birth,place_of_birth,time_of_birth,raasi,dosham,gothram= None,None,None,None,None,None,None,None,None,None
    else:
        level_one= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
        level_two = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
        level_three= profile.user_religion_details.religion.level_three if profile.user_religion_details.religion  else None
        star=profile.user_religion_details.star.id if profile.user_religion_details.star else None
        gothram=profile.user_religion_details.gothram
        raasi=profile.user_religion_details.raasi
        dosham=profile.user_religion_details.dosham.value if profile.user_religion_details.dosham else None
        place_of_birth=profile.user_religion_details.place_of_birth
        time_of_birth=profile.user_religion_details.time_of_birth

    if profile.user_professional_details==None:
        qualification,employed_in,occupation_name,annual_income,currency_type,work_location,role,organization,work_state,work_citizenship,residential_status,institutions=None,None,None,None,None,None,None,None,None,None,None,None
    else:
        qualification= profile.user_professional_details.highest_education.id if profile.user_professional_details.highest_education else None
        employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
        occupation_name= profile.user_professional_details.occupation.id if profile.user_professional_details.occupation else None
        area_of_specialization= profile.user_professional_details.area_of_specialization.id if profile.user_professional_details.area_of_specialization else None
        annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
        currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None
        work_location=profile.user_professional_details.work_location
        role=profile.user_professional_details.role
        organization=profile.user_professional_details.organization
        work_state=profile.user_professional_details.state
        work_city=profile.user_professional_details.city
        work_citizenship=profile.user_professional_details.citizenship
        institutions=profile.user_professional_details.institutions
        residential_status=profile.user_professional_details.residential_status.value if profile.user_professional_details.residential_status else None

    if profile.user_lifestyle_details != None:
        eating_habits=profile.user_lifestyle_details.food_habits.value if profile.user_lifestyle_details.food_habits != None else None
        drinking_habits=profile.user_lifestyle_details.drinking_habits.value if profile.user_lifestyle_details.drinking_habits != None else None
        smoking_habits=profile.user_lifestyle_details.smoking_habits.value if profile.user_lifestyle_details.smoking_habits != None else None
        hobbies=profile.user_lifestyle_details.hobbies['hobbies'] if profile.user_lifestyle_details.hobbies != None else []
        interests= profile.user_lifestyle_details.interests['interests'] if profile.user_lifestyle_details.interests != None else []
        favourite_music=profile.user_lifestyle_details.favourite_music['favourite_music'] if profile.user_lifestyle_details.interests  else []
        favourite_books=profile.user_lifestyle_details.favourite_books['favourite_books'] if profile.user_lifestyle_details.favourite_books != None else []
        preffered_movies=profile.user_lifestyle_details.preffered_movies['preffered_movies'] if profile.user_lifestyle_details.preffered_movies != None else []
        sports_activities=profile.user_lifestyle_details.sports_activities['sports_activities'] if profile.user_lifestyle_details.sports_activities != None else []
        fitness_activities=profile.user_lifestyle_details.fitness_activities['fitness_activities'] if profile.user_lifestyle_details.fitness_activities else []
        # print(profile.user_lifestyle_details.fitness_activities['fitness_activities'],"...............//>>")
        # print(profile.user_lifestyle_details.fitness_activities,"......................>>>>>>>////////>>>>>>>>>>")
        favourite_cuisine=profile.user_lifestyle_details.favourite_cuisine['favourite_cuisine'] if profile.user_lifestyle_details.favourite_cuisine != None else []
        preferred_dress_style=profile.user_lifestyle_details.preferred_dress_style['preferred_dress_style'] if profile.user_lifestyle_details.preferred_dress_style != None else []
        spoken_languages=profile.user_lifestyle_details.spoken_languages['spoken_languages'] if profile.user_lifestyle_details.spoken_languages != None else []
    else:
        eating_habits,smoking_habits,drinking_habits,hobbies,interests,favourite_music,favourite_books,preffered_movies,sports_activities,fitness_activities,favourite_cuisine,preferred_dress_style,spoken_languages=None,None,None,[],[],[],[],[],[],[],[],[],[]
    if user.user_family_details == None:
        family_type,family_values,family_status,living_status,description,father_name,father_occupation,mother_name,mother_occupation=None,None,None,None,None,None,None,None,None
    else:
        family_type=user.user_family_details.family_type.value if user.user_family_details.family_type != None else None
        family_values=user.user_family_details.family_values.value if user.user_family_details.family_values != None else None
        family_status=user.user_family_details.family_status.value if user.user_family_details.family_status != None else None
        living_status=user.user_family_details.living_status.value if user.user_family_details.living_status != None else None
        description=user.user_family_details.description
        father_name=user.user_family_details.father_name
        father_occupation=user.user_family_details.father_occupation_id
        mother_name=user.user_family_details.mother_name
        mother_occupation=user.user_family_details.mother_occupation_id
    siblings=[]
    if user.user_siblings_details==None:
        siblings=None
    else:
        siblingsq = db.query(UserSiblingsDetails).filter(UserSiblingsDetails.user_id == user_id,UserSiblingsDetails.is_deleted == False).all()
        if len(siblingsq) > 0:
            for k in siblingsq:
                siblings_type = k.siblings_type
                relation=k.relation
                marital_status_siblings=k.marital_status
                count=k.count
                siblingsdict={"id":k.id,"count":count,"marital_status":marital_status_siblings.value if marital_status_siblings else None,"relation":relation.value if relation else None,"siblings_type":siblings_type.value if siblings_type else None}
                siblings.append(siblingsdict)
        social=[]
        if user.user_socialmedia_details == None:
            link=None
            social_media_profile=None
        else:
            social_media = db.query(UserSocialMediaProfiles).filter(UserSocialMediaProfiles.user_id == user_id,UserSocialMediaProfiles.is_deleted == False).all()
            if social_media:
                for i in social_media:
                    link=i.link
                    social_media_profile=i.social_media_profile.value
                    social_media_dict={"id":i.id,"link":link,"social_media_profile":social_media_profile}
                    social.append(social_media_dict)
        questionlist=[]
        if user.user_profile_questions == None:
            questions=None
            answer=None
        else:
            profile_questions = db.query(UserProfileQuestions).filter(UserProfileQuestions.user_id == user_id,UserProfileQuestions.is_deleted == False).all()
            if profile_questions:
                for k in profile_questions:
                    questions=k.question
                    answer=k.answer
                    profile_questions_dict={"id":k.id,"question":questions,"answer":answer}
                    questionlist.append(profile_questions_dict)

    print(profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue  else None,"============")
    pr ={
    "user_main": {
        "name":profile.name,
        "email": profile.email,
        "gender": profile.gender,
        "dob": profile.dob,
    },
    "user_basic": {
        "about_me": profile.user_basic_details.about_me,
        "height": profile.user_basic_details.height_in_cm,
        "weight": profile.user_basic_details.weight,
        "skin_color": profile.user_basic_details.skin_color.value if profile.user_basic_details.skin_color else None,
        "have_child":"Yes" if profile.user_basic_details.have_child is True else "No" ,
        "physical_status": profile.user_basic_details.physical_status.value if profile.user_basic_details.physical_status  else None,
        "get_married_in": profile.user_basic_details.get_married_in.value if profile.user_basic_details.get_married_in  else None,
        "marital_status": profile.user_basic_details.marital_status.value if profile.user_basic_details.marital_status  else None,
        "body_type": profile.user_basic_details.body_type.value if profile.user_basic_details.body_type else None,
        "profile_created_for": profile.user_basic_details.profile_created_for.value,
        "mother_tounge": profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue  else None
    },
    "user_religion": {
        "level_one":level_one,
        "level_two":level_two,
        "level_three": level_three,
        "star_id":star ,
        "gothram":gothram,
        "raasi": raasi,
        "time_of_birth": time_of_birth,
        "place_of_birth": place_of_birth,
        "dosham": dosham
    },
    "user_location": {
        "country": country,
        "state": state,
        "city": city,
        "district": district,
        "citizenship":citizenship
    },
    "user_profession": {
        "highest_education_id": qualification,
        "organization": organization,
        "occupation_id":occupation_name,
        "area_of_specialization_id":area_of_specialization,
        "employed_in": employed_in,
        "role": role,
        "currency_type": currency_type,
        "annual_income": annual_income,
        "work_location": work_location,
        "work_state": work_state,
        "work_city": work_city,
        "work_citizenship": work_citizenship,
        "institutions":institutions,
        "residential_status": residential_status,
    },
    "user_family": {
        "family_type":family_type,
        "family_values": family_values,
        "family_status": family_status,
        "living_status": living_status,
        "description": description,
        "father_name": father_name,
        "father_occupation_id": father_occupation,
        "mother_occupation_id": mother_occupation,
        "mother_name": mother_name
    },
    "user_siblings": siblings,
    "user_profile_question": questionlist,
    "user_socialmedia": social,
    "user_lifestyle": {
        "hobbies": hobbies,
        "interests": interests,
        "favourite_music": favourite_music,
        "favourite_books":favourite_books,
        "fitness_activities":fitness_activities,
        "preffered_movies": preffered_movies,
        "sports_activities": sports_activities,
        "favourite_cuisine": favourite_cuisine,
        "preffered_dress_style": preferred_dress_style,
        "spoken_languages": spoken_languages,
        "smoking_habits": smoking_habits,
        "drinking_habits": drinking_habits,
        "food_habits": eating_habits
    }
}
    resp.append(pr)
    return resp

def get_user_privacy_details(db: Session, user_id: int):
    return db.query(UserPrivacyOption).filter(UserPrivacyOption.user_id==user_id, UserPrivacyOption.is_deleted==False).all()


def update_user_privacy_details(db: Session, user_id: int, privacy_options: List[PrivacyOptionCreate]):

    for privacy_option in privacy_options:
        is_already_added = db.query(UserPrivacyOption).filter(UserPrivacyOption.user_id==user_id, UserPrivacyOption.is_deleted==False, UserPrivacyOption.type==privacy_option.type).first()
        if is_already_added:
            is_already_added.privacy_option=privacy_option.privacy_option
            db.commit()
        else:
            db_privacy = UserPrivacyOption(type=privacy_option.type, privacy_option=privacy_option.privacy_option, user_id=user_id)
            db.add(db_privacy)
            db.commit()
            db.refresh(db_privacy)
