import string
from sqlalchemy.orm import Session
from app.models.model import *
from app.schemas.profile.profile_manage import *
from app.schemas.profile.profile import *
import config
from app.crud.home.home import *


def get_user_location_details(db: Session, user_id: int):
    return db.query(UserLocationDetails).filter(UserLocationDetails.user_id==user_id).first()


def get_user_socialmedia_by_type(db: Session, user_id: int, social_media: str):
    return db.query(UserSocialMediaProfiles).filter(UserSocialMediaProfiles.user_id==user_id, UserSocialMediaProfiles.social_media_profile==social_media,UserSocialMediaProfiles.is_deleted==False).first()


def get_user_lifestyle(db: Session, user_id: int):
    return db.query(UserLifeStyle).filter(UserLifeStyle.user_id==user_id).first()


def get_user_profile_question_by_ID(db: Session, question_id: int):
    return db.query(UserProfileQuestions).get(question_id)


def get_user_religion_info(db: Session, user_id: int):
    return db.query(UserReligionInfo).filter(UserReligionInfo.user_id==user_id).first()


def get_user_info(db: Session, user_id: int):
    return db.query(User).get(user_id)


def get_user_basic_info(db: Session, user_id: int):
    return db.query(UserBasicDetails).filter(UserBasicDetails.user_id==user_id).first()


def get_user_family_details(db: Session, user_id: int):
    return db.query(UserFamilyDetails).filter(UserFamilyDetails.user_id==user_id).first()


def get_user_siblings_by_ID(db: Session, sibling_id: int):
    return db.query(UserSiblingsDetails).get(sibling_id)


def get_user_professional_details(db: Session, user_id: int):
    return db.query(UserProfessionalDetails).filter(UserProfessionalDetails.user_id==user_id).first()


def update_user_location(db: Session, user_location: UserLocationCreate, user_id: int):
    cur_location = get_user_location_details(db, user_id)
    if cur_location:
        cur_location.country = user_location.country
        cur_location.state = user_location.state
        cur_location.city = user_location.city
        cur_location.district = user_location.district
        cur_location.citizenship = user_location.citizenship
        cur_location.ancestral_origin = user_location.ancestral_origin
        cur_location.longitude = user_location.longitude
        cur_location.latitude = user_location.latitude
        db.commit()
    else:
        db_user_location = UserLocationDetails(country=user_location.country, state=user_location.state, city=user_location.city, district=user_location.district, citizenship=user_location.citizenship, ancestral_origin=user_location.ancestral_origin, longitude=user_location.longitude,latitude=user_location.latitude, user_id=user_id)
        db.add(db_user_location)
        db.commit()
        db.refresh(db_user_location)


def update_user_socialmedia(db: Session, user_socialmedia_list: List[UserSocialMediaCreate], user_id: int):
    social_media_id = [user_socialmedia.id for user_socialmedia in user_socialmedia_list if user_socialmedia.id is not None]
    deleted_medias = db.query(UserSocialMediaProfiles).filter(~UserSocialMediaProfiles.id.in_(social_media_id), UserSocialMediaProfiles.user_id==user_id, UserSocialMediaProfiles.is_deleted==False).all()
    for deleted_media in deleted_medias:
        deleted_media.is_deleted = True
        db.commit()
    for user_socialmedia in user_socialmedia_list:
        if user_socialmedia.id is not None:
            cur_socialmedia = get_user_socialmedia_by_type(db, user_id, user_socialmedia.social_media_profile)
            cur_socialmedia.link = user_socialmedia.link
            db.commit()
        else:
            db_user_socialmedia= UserSocialMediaProfiles(link=user_socialmedia.link, social_media_profile=user_socialmedia.social_media_profile, user_id=user_id)
            db.add(db_user_socialmedia)
            db.commit()
            db.refresh(db_user_socialmedia)


def update_user_lifestyle(db: Session, user_lifestyle: UserLifestyleCreate, user_id: int):
    cur_lifestyle = get_user_lifestyle(db, user_id)
    if cur_lifestyle:
        cur_lifestyle.smoking_habits = user_lifestyle.smoking_habits
        cur_lifestyle.drinking_habits = user_lifestyle.drinking_habits
        cur_lifestyle.food_habits = user_lifestyle.food_habits
        cur_lifestyle.hobbies = {"hobbies": user_lifestyle.hobbies}
        cur_lifestyle.interests = {"interests": user_lifestyle.interests}
        cur_lifestyle.favourite_music = {"favourite_music": user_lifestyle.favourite_music}
        cur_lifestyle.favourite_books = {"favourite_books": user_lifestyle.favourite_books}
        cur_lifestyle.preffered_movies = {"preffered_movies": user_lifestyle.preffered_movies}
        cur_lifestyle.sports_activities = {"sports_activities": user_lifestyle.sports_activities}
        cur_lifestyle.fitness_activities = {"fitness_activities": user_lifestyle.fitness_activities}
        cur_lifestyle.favourite_cuisine = {"favourite_cuisine": user_lifestyle.favourite_cuisine}
        cur_lifestyle.preferred_dress_style = {"preferred_dress_style": user_lifestyle.preffered_dress_style}
        cur_lifestyle.spoken_languages = {"spoken_languages": user_lifestyle.spoken_languages}
        db.commit()
    else:
        db_user_lifestyle = UserLifeStyle(smoking_habits=user_lifestyle.smoking_habits, drinking_habits=user_lifestyle.drinking_habits, food_habits=user_lifestyle.food_habits, hobbies={"hobbies": user_lifestyle.hobbies}, interests={"interests": user_lifestyle.interests}, favourite_music={"favourite_music": user_lifestyle.favourite_music}, favourite_books={"favourite_books": user_lifestyle.favourite_books},preffered_movies={"preffered_movies": user_lifestyle.preffered_movies},sports_activities={"sports_activities": user_lifestyle.sports_activities},fitness_activities={"fitness_activities": user_lifestyle.fitness_activities},favourite_cuisine={"favourite_cuisine": user_lifestyle.favourite_cuisine},preferred_dress_style={"preferred_dress_style": user_lifestyle.preffered_dress_style},spoken_languages={"spoken_languages": user_lifestyle.spoken_languages}, user_id=user_id)
        db.add(db_user_lifestyle)
        db.commit()
        db.refresh(db_user_lifestyle)


def update_user_profile_questions(db: Session, user_profile_question_list: List[UserProfileQuestionCreate], user_id: int):
    profile_questions_id = [user_profile_question.id for user_profile_question in user_profile_question_list if user_profile_question.id is not None]
    deleted_questions = db.query(UserProfileQuestions).filter(~UserProfileQuestions.id.in_(profile_questions_id), UserProfileQuestions.user_id==user_id, UserProfileQuestions.is_deleted==False).all()
    for deleted_question in deleted_questions:
        deleted_question.is_deleted = True
        db.commit()
    for user_profile_quesion in user_profile_question_list:
        if user_profile_quesion.id is not None:
            cur_profile_questions = get_user_profile_question_by_ID(db, user_profile_quesion.id)
            cur_profile_questions.question = user_profile_quesion.question
            cur_profile_questions.answer = user_profile_quesion.answer
            db.commit()
        else:
            db_user_profile_question = UserProfileQuestions(question=user_profile_quesion.question, answer=user_profile_quesion.answer, user_id=user_id)
            db.add(db_user_profile_question)
            db.commit()
            db.refresh(db_user_profile_question)


def update_user_religion_info(db: Session, user_religon_info: UserReligionCreate, user_id: int):
    relgn = None
    if user_religon_info.level_one is not None:
        relgn = db.query(Religion).filter_by(level_one=user_religon_info.level_one)
    if user_religon_info.level_two is not None:
        relgn = relgn.filter_by(level_two=user_religon_info.level_two)
    if user_religon_info.level_three is not None:
        relgn = relgn.filter_by(level_three=user_religon_info.level_three)
    if relgn is not None:
        relgn = relgn.first()
        relgn = relgn.id
    cur_religion_info = get_user_religion_info(db, user_id)
    if cur_religion_info:
        cur_religion_info.dosham = user_religon_info.dosham
        cur_religion_info.raasi = user_religon_info.raasi
        cur_religion_info.gothram = user_religon_info.gothram
        cur_religion_info.place_of_birth = user_religon_info.place_of_birth
        cur_religion_info.time_of_birth = user_religon_info.time_of_birth
        cur_religion_info.star_id = user_religon_info.star_id
        cur_religion_info.religion_id = relgn
        db.commit()
    else:
        db_user_religion_info = UserReligionInfo(dosham=user_religon_info.dosham, raasi=user_religon_info.raasi, gothram=user_religon_info.gothram, place_of_birth=user_religon_info.place_of_birth, time_of_birth=user_religon_info.time_of_birth, star_id=user_religon_info.star_id, religion_id=relgn, user_id=user_id)
        db.add(db_user_religion_info)
        db.commit()
        db.refresh(db_user_religion_info)


def update_user_main(db: Session, user_main: UserCreate, user_id: int):
    if user_id is not None:
        cur_user_main = get_user_info(db, user_id)
        cur_user_main.name = user_main.name
        cur_user_main.email = user_main.email
        cur_user_main.gender = user_main.gender
        cur_user_main.dob = user_main.dob
        db.commit()
    else:
        db_user_main = User(name=user_main.name, email=user_main.email, gender=user_main.gender, dob=user_main.dob, id=user_id)
        db.add(db_user_main)
        db.commit()
        db.refresh(db_user_main)


def update_user_basic_info(db: Session, user_basic_info: UserBasicCreate, user_id: int):
    height_in_feet = None
    if user_basic_info.height:
        height_in_feet = config.cm_to_feet(user_basic_info.height)
    cur_basic_info = get_user_basic_info(db, user_id)
    have_child = user_basic_info.have_child
    if have_child is None:
        have_child = False
    if cur_basic_info:
        cur_basic_info.profile_created_for = user_basic_info.profile_created_for
        cur_basic_info.mother_tongue = user_basic_info.mother_tounge
        cur_basic_info.body_type = user_basic_info.body_type
        cur_basic_info.physical_status = user_basic_info.physical_status
        cur_basic_info.skin_color = user_basic_info.skin_color
        cur_basic_info.height_in_cm = user_basic_info.height
        cur_basic_info.height_in_ft = height_in_feet
        cur_basic_info.weight = user_basic_info.weight
        cur_basic_info.marital_status = user_basic_info.marital_status
        cur_basic_info.get_married_in = user_basic_info.get_married_in
        cur_basic_info.about_me = user_basic_info.about_me
        cur_basic_info.have_child = have_child
        db.commit()
    else:
        db_user_basic_info = UserBasicDetails(profile_created_for=user_basic_info.profile_created_for, mother_tongue=user_basic_info.mother_tounge, body_type=user_basic_info.body_type, physical_status=user_basic_info.physical_status, skin_color=user_basic_info.skin_color, height_in_cm=user_basic_info.height, height_in_ft=height_in_feet,weight=user_basic_info.weight,marital_status=user_basic_info.marital_status,get_married_in=user_basic_info.get_married_in,about_me=user_basic_info.about_me,have_child=user_basic_info.have_child, user_id=user_id)
        db.add(db_user_basic_info)
        db.commit()
        db.refresh(db_user_basic_info)


def update_user_family_details(db: Session, user_family_details: UserFamilyCreate, user_id: int):
    print("===================des", user_family_details.description)
    cur_family_detail = get_user_family_details(db, user_id)
    if cur_family_detail:
        cur_family_detail.family_type = user_family_details.family_type
        cur_family_detail.family_values = user_family_details.family_values
        cur_family_detail.family_status = user_family_details.family_status
        cur_family_detail.living_status = user_family_details.living_status
        cur_family_detail.description = user_family_details.description
        cur_family_detail.father_name = user_family_details.father_name
        cur_family_detail.father_occupation_id = user_family_details.father_occupation_id
        cur_family_detail.mother_name = user_family_details.mother_name
        cur_family_detail.mother_occupation_id = user_family_details.mother_occupation_id
        db.commit()
    else:
        db_user_family_detail = UserFamilyDetails(family_type=user_family_details.family_type, family_values=user_family_details.family_values, family_status=user_family_details.family_status, living_status=user_family_details.living_status, description=user_family_details.description, father_name=user_family_details.father_name, father_occupation_id=user_family_details.father_occupation_id,mother_name=user_family_details.mother_name,mother_occupation_id=user_family_details.mother_occupation_id, user_id=user_id)
        db.add(db_user_family_detail)
        db.commit()
        db.refresh(db_user_family_detail)


def update_user_sibling_details(db: Session, user_sibling_details_list: List[UserSiblingCreate], user_id: int):
    user_sibling_details_id = [user_sibling_details.id for user_sibling_details in user_sibling_details_list if user_sibling_details.id is not None]
    deleted_user_sibling_details = db.query(UserSiblingsDetails).filter(~UserSiblingsDetails.id.in_(user_sibling_details_id), UserSiblingsDetails.user_id==user_id, UserSiblingsDetails.is_deleted==False).all()
    for deleted_user_sibling_detail in deleted_user_sibling_details:
        deleted_user_sibling_detail.is_deleted = True
        db.commit()
    for user_sibling_details in user_sibling_details_list:
        if user_sibling_details.id is not None:
            cur_sibling_details = get_user_siblings_by_ID(db, user_sibling_details.id)
            cur_sibling_details.siblings_type = user_sibling_details.siblings_type
            cur_sibling_details.relation = user_sibling_details.relation
            cur_sibling_details.marital_status = user_sibling_details.marital_status
            cur_sibling_details.count = user_sibling_details.count
            db.commit()
        else:
            db_user_sibling_details = UserSiblingsDetails(siblings_type=user_sibling_details.siblings_type, relation=user_sibling_details.relation,marital_status=user_sibling_details.marital_status,count=user_sibling_details.count, user_id=user_id)
            db.add(db_user_sibling_details)
            db.commit()
            db.refresh(db_user_sibling_details)


def update_user_professional_details(db: Session, user_professional_details: UserProfessionalCreate, user_id: int):

    income = [item for item in config.INCOME_CHOICE_INPUT
          if item[0] == user_professional_details.annual_income]
    income_lower_limit = 0
    income_upper_limit = 0
    if len(income)>0:
        income_lower_limit = config.convert_currency(user_professional_details.currency_type, "INR", income[0][2])
        income_upper_limit = config.convert_currency(user_professional_details.currency_type, "INR", income[0][3])

    cur_professional_detail = get_user_professional_details(db, user_id)
    if cur_professional_detail:
        cur_professional_detail.highest_education_id = user_professional_details.highest_education_id
        cur_professional_detail.institutions = user_professional_details.institutions
        cur_professional_detail.area_of_specialization_id = user_professional_details.area_of_specialization_id
        cur_professional_detail.employed_in = user_professional_details.employed_in
        cur_professional_detail.occupation_id = user_professional_details.occupation_id
        cur_professional_detail.organization = user_professional_details.organization
        cur_professional_detail.role = user_professional_details.role
        cur_professional_detail.currency_type = user_professional_details.currency_type
        cur_professional_detail.annual_income = user_professional_details.annual_income
        cur_professional_detail.annual_income_lower_limit_inr = income_lower_limit
        cur_professional_detail.annual_income_upper_limit_inr = income_upper_limit
        cur_professional_detail.work_location = user_professional_details.work_location
        cur_professional_detail.state = user_professional_details.work_state
        cur_professional_detail.city = user_professional_details.work_city
        cur_professional_detail.citizenship = user_professional_details.work_citizenship
        cur_professional_detail.residential_status = user_professional_details.residential_status
        db.commit()
    else:
        db_user_professional_detail = UserProfessionalDetails(highest_education_id=user_professional_details.highest_education_id, institutions=user_professional_details.institutions, area_of_specialization_id=user_professional_details.area_of_specialization_id, employed_in=user_professional_details.employed_in, occupation_id=user_professional_details.occupation_id, organization=user_professional_details.organization, role=user_professional_details.role,currency_type=user_professional_details.currency_type,annual_income=user_professional_details.annual_income, annual_income_lower_limit_inr=income_lower_limit, annual_income_upper_limit_inr=income_upper_limit, work_location=user_professional_details.work_location, state=user_professional_details.work_state, city=user_professional_details.work_city, citizenship=user_professional_details.work_citizenship, residential_status=user_professional_details.residential_status, user_id=user_id)
        db.add(db_user_professional_detail)
        db.commit()
        db.refresh(db_user_professional_detail)


def Profile_Deactivation(db: Session, reason: DeactivationReason, user_id: int):
    deactivate_profile = UserDeactivation(user_id=user_id, reason=reason.reason)
    db.add(deactivate_profile)
    db.commit()
    db.refresh(deactivate_profile)
    update_sts = db.query(User).filter(User.id == user_id).one()
    update_sts.is_active= False
    db.commit()
    return "profile deactivated"



def get_partner_image(db: Session, from_user: User, to_user: User):
    image = None
    privacy_check = db.query(UserPrivacyOption).filter(UserPrivacyOption.type=="photo", UserPrivacyOption.user_id==to_user.id).first()
    if privacy_check:
        if privacy_check.privacy_option == "Everyone":
            image = get_user_main_image(to_user)
        elif privacy_check.privacy_option == "Verified account only":
            if from_user.is_number_verified and from_user.is_photos_verified and from_user.is_documents_verified:
                image = get_user_main_image(to_user)
            else:
                image = get_user_main_image_blured(to_user)
        else:
            photo_request = db.query(PhotoRequests).filter(PhotoRequests.from_user_id==from_user.id, PhotoRequests.to_user_id==to_user.id,PhotoRequests.is_deleted==False,PhotoRequests.status=="accepted").first()
            if photo_request:
                image = get_user_main_image(to_user)
            else:
                image = get_user_main_image_blured(to_user)
    else:
        image = get_user_main_image(to_user)
    return image

def get_partner_all_images(db: Session, from_user: User, to_user: User):
    image = None
    privacy_check = db.query(UserPrivacyOption).filter(UserPrivacyOption.type=="photo", UserPrivacyOption.user_id==to_user.id).first()
    if privacy_check:
        if privacy_check.privacy_option == "Everyone":
            image = get_user_all_images(to_user)
        elif privacy_check.privacy_option == "Verified account only":
            if from_user.is_number_verified and from_user.is_photos_verified and from_user.is_documents_verified:
                image = get_user_all_images(to_user)
            else:
                image = get_user_all_image_blured(to_user)
        else:
            photo_request = db.query(PhotoRequests).filter(PhotoRequests.from_user_id==from_user.id, PhotoRequests.to_user_id==to_user.id,PhotoRequests.is_deleted==False,PhotoRequests.status=="accepted").first()
            if photo_request:
                image = get_user_all_images(to_user)
            else:
                image = get_user_all_image_blured(to_user)
    else:
        image = get_user_all_images(to_user)
    
    return [i for i in image if i != ""]

def get_user_main_image_blured(user: User):
    image = None
    image_set = user.user_table
    if len(image_set) == 0:
        image = get_user_avatar(user)
    else:
        for i in image_set:
            main = i.is_main
            verified = i.is_verified
            is_deleted = i.is_deleted
            if main and verified and not is_deleted:
                image = i.blurred_image
        if (image is None) or (image == ""):
            image = get_user_avatar(user)
    return image

def get_user_all_image_blured(user: User):
    image = []
    image_set = user.user_table
    additional_image_set = user.additional_image_table
    if len(image_set) == 0:
        image.append(get_user_avatar(user))
    else:
        for i in image_set:
            # main = i.is_main
            verified = i.is_verified
            is_deleted = i.is_deleted
            if verified and not is_deleted:
                image.append(i.blurred_image)
        if image is None:
            image = get_user_avatar(user)
    if len(additional_image_set) != 0:
        for j in additional_image_set:
                # main = i.is_main
                verified = j.is_verified
                is_deleted = j.is_deleted
                img = j.image
                if verified and (not is_deleted) and (img !=""):
                    image.append(j.image)
    return [i for i in image if i != ""]


def get_user_main_image(user: User):
    image = None
    image_set = user.user_table
    if len(image_set) == 0:
        image = get_user_avatar(user)
    else:
        for i in image_set:
            main = i.is_main
            verified = i.is_verified
            is_deleted = i.is_deleted
            img = i.image
            if main and verified and (not is_deleted) and (img !=""):
                image = i.image
        if (image is None) or (image == ""):
            image = get_user_avatar(user)
    return image

def get_user_all_images(user: User):
    image = []
    image_set = user.user_table
    additional_image_set = user.additional_image_table
    if len(image_set) == 0:
        image.append(get_user_avatar(user))
    else:
        for i in image_set:
            # main = i.is_main
            verified = i.is_verified
            is_deleted = i.is_deleted
            img = i.image
            is_main = i.is_main
            if verified and (not is_deleted) and (img != ""):
                if is_main:
                    image.insert(0,i.image)
                else:
                    image.append(i.image)        
        
        if len(image)==0:
            image.append(get_user_avatar(user))
    if len(additional_image_set) != 0:
        for j in additional_image_set:
                # main = i.is_main
                verified = j.is_verified
                is_deleted = j.is_deleted
                img = j.image
                if verified and (not is_deleted) and (img != ""):
                    image.append(j.image)
    
    return [i for i in image if i != ""]


def get_current_user_all_images(user: User):
    image = []
    image_set = user.user_table
    additional_image_set = user.additional_image_table
    if len(image_set) > 0:
        for i in image_set:
            # main = i.is_main
            verified = i.is_verified
            is_deleted = i.is_deleted
            img = i.image
            is_main = i.is_main
            if verified and (not is_deleted)  and (img != ""):
                if is_main:
                    image.insert(0,i.image)
                else:
                    image.append(i.image)
    if len(additional_image_set) != 0:
        for j in additional_image_set:
                # main = i.is_main
                verified = j.is_verified
                is_deleted = j.is_deleted
                img = j.image
                if verified and (not is_deleted)  and (img != ""):
                    image.append(j.image)
    if len(image)==0:
        image.append(get_user_avatar(user))
    
    return [i for i in image if i != ""]


def get_user_avatar(user: User):
    if user.gender == "Female":
        image = config.default_avatar_female
    elif user.gender == "Male":
        image = config.default_avatar_male
    else:
        image = config.default_avatar_others
    return image