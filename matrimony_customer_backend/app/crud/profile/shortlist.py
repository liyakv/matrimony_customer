from sqlalchemy.orm import Session
from app.models.model import *
from app.schemas.profile.profileShortlist import *


def check_already_shortlisted(db: Session, to_user_id: int, from_user_id: int):
    return db.query(ProfileShortlist).filter(ProfileShortlist.is_deleted==False, ProfileShortlist.from_user_id==from_user_id, ProfileShortlist.to_user_id==to_user_id).all()

def check_recently_shortlisted(db: Session, to_user_id: int, from_user_id: int):
    return db.query(ProfileShortlist).filter(ProfileShortlist.is_deleted==False, ProfileShortlist.from_user_id==from_user_id, ProfileShortlist.to_user_id==to_user_id).first()


def delete_shortlist(db: Session, cur_shortlist: ProfileShortlist):
    cur_shortlist.is_deleted = True
    cur_shortlist.deleted_on = datetime.now()
    db.commit()
    return cur_shortlist


def profile_short_list(db: Session, short_list: ShortListCreate, from_user_id: int):
    db_short_list = ProfileShortlist(from_user_id=from_user_id, to_user_id=short_list.user_id)
    db.add(db_short_list)
    db.commit()
    db.refresh(db_short_list)
    return db_short_list


def get_short_listed_by_partners(db: Session, user_id: int):
    query = f"""
    select matrimony_user.name, matrimony_user.gender, matrimony_user.dob, date_part('year',age(now(), matrimony_user.dob)) as age_year,
    date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
    matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
    matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_star.star_name,
    matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
    matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,matrimony_profile_shortlist.is_viewed,matrimony_profile_shortlist.id as shortlist_id,matrimony_user.id as user_id,matrimony_profile_shortlist.created_on, matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified,matrimony_profile_shortlist.updated_on
    from matrimony_profile_shortlist inner join matrimony_user on matrimony_profile_shortlist.from_user_id=matrimony_user.id
    and matrimony_user.is_active='1' left outer join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
    left outer join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
    left outer join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
    left outer join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
    left outer join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
    left outer join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
    left outer join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
    left outer join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
    where matrimony_profile_shortlist.to_user_id='{user_id}' and matrimony_profile_shortlist.is_deleted='0' order by matrimony_profile_shortlist.id desc
    """
    return db.execute(query).all()


def get_short_listed_by_user(db: Session, user_id: int):
    query = f"""
    select matrimony_user.name,matrimony_user.gender,matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
    date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
    matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
    matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_star.star_name,
    matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
    matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,matrimony_profile_shortlist.is_viewed,matrimony_profile_shortlist.id as shortlist_id,matrimony_user.id as user_id,matrimony_profile_shortlist.created_on, matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified,matrimony_profile_shortlist.updated_on
    from matrimony_profile_shortlist inner join matrimony_user on matrimony_profile_shortlist.to_user_id=matrimony_user.id
    and matrimony_user.is_active='1' left outer join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
    left outer join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
    left outer join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
    left outer join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
    left outer join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
    left outer join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
    left outer join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
    left outer join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
    where matrimony_profile_shortlist.from_user_id='{user_id}' and matrimony_profile_shortlist.is_deleted='0' order by matrimony_profile_shortlist.id desc
    """
    return db.execute(query).all()


def get_shortlist_by_id(db: Session, id: int):
    return db.query(ProfileShortlist).get(id)


def update_shortlist_views_status(db: Session, cur_shortlist: ProfileShortlist):
    cur_shortlist.is_viewed = True
    db.commit()
    return cur_shortlist