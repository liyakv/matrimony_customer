from sqlalchemy.orm import Session
from app.models.model import *
from app.schemas.profile.requests import *


def check_photo_request_already_send(db: Session, photo_request: PhotoRequestsCreate, from_user_id: int):
    return db.query(PhotoRequests).filter(PhotoRequests.is_deleted==False, PhotoRequests.from_user_id==from_user_id, PhotoRequests.to_user_id==photo_request.user_id).all()

def send_photo_requests(db: Session, photo_request: PhotoRequestsCreate, from_user_id: int):
    db_photo_request = PhotoRequests(from_user_id=from_user_id, to_user_id=photo_request.user_id)
    db.add(db_photo_request)
    db.commit()
    db.refresh(db_photo_request)
    return db_photo_request


def get_photo_request_by_id(db: Session, id: int):
    return db.query(PhotoRequests).get(id)


def update_photo_requests(db: Session, photo_request: PhotoRequestsUpdate):
    cur_request = get_photo_request_by_id(db, id=photo_request.request_id)
    cur_request.status = photo_request.status
    db.commit()
    return cur_request


def get_photo_requests(db: Session, user_id: int):
    query = f"""
    select matrimony_user.name,matrimony_user.gender,matrimony_user.dob,date_part('year',age(now(), matrimony_user.dob)) as age_year,
    date_part('month',age(now(), matrimony_user.dob)) as age_month, matrimony_user_basic_details.height_in_ft,
    matrimony_user_basic_details.mother_tongue,matrimony_user_location_details.city,matrimony_user_location_details.district,
    matrimony_user_location_details.state,matrimony_religion.level_one,matrimony_religion.level_two,matrimony_star.star_name,
    matrimony_user_religion_info.raasi,matrimony_education.qualification,matrimony_user_profession_details.employed_in,
    matrimony_occupation.occupation_name,matrimony_user_profession_details.annual_income,matrimony_user_profession_details.currency_type,
	matrimony_photo_requests.status,matrimony_photo_requests.id as request_id,matrimony_user.id as user_id,matrimony_photo_requests.created_on,
	matrimony_user_basic_details.profile_created_for, matrimony_user.is_number_verified, matrimony_user.is_photos_verified, matrimony_user.is_documents_verified,matrimony_photo_requests.updated_on
    from matrimony_photo_requests inner join matrimony_user on matrimony_photo_requests.from_user_id=matrimony_user.id
    and matrimony_user.is_active='1' left outer join matrimony_user_basic_details on matrimony_user.id=matrimony_user_basic_details.user_id
    left outer join matrimony_user_location_details on matrimony_user.id=matrimony_user_location_details.user_id
    left outer join matrimony_user_religion_info on matrimony_user.id=matrimony_user_religion_info.user_id
    left outer join matrimony_religion on matrimony_user_religion_info.religion_id=matrimony_religion.id
    left outer join matrimony_star on matrimony_user_religion_info.star_id=matrimony_star.id
    left outer join matrimony_user_profession_details on matrimony_user.id=matrimony_user_profession_details.user_id
    left outer join matrimony_education on matrimony_user_profession_details.highest_education_id=matrimony_education.id
    left outer join matrimony_occupation on matrimony_user_profession_details.occupation_id=matrimony_occupation.id
    where matrimony_photo_requests.to_user_id='{user_id}' and matrimony_photo_requests.is_deleted='0' order by matrimony_photo_requests.id desc
    """
    return db.execute(query).all()