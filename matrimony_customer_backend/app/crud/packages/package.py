from gc import freeze
from sqlalchemy.orm import Session
from app.models.model import *
from sqlalchemy import desc
from app.schemas.package.package import PaymentCreate


def get_all_packages_user_crud(db: Session, user_id: int):
    packages =db.execute("""
    select matrimony_packages.*,matrimony_subscribe_package.expiry_date from matrimony_packages inner join matrimony_package_log on matrimony_packages.id=matrimony_package_log.package_id left join matrimony_subscribe_package on matrimony_package_log.id=matrimony_subscribe_package.package_log_id and matrimony_subscribe_package.user_id='"""+user_id+"""' and expiry_date>=now() where matrimony_packages.is_deleted='0' order by matrimony_packages.id desc
    """).fetchall()
    return packages


def get_all_packages_crud(db: Session):
    packages =db.execute("""
    select matrimony_packages.*,null as expiry_date from matrimony_packages where matrimony_packages.is_deleted='0' order by matrimony_packages.id desc
    """).fetchall()
    return packages


def get_subscribed_packages(db: Session, user_id: int):
    packages =db.execute("""
    select matrimony_package_log.*,matrimony_subscribe_package.expiry_date from matrimony_package_log inner join matrimony_subscribe_package on matrimony_package_log.id=matrimony_subscribe_package.package_log_id where matrimony_subscribe_package.subscription_status='completed' and matrimony_subscribe_package.user_id='"""+str(user_id)+"""' and matrimony_subscribe_package.expiry_date>=now()
    """).fetchall()
    return packages


def subscribe_free_package_crud(db: Session, user_id: int):


    free_package = None
    free_package  = db.query(PackageLog).filter(PackageLog.is_deleted==False,  PackageLog.amount==0)
    free_package = free_package[-1]
    free_package = free_package.id

    db_package = SubscribePackage(package_log_id=free_package, user_id = user_id,
                                subscription_status = "completed")
    db.add(db_package)
    db.commit()
    db.refresh(db_package)

    return db_package

def get_package_details(db: Session, package_id: int):
    package =db.query(PackageLog).filter(PackageLog.package_id==package_id).order_by(PackageLog.id.desc()).first()
    return package

def package_subscribe_crud(db: Session, user_id: int, package_id: int, payment: str):
    db_package = SubscribePackage(subscription_status = "new", package_log_id = package_id, user_id = user_id,payment_id = payment)
    db.add(db_package)
    db.commit()
    db.refresh(db_package)
    return db_package


def get_package_by_razorpayID(db: Session, razorpayID: str):
    package =db.query(SubscribePackage).filter(SubscribePackage.payment_id==razorpayID).first()
    return package


def add_payment_details(db: Session, payment: PaymentCreate):
    db_payment = Payment(user_id=payment.paid_user_id, payment_id=payment.payment_id, payment_method=payment.payment_method, amount_paid=payment.amount_paid, subscribed_package_id=payment.order_id, status=payment.status)
    db.add(db_payment)
    db.commit()
    db.refresh(db_payment)
    return db_payment