import profile
from sqlalchemy.orm import Session
from app.crud.preferences.matches import get_match_score
from app.models.model import *
from app.schemas.profile.profileShortlist import *
from app.schemas.preferences.preference import *
from sqlalchemy import text
from app.crud.home.home import gender_pref_list
from app.crud.home.home import active_users,priority_users,gender_pref_list, new_calculation_days
import config
import datetime as dt
from pytz import timezone

# //
import datetime
from pytz import timezone
from sqlalchemy import or_
from sqlalchemy.sql.expression import func
from config import *
import collections
import itertools
from datetime import datetime, timedelta
from app.crud.profile.profile_manage import *
import operator
from sqlalchemy import desc
from app.crud.profile.shortlist import *
from app.crud.profile.partnerInterests import *



def get_total_matches_crud(db: Session, user_id: int, user: User):
    gender = db.query(User).filter(User.gender == user.basic_preferences_table.gender, User.id!=user_id).all()
    return gender


# //***\\\
def get_matches_test(db: Session, user_id: int):
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))

    user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
    user_basic_preff=db.query(BasicPartnerPreferences).filter(BasicPartnerPreferences.is_deleted==False,BasicPartnerPreferences.user_id==user_id).first()
    user_location_preff=db.query(LocationPartnerPreferences).filter(LocationPartnerPreferences.is_deleted==False,LocationPartnerPreferences.user_id==user_id).first()

    main_list=[]

    # proffesional_preferences
    if (user_profession_preff == None):
        print("profession preference NOT provided........")
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
    else:
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)

        user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
        education_strict = user_profession_preff.education_strict
        education = user_profession_preff.education
        employed_in_strict= user_profession_preff.employed_in_strict
        employed_in=user_profession_preff.employed_in
        occupation_strict = user_profession_preff.occupation_strict
        occupation = user_profession_preff.occupation

        annual_income=user_profession_preff.annual_income
        annual_income_lower_limit_inr = user_profession_preff.annual_income_lower_limit_inr
        annual_income_upper_limit_inr = user_profession_preff.annual_income_upper_limit_inr

        if education_strict==True and education != None:
            Edupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.highest_education_id.in_(education['education'])).all()
            edu_list=[i.id for i in Edupref]
            filtered_users=list(set(active_genderpreff_users) & set(edu_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=active_genderpreff_users
            main_list.extend(filtered_users)

        if employed_in_strict==True and employed_in != None:
            Emppref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.employed_in.in_(employed_in['employed_in'])).all()
            empIn_list=[i.id for i in Emppref]
            filtered_users=list(set(filtered_users) & set(empIn_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)

        if occupation_strict==True and occupation != None:
            Occupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.occupation_id.in_(occupation['occupation'])).all()
            occu_list=[i.id for i in Occupref]
            filtered_users=list(set(filtered_users) & set(occu_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)

        if annual_income !="Doesn't matter":
            Incmpref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.annual_income_lower_limit_inr>=annual_income_lower_limit_inr,UserProfessionalDetails.annual_income_upper_limit_inr<=annual_income_upper_limit_inr).all()
            Incm_list=[i.id for i in Incmpref]
            filtered_users=list(set(filtered_users) & set(Incm_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)
          
          
          
    # basic_preferences
    if (user_basic_preff == None):
        print("basic preference NOT provided........")
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
    else:
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)

        user_basic_preff=db.query(BasicPartnerPreferences).filter(BasicPartnerPreferences.is_deleted==False,BasicPartnerPreferences.user_id==user_id).first()

        physical_status_strict = user_basic_preff.physical_status_strict
        physical_status = user_basic_preff.physical_status

        if physical_status_strict==True and physical_status != None:
            PhysPref = db.query(UserBasicDetails).filter(UserBasicDetails.physical_status.in_(physical_status['physical_status'])).all()
            phys_list=[i.id for i in PhysPref]
            filtered_users=list(set(filtered_users) & set(phys_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)


    # location_preferences
    if (user_location_preff == None):
        print("location preference NOT provided........")
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
    else:
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)

        user_location_preff=db.query(LocationPartnerPreferences).filter(LocationPartnerPreferences.is_deleted==False,LocationPartnerPreferences.user_id==user_id).first()


        country_strict = user_location_preff.country_strict
        country = user_location_preff.country
        
        print(country,user_id)

        if country_strict==True and country != None:
            countryPref = db.query(UserLocationDetails).filter(UserLocationDetails.country.in_(country['country'])).all()
            print(countryPref,"111111111111111----------------------======")
            cntry_list=[i.id for i in countryPref]
            print(cntry_list,"============")
            filtered_users=list(set(filtered_users) & set(cntry_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)
            
            
            
    frequency = collections.Counter(main_list)
    sorted_d = list(dict( sorted(dict(frequency).items(), key=operator.itemgetter(1),reverse=True)).keys())
    final_list=list(set(filtered_users) & set(sorted_d))
    if user_id in final_list: final_list.remove(user_id)
    final_users=[db.query(User).filter(User.id==id).one() for id in final_list]
    return final_users
























    # user_profession_preff = db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
    # # user_profession_details= db.query(UserProfessionalDetails).filter(UserProfessionalDetails.is_deleted==False,UserProfessionalDetails.user_id==user_id).first()

    # prefence_details = {}
    # basic_pref_details = []
    
    # if user_profession_preff is not None:
    #     if user_profession_preff.employed_in is not None:
    #         employed_in = user_profession_preff.employed_in
    #         print(employed_in)
    #         if employed_in is not None:
    #             Emppref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.employed_in.in_(employed_in['employed_in'])).all()

    #             print(Emppref.employed_in,'-----')

    # return Emppref

    #         # if user_profession_details.employed_in in employed_in:
    #         #     basic_pref_details.append(prefence_details)   



        




    

















