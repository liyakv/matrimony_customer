import datetime as dt
from typing import List          #not typo
from pytz import timezone
from sqlalchemy import or_
from sqlalchemy.orm import Session
from app.models.model import Religion, User, UserFamilyDetails, UserLifeStyle
from app.crud.home.home import *



def within_a_day(db: Session):
    last24HourDateTime = dt.datetime.now(timezone('Asia/Kolkata')) - dt.timedelta(hours = 24)
    q = db.query(User).filter(User.date_joined > last24HourDateTime ).all()
    rlist = []
    for i in q:
        id_ = i.id
        rlist.append(id_)
    return rlist


def within_a_week(db: Session):
    last7DaysDateTime = dt.datetime.now(timezone('Asia/Kolkata')) - dt.timedelta(days = 7)
    q = db.query(User).filter(User.date_joined > last7DaysDateTime ).all()
    rlist = []
    for i in q:
        id_ = i.id
        rlist.append(id_)
    return rlist


def within_a_month(db: Session):
    last30DaysDateTime = dt.datetime.now(timezone('Asia/Kolkata')) - dt.timedelta(days = 30)
    q = db.query(User).filter(User.date_joined > last30DaysDateTime ).all()
    rlist = []
    for i in q:
        id_ = i.id
        rlist.append(id_)
    return rlist


def with_photo(db: Session):
    rlist = has_image(db)
    return rlist


def profile_created_by(db: Session, key: List):
    return [r.user_id for r in db.query(UserBasicDetails.user_id).filter(UserBasicDetails.profile_created_for.in_(key)).all()]


def profile_by_parent(db: Session):
    return [r.user_id for r in db.query(UserBasicDetails).filter(or_(UserBasicDetails.profile_created_for == 'Son',UserBasicDetails.profile_created_for == "Daughter")).all()]


def profile_by_age_limit(db: Session, lower_limit: int, upper_limit: int):
    query = f"""
    select matrimony_user.id from matrimony_user where date_part('year',age(now(), matrimony_user.dob)) between {lower_limit} and {upper_limit}
    """
    return [id[0] for id in db.execute(query).all()]


def profile_by_height_limit(db: Session, lower_limit: int, upper_limit: int):
    query = f"""
    select matrimony_user_basic_details.user_id from matrimony_user_basic_details where matrimony_user_basic_details.height_in_cm between {lower_limit} and {upper_limit}
    """
    return [id[0] for id in db.execute(query).all()]


def profile_by_mother_tongue(db: Session, mother_tongue: List):
    result = db.query(UserBasicDetails.user_id).filter(UserBasicDetails.mother_tongue.in_(mother_tongue)).all()
    return [id[0] for id in result]


def profile_by_marital_status(db: Session, marital_status: List):
    result = db.query(UserBasicDetails.user_id).filter(UserBasicDetails.marital_status.in_(marital_status)).all()
    return [id[0] for id in result]


def profile_by_religion_level_one(db: Session, level_one: str):
    result = db.query(UserReligionInfo.user_id).join(Religion).filter(Religion.level_one==level_one).all()
    return [id[0] for id in result]


def profile_by_religion_level_two(db: Session, level_two: List):
    result = db.query(UserReligionInfo.user_id).join(Religion).filter(Religion.level_two.in_(level_two)).all()
    return [id[0] for id in result]


def profile_by_religion_level_three(db: Session, level_three: List):
    result = db.query(UserReligionInfo.user_id).join(Religion).filter(Religion.level_three.in_(level_three)).all()
    return [id[0] for id in result]


def profile_by_star(db: Session, star: List):
    star_ids = db.query(Star).filter(Star.star_name.in_(star), Star.is_deleted==False).with_entities(Star.id).all()
    retVal = [row[0] for row in star_ids]
    result = db.query(UserReligionInfo.user_id).filter(UserReligionInfo.star_id.in_(retVal)).all()
    return [id[0] for id in result]


def by_education(db: Session, key: List):
    education_ids = db.query(Education).filter(Education.qualification.in_(key), Education.is_deleted==False).with_entities(Education.id).all()
    retVal = [row[0] for row in education_ids]
    return [r.user_id for r in db.query(UserProfessionalDetails).filter(UserProfessionalDetails.highest_education_id.in_(retVal)).all()]


def by_employed(db: Session, key: List):
    return [r.user_id for r in db.query(UserProfessionalDetails).filter(UserProfessionalDetails.employed_in.in_(key)).all()]


def by_occupation(db: Session, key: List):
    occupation_ids = db.query(Occupation).filter(Occupation.occupation_name.in_(key), Occupation.is_deleted==False).with_entities(Occupation.id).all()
    retVal = [row[0] for row in occupation_ids]
    return [r.user_id for r in db.query(UserProfessionalDetails).filter(UserProfessionalDetails.occupation_id.in_(retVal)).all()]


def by_annual_income(db: Session, key: str):
    return [r.user_id for r in db.query(UserProfessionalDetails).filter(UserProfessionalDetails.annual_income == key).all()]


def by_country(db: Session, key: List):
    return [r.user_id for r in db.query(UserLocationDetails).filter(UserLocationDetails.country.in_(key)).all()]


def by_citizenship(db: Session, key: List):
    return [r.user_id for r in db.query(UserLocationDetails).filter(UserLocationDetails.citizenship.in_(key)).all()]


def by_resident_status(db: Session, key: List):
    return [r.user_id for r in db.query(UserProfessionalDetails).filter(UserProfessionalDetails.residential_status.in_(key)).all()]


def by_family_status(db: Session, status: List):
    return [r.user_id for r in db.query(UserFamilyDetails).filter(UserFamilyDetails.family_status.in_(status)).all()]


def by_family_values(db: Session, values: List):
    return [r.user_id for r in db.query(UserFamilyDetails).filter(UserFamilyDetails.family_values.in_(values)).all()]


def by_family_type(db: Session, types: List):
    return [r.user_id for r in db.query(UserFamilyDetails).filter(UserFamilyDetails.family_type.in_(types)).all()]

def get_profile_from_id(db: Session, id: int):
    return db.query(User).filter(User.id == id).first()


def get_profile_by_eating_habits(db: Session, eating_habits: List):
    result = db.query(UserLifeStyle.user_id).filter(UserLifeStyle.food_habits.in_(eating_habits)).all()
    return [id[0] for id in result]


def get_profile_by_smoking_habits(db: Session, smoking_habits: List):
    result = db.query(UserLifeStyle.user_id).filter(UserLifeStyle.smoking_habits.in_(smoking_habits)).all()
    return [id[0] for id in result]


def get_profile_by_drinking_habits(db: Session, drinking_habits: List):
    result = db.query(UserLifeStyle.user_id).filter(UserLifeStyle.drinking_habits.in_(drinking_habits)).all()
    return [id[0] for id in result]


def get_profile_by_physical_status(db: Session, physical_status: List):
    result = db.query(UserBasicDetails.user_id).filter(UserBasicDetails.physical_status.in_(physical_status)).all()
    return [id[0] for id in result]


def get_profile_by_body_type(db: Session, body_type: List):
    result = db.query(UserBasicDetails.user_id).filter(UserBasicDetails.body_type.in_(body_type)).all()
    return [id[0] for id in result]
