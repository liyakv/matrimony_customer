from sqlalchemy.orm import Session
from app.models.model import *
from sqlalchemy import desc


def get_horizontal_advertisement(db: Session, position: str):
    return db.query(Advertisement).filter(Advertisement.is_deleted==False, Advertisement.type=='Horizontal', Advertisement.position==position).order_by(desc(Advertisement.id)).all()[:5]


def get_vertical_advertisement(db: Session):
    return db.query(Advertisement).filter(Advertisement.is_deleted==False, Advertisement.type=='Vertical').order_by(desc(Advertisement.id)).first()
