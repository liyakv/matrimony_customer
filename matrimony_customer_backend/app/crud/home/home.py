import datetime
from operator import is_           #not type
import random
from pytz import timezone
from sqlalchemy import or_
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import func
from app.crud.preferences.matches import get_match_score
# from app.crud.profile.profile import get_user_by_id
from config import *
from app.models.model import *
from app.schemas.preferences.preference import *
import datetime as dt
import config
import collections
import itertools
from datetime import datetime, timedelta
from app.crud.profile.profile_manage import *
import operator
from sqlalchemy import desc
from app.crud.profile.shortlist import *
from app.crud.profile.partnerInterests import *


def get_user_by_id(db: Session, id: int):
    return db.query(User).get(id)


# get priority users list
def priority_users(db: Session):
    priority_package_logs = db.query(PackageLog).filter(PackageLog.prty_in_srch_rslt == True).all()
    priority_package_id = []
    for i in range(0,len(priority_package_logs)):
        j = priority_package_logs[i].id
        priority_package_id.append(j)
    users = db.query(SubscribePackage).filter(SubscribePackage.subscription_status == "completed", SubscribePackage.expiry_date > dt.date.today(), SubscribePackage.package_log_id.in_(priority_package_id)).all()
    ids=[]
    for i in range(0,len(users)):
        j = users[i].id
        ids.append(j)
    return ids


# get active users_id as list without current user's id
def active_users(db: Session, user_id: int):
    active = db.query(User).filter(User.is_active == True).all()
    admin_ids = [r.id for r in db.query(User).filter(or_(User.is_admin == True, User.is_staff == True, User.is_superuser == True)).all()]
    ids = []
    count = len(active)
    for i in range(0,count):
        j = active[i].id
        ids.append(j)
    if user_id in ids:
        ids.remove(user_id)
    for i in admin_ids:
        if i in ids:
            ids.remove(i)
    return ids


# get current users_id with profile picture
def has_image(db:Session):
    users = db.query(UserImage).filter(UserImage.is_main == True , UserImage.is_verified == True).all()
    ids = []
    for i in range(0,len(users)):
        j = users[i].user_id
        ids.append(j)
    return ids


# get current user's gender preference as String
def get_gender_preference(db: Session, user_id: int):
    user = db.query(User).get(user_id)
    user_gender = user.gender
    gender_pref = ''
    if (user_gender == 'Male' or user_gender == 'male'):
        gender_pref = "Female"
    elif (user_gender =='Female' or user_gender == 'female'):
        gender_pref = "Male"
    else:
        pass
    try:
        pref = db.query(BasicPartnerPreferences).filter(BasicPartnerPreferences.user_id == user_id).first()
        if (pref != None):
            if (pref.gender == None):
                pass
            else:
                gender_pref = pref.gender
        else:
            pass
    except:
        pass
    finally:
        if (gender_pref == ''):
            gender_pref = "No enough data to get gender preference"
    return gender_pref


def gender_pref_list2(db: Session, user_id: int, search_key: str):
    gender_pref = get_gender_preference(db, user_id)
    users = db.query(User).filter(User.gender == gender_pref)
    if search_key:
     users = users.filter(User.name.ilike("%"+search_key+"%"))
    users = users.all()
    ids = []
    count = len(users)
    for i in range(0,count):
        j = users[i].id
        ids.append(j)
    return ids


def gender_pref_list(db: Session, user_id: int):
    gender_pref = get_gender_preference(db, user_id)
    users = db.query(User).filter(User.gender == gender_pref).all()
    ids = []
    count = len(users)
    for i in range(0,count):
        j = users[i].id
        ids.append(j)
    return ids



def city_district_state_selection(db: Session, user_id: int,city_name:String):
    user = get_user_by_id(db, user_id)
    match_count=0
    new_count=0
    final_data=[]
    img_list = []
    if city_name:
        city_match = db.query(UserLocationDetails).filter(UserLocationDetails.city == city_name,UserLocationDetails.is_deleted == False).all()
        match_user_ids = []
        count = len(city_match)
        for i in range(0,count):
            j = city_match[i].user_id
            match_user_ids.append(j)
        final_intersection_list = list(set(intersect_list_users) & set(match_user_ids))
        final_intersection_list_2 = []
        final_intersection_list_2.extend(final_intersection_list)
        match_count = len(final_intersection_list)

        # 5 images as list
        if (match_count>5):
            image_count = 5
            const_image_count = 5
        else:
            image_count = match_count
            const_image_count = match_count

        if (image_count>0):
            for i in range(0,(len(final_intersection_list))):
                if(len(img_list)==const_image_count):
                    break
                partner = get_user_by_id(db, final_intersection_list[i])
                img_list.append(get_partner_image(db,user, partner))

        # new users count
        users_joined_date_list = []
        for i in range(0,match_count):
            new_users = db.query(User).filter(User.id == final_intersection_list_2[i]).first()
            users_joined_date_list.append(new_users.date_joined)
        calcultion_date = dt.datetime.now(timezone('Asia/Kolkata')) - dt.timedelta(days=new_calculation_days)
        final_new_list = []
        for i in range(0, len(users_joined_date_list)):
            if users_joined_date_list[i] > calcultion_date:
                final_new_list.append(i)
        new_count = len(final_new_list)
    final_data=[img_list,match_count,new_count]

    return final_data

def get_city_match(db: Session, user_id: int):
    global gender_pref
    global intersect_list_users
    global priority_list
    gender_pref = get_gender_preference(db, user_id)
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    priority_list = priority_users(db)
    intersect_list_users = list(set(active_user_list) & set(gender_pref_users_list))

    location = db.query(UserLocationDetails).filter(UserLocationDetails.user_id == user_id, UserLocationDetails.is_deleted == False).first()
    if location:
        city_name = location.city
        data=city_district_state_selection(db, user_id,city_name)
        city_dict = {
                "name" : "In Your City",
                "display_images" : data[0],
                "match_count" : data[1],
                "new_match_count" : data[2]
            }

        if len(data[0])==0:
            city_name = location.district
            data=city_district_state_selection(db, user_id,city_name)
            city_dict = {
                "name" : "In Your City",
                "match_count" : data[1],
                "display_images" : data[0],
                "new_match_count" : data[2]
            }
            if len(data[0])==0:
                city_name = location.state
                data=city_district_state_selection(db, user_id,city_name)
                city_dict = {
                    "name" : "In Your City",
                    "match_count" : data[1],
                    "display_images" : data[0],
                    "new_match_count" : data[2]
                }
    else:
        city_dict = {
        "name" : "In Your City",
        "exception" : "No location details for this User"
    }
    return city_dict




def get_city_data(db: Session,city_name:String,intersect_list_users:List):
    city_match = db.query(UserLocationDetails).filter(UserLocationDetails.city == city_name, UserLocationDetails.is_deleted == False).all()
    match_user_ids = []
    count = len(city_match)
    for i in range(0,count):
        j = city_match[i].user_id
        match_user_ids.append(j)
    final_intersection_list = list(set(intersect_list_users) & set(match_user_ids))
    priority_list=priority_users(db)
    priority_intersect_final = list(set(final_intersection_list) & set(priority_list))
    if priority_intersect_final:
        for i in priority_intersect_final:
            final_intersection_list.remove(i)
        priority_intersect_final.extend(final_intersection_list)
    else:
        priority_intersect_final = final_intersection_list
    match_count = len(priority_intersect_final)
    return [match_count,priority_intersect_final]

def get_city_match_detailed(db: Session, logined_user: User):
    user_id = logined_user.id
    gender_pref = get_gender_preference(db, user_id)
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    intersect_list_users = list(set(active_user_list) & set(gender_pref_users_list))
    resp = []
    location = db.query(UserLocationDetails).filter(UserLocationDetails.user_id == user_id, UserLocationDetails.is_deleted == False).first()
    if location:
        city_name = location.city
        final_lst = []
        if city_name:
            data=get_city_data(db,city_name,intersect_list_users)
            final_lst=data[1] if len(data) != 0 else []
        if len(final_lst)==0 or city_name==None:
            city_name = location.district
            data=get_city_data(db,city_name,intersect_list_users)
            final_lst=data[1] if len(data) != 0 else []
            if len(final_lst)==0 or location.district==None:
                city_name = location.state
                data=get_city_data(db,city_name,intersect_list_users)
                final_lst=data[1] if len(data) != 0 else []
        match_count=data[0]
        for i in range(0,match_count):
            try:
                users = db.query(User).filter(User.id == final_lst[i]).all()
            except Exception as e:
                users=None

            if users!=None:
                for user in users:
                    year,month=calculate_age_month(user.dob)
                    if user.user_basic_details == None:
                        profile_created_for=None
                        height_in_ft=None
                        mother_tongue=None
                    else:
                        profile_created_for=user.user_basic_details.profile_created_for.value if user.user_basic_details.profile_created_for else None
                        height_in_ft=user.user_basic_details.height_in_ft
                        mother_tongue= user.user_basic_details.mother_tongue.value if user.user_basic_details.mother_tongue else None
                    if user.user_location_details == None:
                        city=None
                        district=None
                        state=None
                    else:
                        city=user.user_location_details.city
                        district=user.user_location_details.district
                        state=user.user_location_details.state

                    if user.user_religion_details==None:
                        level_one= None
                        level_two = None
                        level_three = None
                        star_name= None
                        raasi=None
                    else:
                        level_one= user.user_religion_details.religion.level_one if user.user_religion_details.religion.level_one else None
                        level_two = user.user_religion_details.religion.level_two if user.user_religion_details.religion.level_two else None
                        level_three=user.user_religion_details.religion.level_three if user.user_religion_details.religion.level_three else None
                        star_name= user.user_religion_details.star.star_name if user.user_religion_details.star else None
                        raasi=user.user_religion_details.raasi
                    if user.user_professional_details==None:
                        qualification=None
                        employed_in=None
                        occupation_name=None
                        annual_income=None
                        currency_type=None
                    else:
                        qualification= user.user_professional_details.highest_education.qualification
                        employed_in=user.user_professional_details.employed_in.value
                        occupation_name= user.user_professional_details.occupation.occupation_name
                        annual_income= user.user_professional_details.annual_income.value
                        currency_type=user.user_professional_details.currency_type.value

                    image = get_partner_image(db,logined_user, user)
                    is_shortlisted = False
                    if len(check_already_shortlisted(db,user.id, logined_user.id))>0:
                        is_shortlisted = True
                    is_interest_send = False
                    if len(check_partner_interest_already_send(db,user.id,logined_user.id))>0:
                        is_interest_send = True
                    # me_user = get_user_by_id(db, user_id)
                    match_dict, total,score = get_match_score(db,user, logined_user)
                    try:
                        percent = round((score*100)/total)
                    except ZeroDivisionError:
                        percent = 0
                    pr ={
                        "id": user.id,
                        "name": user.name,
                        "profile_created_for":profile_created_for,
                        "is_user_verified": user.is_user_verified,
                        "image": image,
                        "details": {
                            "age_year": year,
                            "age_month": month,
                            "height_in_ft": height_in_ft,
                            "mother_tongue": mother_tongue,
                            "city": city,
                            "district":district ,
                            "state":state ,
                            "level_one": level_one,
                            "level_two": level_two,
                            "lefvel_three": level_three,
                            "star_name": star_name,
                            "raasi": raasi,
                            "qualification": qualification,
                            "employed_in": employed_in,
                            "occupation_name": occupation_name,
                            "annual_income":annual_income,
                            "currency_type": currency_type,
                        },
                            "created_on": user.date_joined,
                            "match_score": percent,
                            "match_score_total": total,
                            "match_score_matching_nos": score,
                            "is_shortlisted": is_shortlisted,
                            "is_interest_send": is_interest_send
                    }
                    resp.append(pr)
    return resp


# deffered
def get_star_match(db: Session, user_id: int):
    religion = db.query(UserReligionInfo).filter(UserReligionInfo.user_id == user_id).first()
    if religion:
        star = religion.star_id
        if star:
            star_match = db.query(UserReligionInfo).filter(UserReligionInfo.star_id == star).all()
            count = len(star_match)
            match_list_ids = []
            for i in range(0,count):
                j = star_match[i].user_id
                match_list_ids.append(j)
            final_intersection_list = list(set(intersect_list_users) & set(match_list_ids))
            final_intersection_list_2 = []
            final_intersection_list_2.extend(final_intersection_list)
            match_count = len(final_intersection_list)

            # 5 images as list
            image_count = 5
            img_list = []
            priority_intersect_final = list(set(final_intersection_list) & set(priority_list))
            if priority_intersect_final:
                image_count = image_count - len(priority_intersect_final)
                for i in final_intersection_list:
                    final_intersection_list.remove(i)
                for i in range(0,len(priority_intersect_final)):
                    try:
                        user = db.query(UserImage).filter(UserImage.user_id == priority_intersect_final[i], UserImage.is_main == True).first()
                    except:
                        user=None
                    if(user):
                        j = user.image
                        img_list.append(j)
            if (image_count>0):
                for i in range(0,(len(final_intersection_list))):
                    try:
                        if(i==len(final_intersection_list)):
                            break
                        user = db.query(UserImage).filter(UserImage.user_id == final_intersection_list[i], UserImage.is_main == True).first()
                    except:
                        user=None
                    if (user != None):
                        j = user.image
                        img_list.append(j)
            while (len(img_list)!=5):
                if (gender_pref == "Female"):
                    img_list.append(default_avatar_female)
                elif (gender_pref == "Male"):
                    img_list.append(default_avatar_male)
                else:
                    img_list.append(default_avatar_others)

            # new users count
            users_joined_date_list = []
            for i in range(0,match_count):
                new_users = db.query(User).filter(User.id == final_intersection_list_2[i]).first()
                users_joined_date_list.append(new_users.date_joined)

            calcultion_date = datetime.datetime.now(timezone('Asia/Kolkata')) - datetime.timedelta(days=new_calculation_days)
            final_new_list = []
            for i in range(0, len(users_joined_date_list)):
                if users_joined_date_list[i] > calcultion_date:
                    final_new_list.append(i)
            new_count = len(final_new_list)

            star_dict = {
                "name" : "star",
                "match_count" : match_count,
                "display_images" : img_list,
                "new_match_count" : new_count
            }
        else:
            star_dict = {
                "name" : "star",
                'exception' : "No star selected by the user"
            }
    else:
        star_dict = {
                "name" : "star",
                "exception" : "No Religion Details Available for this User"
            }
    return star_dict

def profession_pref_match(db: Session, user_id: int):
    new_count=0
    img_list=[]
    match_count=0
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))
    user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
    main_list=[]
    if (user_profession_preff == None):
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
    else:
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
        user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
        occupation_strict = user_profession_preff.occupation_strict
        occupation = user_profession_preff.occupation

        if occupation_strict==True and occupation != None:
            Occupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.occupation_id.in_(occupation['occupation'])).all()
            occu_list=[i.id for i in Occupref]
            filtered_users=list(set(filtered_users) & set(occu_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)
    frequency = collections.Counter(main_list)
    sorted_d = list(dict( sorted(dict(frequency).items(), key=operator.itemgetter(1),reverse=True)).keys())
    final_list=list(set(filtered_users) & set(sorted_d))
    match_count=len(final_list)
    userobj=db.query(User).filter(User.id==user_id).first()
    img_list = [get_partner_image(db, userobj, db.query(User).filter(User.id==i).first()) for i in final_list][:5]
    users_joined_date_list = []
    for i in range(0,match_count):
        new_users = db.query(User).filter(User.id == final_list[i]).first()
        users_joined_date_list.append(new_users.date_joined)
    calcultion_date = datetime.now(timezone('Asia/Kolkata')) - timedelta(days=new_calculation_days)
    final_new_list = []
    for i in range(0, len(users_joined_date_list)):
        if users_joined_date_list[i] > calcultion_date:
            final_new_list.append(i)
    new_count = len(final_new_list)
    return [img_list,match_count,new_count]

def get_profession_match(db: Session, user_id: int):
    user = get_user_by_id(db, user_id)
    profession_dict = {}
    profession = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.user_id == user_id).first()
    if profession:
        occupation = profession.occupation_id
        if occupation:
            occupation_match = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.occupation_id == occupation).all()
            count = len(occupation_match)
            match_list_ids = []
            for i in range(0,count):
                j = occupation_match[i].user_id
                match_list_ids.append(j)
            final_intersection_list = list(set(intersect_list_users) & set(match_list_ids))
            final_intersection_list_2 = []
            final_intersection_list_2.extend(final_intersection_list)
            match_count = len(final_intersection_list)

            # 5 images as list
            if (match_count>5):
                image_count = 5
                const_image_count = 5
            else:
                image_count = match_count
                const_image_count = match_count
            img_list = []

            if (image_count>0):
                for i in range(0,(len(final_intersection_list))):
                    if(len(img_list)==const_image_count):
                        break
                    partner = get_user_by_id(db, final_intersection_list[i])
                    img_list.append(get_partner_image(db, user, partner))

            # new users count
            users_joined_date_list = []
            for i in range(0,match_count):
                new_users = db.query(User).filter(User.id == final_intersection_list_2[i]).first()
                users_joined_date_list.append(new_users.date_joined)
            calcultion_date = datetime.now(timezone('Asia/Kolkata')) - timedelta(days=new_calculation_days)
            final_new_list = []
            for i in range(0, len(users_joined_date_list)):
                if users_joined_date_list[i] > calcultion_date:
                    final_new_list.append(i)
            new_count = len(final_new_list)
            profession_dict = {
                "name" : "Your Profession",
                "match_count" : match_count,
                "display_images" : img_list,
                "new_match_count" : new_count,
                "flag":"match"
            }
            if len(img_list)==0:
                profession_dict["flag"]="all"
                img_list=profession_pref_match(db,user_id)

                profession_dict = {
                "name" : "Your Profession",
                "match_count" : img_list[1],
                "display_images" : img_list[0],
                "new_match_count" : img_list[2],
                "flag":"preference"
            }

        else:

            img_list=profession_pref_match(db,user_id)
            profession_dict = {
                "name" : "Your Profession",
                "match_count" : img_list[1],
                "display_images" : img_list[0],
                "new_match_count" :img_list[2],
                "flag":"preference"
            }
    else:
        profession_dict = {
            "name" : "profession",
            "exception" : "No Professional Details Available for this User"
        }
    return profession_dict



def get_education_match(db: Session, user_id: int):
    user = get_user_by_id(db, user_id)
    profession = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.user_id == user_id).first()
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    # priority_list = priority_users(db)
    intersect_list_users = list(set(active_user_list) & set(gender_pref_users_list))
    education_dict={}
    if profession:
        education = profession.highest_education_id
        if education:
            education_match = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.highest_education_id == education).all()
            count = len(education_match)
            match_list_ids = []
            for i in range(0,count):
                j = education_match[i].user_id
                match_list_ids.append(j)
            final_intersection_list = list(set(intersect_list_users) & set(match_list_ids))
            final_intersection_list_2 = []
            final_intersection_list_2.extend(final_intersection_list)
            match_count = len(final_intersection_list)

            # 5 images as list
            if (match_count>5):
                image_count = 5
                const_image_count = 5
            else:
                image_count = match_count
                const_image_count = match_count
            img_list = []
            if (image_count>0):
                for i in range(0,(len(final_intersection_list))):

                    if(len(img_list)==const_image_count):
                        break
                    partner = get_user_by_id(db, final_intersection_list[i])
                    img_list.append(get_partner_image(db, user, partner))

            users_joined_date_list = []
            for i in range(0,match_count):
                new_users = db.query(User).filter(User.id == final_intersection_list_2[i]).first()
                users_joined_date_list.append(new_users.date_joined)

            calcultion_date = datetime.now(timezone('Asia/Kolkata')) - timedelta(days=new_calculation_days)
            final_new_list = []
            for i in range(0, len(users_joined_date_list)):
                if users_joined_date_list[i] > calcultion_date:
                    final_new_list.append(i)
            new_count = len(final_new_list)
            education_dict = {
                "name" : "Your Education",
                "match_count" : match_count,
                "display_images" : img_list,
                "new_match_count" : new_count,
                "flag":"all"
            }

            if len(img_list) == 0:
                active_user_list = active_users(db, user_id)
                gender_pref_users_list = gender_pref_list(db,user_id)
                active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))
                user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
                main_list=[]
                if (user_profession_preff == None):
                    filtered_users=active_genderpreff_users
                    main_list.extend(filtered_users)
                else:
                    filtered_users=active_genderpreff_users
                    main_list.extend(filtered_users)

                    user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
                    education_strict = user_profession_preff.education_strict
                    education = user_profession_preff.education

                    if education_strict==True and education != None:
                        Edupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.highest_education_id.in_(education['education'])).all()
                        edu_list=[i.id for i in Edupref]
                        filtered_users=list(set(active_genderpreff_users) & set(edu_list))
                        main_list.extend(filtered_users)
                    else:
                        filtered_users=active_genderpreff_users
                        main_list.extend(filtered_users)

                frequency = collections.Counter(main_list)
                sorted_d = list(dict( sorted(dict(frequency).items(), key=operator.itemgetter(1),reverse=True)).keys())
                final_list=list(set(filtered_users) & set(sorted_d))
                userobj=db.query(User).filter(User.id==user_id).first()
                # final_users=[db.query(User).filter(User.id==id).one() for id in final_list]
                img_list = [get_partner_image(db, userobj, db.query(User).filter(User.id==i).first()) for i in final_list][:5]
                users_joined_date_list = []
                for i in range(0,match_count):
                    new_users = db.query(User).filter(User.id == final_list[i]).first()
                    users_joined_date_list.append(new_users.date_joined)

                calcultion_date = datetime.now(timezone('Asia/Kolkata')) - timedelta(days=new_calculation_days)
                final_new_list = []
                for i in range(0, len(users_joined_date_list)):
                    if users_joined_date_list[i] > calcultion_date:
                        final_new_list.append(i)
                new_count = len(final_new_list)

                education_dict = {
                    "name" : "Your Education",
                    "match_count" : match_count,
                    "display_images" : img_list,
                    "new_match_count" : new_count,
                    "flag":"preference"
                }

    else:
        education_dict = {
            "name" : 'education',
            "exception" : "No Professional details available for this user"
        }
    return education_dict

def data_occupation(db: Session, user_id: int):
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))
    user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
    main_list=[]
    if (user_profession_preff == None):
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
    else:
        filtered_users=active_genderpreff_users
        main_list.extend(filtered_users)
        user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
        occupation_strict = user_profession_preff.occupation_strict
        occupation = user_profession_preff.occupation

        if occupation_strict==True and occupation != None:
            Occupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.occupation_id.in_(occupation['occupation'])).all()
            occu_list=[i.id for i in Occupref]
            filtered_users=list(set(filtered_users) & set(occu_list))
            main_list.extend(filtered_users)
        else:
            filtered_users=filtered_users
            main_list.extend(filtered_users)
    frequency = collections.Counter(main_list)
    sorted_d = list(dict( sorted(dict(frequency).items(), key=operator.itemgetter(1),reverse=True)).keys())
    final_list=list(set(filtered_users) & set(sorted_d))
    if user_id in final_list: final_list.remove(user_id)
    final_users=[db.query(User).filter(User.id==id).one() for id in final_list]
    return final_users


def get_my_profession_match_users(db: Session, user_id: int):
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))
    user_profession=db.query(UserProfessionalDetails).filter(UserProfessionalDetails.is_deleted==False,UserProfessionalDetails.user_id==user_id).first()
    if user_profession:
        occupation_id=user_profession.occupation_id
        if occupation_id:
            occupation_match = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.occupation_id == occupation_id).all()
            # occupation_match = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.occupation_id == occupation).all()

            occu_list=[i.user_id for i in occupation_match]
            final_intersection_list = list(set(active_genderpreff_users) & set(occu_list))
            # priority_list=[i for i in priority_users(db)]
            # users_in_riority=list(set(priority_list) & set(final_intersection_list))
            # final_list=[*filter(lambda x: x in set(final_intersection_list), users_in_riority), *filter(lambda x: x not in set(users_in_riority), final_intersection_list)]
            final_list=final_intersection_list
            if user_id in final_list: final_list.remove(user_id)
            final_users=[db.query(User).filter(User.id==id).one() for id in final_list]
            if len(final_users) == 0:
                final_users=data_occupation(db, user_id)
        else:

            final_users=data_occupation(db, user_id)
        return final_users


def get_education_match_list(db: Session, user_id: int):
    users_list = []
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    priority_list = priority_users(db)
    intersect_list_users = list(set(active_user_list) & set(gender_pref_users_list))
    profession = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.user_id == user_id).first()
    if profession:
        education = profession.highest_education_id
        if education:
            education_match = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.highest_education_id == education).all()
            count = len(education_match)
            match_list_ids = []
            for i in range(0,count):
                j = education_match[i].user_id
                match_list_ids.append(j)
            final_intersection_list = list(set(intersect_list_users) & set(match_list_ids))
            priority_intersect_final = list(set(final_intersection_list) & set(priority_list))
            if priority_intersect_final:
                for i in priority_intersect_final:
                    final_intersection_list.remove(i)
                priority_intersect_final.extend(final_intersection_list)
            else:
                priority_intersect_final = final_intersection_list

            for i in range(0,len(priority_intersect_final)):
                j = db.query(User).filter(User.id == priority_intersect_final[i]).first()
                users_list.append(j)

            if len(users_list) == 0:
                active_user_list = active_users(db, user_id)
                gender_pref_users_list = gender_pref_list(db,user_id)
                active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))
                user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
                main_list=[]
                if (user_profession_preff == None):
                    filtered_users=active_genderpreff_users
                    main_list.extend(filtered_users)
                else:
                    filtered_users=active_genderpreff_users
                    main_list.extend(filtered_users)

                    user_profession_preff=db.query(ProffesionalPartnerPreferences).filter(ProffesionalPartnerPreferences.is_deleted==False,ProffesionalPartnerPreferences.user_id==user_id).first()
                    education_strict = user_profession_preff.education_strict
                    education = user_profession_preff.education

                    if education_strict==True and education != None:
                        Edupref = db.query(UserProfessionalDetails).filter(UserProfessionalDetails.highest_education_id.in_(education['education'])).all()
                        edu_list=[i.id for i in Edupref]
                        filtered_users=list(set(active_genderpreff_users) & set(edu_list))
                        main_list.extend(filtered_users)
                    else:
                        filtered_users=active_genderpreff_users
                        main_list.extend(filtered_users)

                frequency = collections.Counter(main_list)
                sorted_d = list(dict( sorted(dict(frequency).items(), key=operator.itemgetter(1),reverse=True)).keys())
                final_list=list(set(filtered_users) & set(sorted_d))
                if user_id in final_list: final_list.remove(user_id)
                for id in final_list:
                    x=db.query(User).filter(User.id==id).one()
                    users_list.append(x)
                # final_users=[db.query(User).filter(User.id==id).one() for id in final_list]
            # print(len(users_list),"...............................>>")
            # return users_list
        else:
            pass
    else:
        pass
    return users_list

def get_new_users(db: Session, user_id: int):
    active = active_users(db, user_id)
    gender = gender_pref_list(db,user_id)
    active_intersect_gender = list(set(active) & set(gender))
    active_intersect_gender = active_intersect_gender.remove(user_id) if user_id in active_intersect_gender else active_intersect_gender
    users = db.query(User).filter(User.id.in_(active_intersect_gender)).order_by(desc(User.id)).limit(20).all()
    return users

def get_daily_recommendations(db: Session, user_id: int, once: str, **kwargs):
    """
    ### shows random 20 users

    (conditions: active, profile picture uploaded and verified, viewed a day not viewed another day unless no more users to show)
    """
    daily_show_count = 8

    shown_today = db.query(DailyRecommendations).filter(DailyRecommendations.user_id == user_id, DailyRecommendations.date == dt.datetime.now(timezone('Asia/Kolkata')).date()).first()
    if shown_today == None:
        active = active_users(db, user_id)
        gender = gender_pref_list(db,user_id)
        image_users = has_image(db)
        active_intersect_gender = list(set(active) & set(gender))
        image_intersect_aig = list(set(active_intersect_gender) & set(image_users))
        print(image_intersect_aig,"======================================image_intersect_aig")
        res = random.shuffle(image_intersect_aig)
        print(res,"--------------------------------------image_intersect_aig")
        already_shown = db.query(DailyRecommendations).filter(DailyRecommendations.user_id == user_id).order_by(DailyRecommendations.id.desc()).first()
        if already_shown:
            print("----------------------------------------already shown---------------------------------")
            shown_data = db.query(DailyRecommendations).filter(DailyRecommendations.user_id == user_id).all()
            shown_data_list = []

            for i in shown_data:
                if (i.user_list):
                    temp_list = i.user_list.split(",")
                    shown_data_list.extend(temp_list)
            shown_data_list = [int(i) for i in shown_data_list]
            shown_data_list = list(set(shown_data_list))
            for i in shown_data_list:
                if i in shown_data_list:
                    image_intersect_aig.remove(i)

        daily_user_list = []
        daily_user_list_id = []
        # if pending:
        #     daily_user_list_id.extend(pending)
        #     for i in range(0,len(pending)):
        #         j = db.query(User).filter(User.id == pending[i]).first()
        #         daily_user_list.append(j)
        for i in range(0,len(image_intersect_aig)):
            new_users = db.query(User).filter(User.id == image_intersect_aig[i]).first()
            daily_user_list.append(new_users)
            daily_user_list_id.append(new_users.id)
            if len(daily_user_list) == daily_show_count:
                break

        list_user = ','.join(map(str, daily_user_list_id))
        if ((len(daily_user_list_id)!=daily_show_count) and (once == "first")):
            delet = db.query(DailyRecommendations).filter(DailyRecommendations.user_id == user_id)
            delet.delete()
            db.commit()
            get_daily_recommendations(db, user_id,once = "second", pending = daily_user_list_id)

        db_daily = DailyRecommendations(user_id = user_id, date = dt.datetime.now(timezone('Asia/Kolkata')).date(), user_list = list_user )
        db.add(db_daily)
        db.commit()
        db.refresh(db_daily)

        return daily_user_list

    # when shown today has value
    else:
        ids = shown_today.user_list
        print(ids,"...............................>>ids........>>")
        ids = ids.split(",")
        daily_user_list = []
        if ids[0] != '':
            for i in range (0,len(ids)):
                new_users = db.query(User).filter(User.id == ids[i]).first()
                daily_user_list.append(new_users)
            return daily_user_list
        else:
            return []



def get_new_match_list(db: Session, user_id: int):
    calcultion_date = dt.datetime.now(timezone('Asia/Kolkata')) - dt.timedelta(days = new_calculation_days)
    active = active_users(db, user_id)
    priority = priority_users(db)
    gender = gender_pref_list(db,user_id)
    active_intersect_gender = list(set(active) & set(gender))
    priority_intersect_a_i_g = list(set(priority) & set(active_intersect_gender))
    current_user = db.query(User).filter(User.id == user_id).first()

    total_match_list_users=db.query(User).filter(User.id.in_(active_intersect_gender)).all()
    total_match_list_ids = []
    for i in total_match_list_users:
        match_dict, total, score = get_match_score(db,current_user,i)
        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        if percent >= config.MATCH_SCORE_PERCENT:
            total_match_list_ids.append(i.id)

    if priority_intersect_a_i_g: #2bd
        for i in priority_intersect_a_i_g:
            if i in total_match_list_ids:
                total_match_list_ids.remove(i)       #onget change (active_intersect_gender)
        priority_intersect_a_i_g.extend(total_match_list_ids)
    else:
        priority_intersect_a_i_g = total_match_list_ids

    new_user_list = []
    for i in range(0,len(priority_intersect_a_i_g)):
        new_users = db.query(User).filter(User.id == priority_intersect_a_i_g[i]).first()
        if new_users.date_joined > calcultion_date:
            new_user_list.append(new_users)

    return new_user_list


def Count_dict(db: Session, choices: list, table_list: list,):
    small_dict=[{i[0].value :i[1]}for i in table_list]
    all_dict=[{i:0} for i in choices]
    check_val = dict((list(i.keys())[0], list(i.values())[0]) for i in small_dict)
    output_dict = {list(i.keys())[0]: check_val.get(list(i.keys())[0]) if (check_val.get(list(i.keys())[0])) else 0 for i in all_dict }
    return output_dict

def Count_from_table(db: Session, dict_all: list, dict_small: list,):
    ds = [dict(dict_all), collections.Counter(list(itertools.chain(*dict_small)))]
    d = {}
    for k in dict(dict_all).keys():
      d[k] = tuple(d[k] for d in ds)
    _list=dict(list(d.values()))
    return _list

def Religion_table_count(db: Session, religion_count: list,  position:int):
    from itertools import groupby
    level_list=[]
    for r in religion_count:
        religion=db.query(Religion).filter(Religion.id==r[0]).with_entities(Religion.level_one,Religion.level_two,Religion.level_three).one()

        if religion[position] != None:
            tup_1=(religion[position],r[1])
            level_list.append(tup_1)
    result_one = { k : [*map(lambda v: v[1], values)]
        for k, values in groupby(sorted(level_list, key=lambda x: x[0]), lambda x: x[0])
        }
    for i in result_one:
        result_one[i]=sum(result_one[i])
    return result_one



import datetime
from datetime import datetime, timedelta
from datetime import datetime,date
def get_filter_components(db: Session, user_id: int):
    lst=[]
    active_user_list = active_users(db, user_id)
    gender_pref_users_list = gender_pref_list(db,user_id)
    active_genderpreff_users = list(set(active_user_list) & set(gender_pref_users_list))
    print(active_genderpreff_users,"..........>")
    queryset=db.query(User).filter(User.id.in_(active_genderpreff_users))

    last24HourDateTime = datetime.now(timezone('Asia/Kolkata')) - timedelta(hours = 24)
    with_in_a_day=queryset.filter(User.date_joined > last24HourDateTime ).count()

    last1WeekDateTime = datetime.now(timezone('Asia/Kolkata')) - timedelta(days=7)
    with_in_a_week=queryset.filter(User.date_joined > last1WeekDateTime ).count()

    last1MonthDateTime = datetime.now(timezone('Asia/Kolkata')) - timedelta(days = 30)
    with_in_a_month=queryset.filter(User.date_joined > last1MonthDateTime ).count()

    profile_created_for_lst=[ i[0] for i in UserBasicDetails.PROFILE_FOR ]
    profile_created=db.query(UserBasicDetails.profile_created_for,func.count(UserBasicDetails.profile_created_for)).filter(UserBasicDetails.is_deleted==False,UserBasicDetails.user_id.in_(active_genderpreff_users),UserBasicDetails.profile_created_for.in_(profile_created_for_lst)).group_by(UserBasicDetails.profile_created_for).all()
    profile_created_for_dict=Count_dict(db,profile_created_for_lst,profile_created)
    s=profile_created_for_dict['Son'] if profile_created_for_dict['Son'] else 0
    d=profile_created_for_dict['Daughter'] if profile_created_for_dict['Daughter'] else 0
    Parent=int(s)+int(d)
    del profile_created_for_dict["Son"]
    del profile_created_for_dict["Daughter"]
    profile_created_for_dict['Parent']=Parent

    has_image=db.query(UserImage).filter(UserImage.is_verified==True,UserImage.is_deleted==False,UserImage.user_id.in_(active_genderpreff_users)).all()
    imglst=[*set(i.user_id for i in has_image)]

    mother_tongue_lst=[ i[0] for i in UserBasicDetails.MOTHER_TONGUE ]
    marital_status_lst=[ i[0] for i in UserBasicDetails.MARITAL_STATUS ]
    body_type_lst=[ i[0] for i in UserBasicDetails.BODY_TYPE ]
    physical_status_lst=[ i[0] for i in UserBasicDetails.PHYSICAL_STATUS ]

    mother_tongue=db.query(UserBasicDetails.mother_tongue,func.count(UserBasicDetails.mother_tongue)).filter(UserBasicDetails.is_deleted==False,UserBasicDetails.user_id.in_(active_genderpreff_users),UserBasicDetails.mother_tongue.in_(mother_tongue_lst)).group_by(UserBasicDetails.mother_tongue).all()
    marital_status=db.query(UserBasicDetails.marital_status,func.count(UserBasicDetails.marital_status)).filter(UserBasicDetails.is_deleted==False,UserBasicDetails.user_id.in_(active_genderpreff_users),UserBasicDetails.marital_status.in_(marital_status_lst)).group_by(UserBasicDetails.marital_status).all()
    physical_status=db.query(UserBasicDetails.physical_status,func.count(UserBasicDetails.physical_status)).filter(UserBasicDetails.is_deleted==False,UserBasicDetails.user_id.in_(active_genderpreff_users),UserBasicDetails.physical_status.in_(physical_status_lst)).group_by(UserBasicDetails.physical_status).all()
    body_type=db.query(UserBasicDetails.body_type,func.count(UserBasicDetails.body_type)).filter(UserBasicDetails.is_deleted==False,UserBasicDetails.user_id.in_(active_genderpreff_users),UserBasicDetails.body_type.in_(body_type_lst)).group_by(UserBasicDetails.body_type).all()

    employed_in_lst=[ i[0] for i in UserProfessionalDetails.EMPLOYED_IN ]
    residential_status_lst=[ i[0] for i in UserProfessionalDetails.RESIDENTIAL_STATUS ]
    annual_income_lst=[ i[0] for i in UserProfessionalDetails.INCOME_CHOICE ]

    employed_in=db.query(UserProfessionalDetails.employed_in,func.count(UserProfessionalDetails.employed_in)).filter(UserProfessionalDetails.is_deleted==False,UserProfessionalDetails.user_id.in_(active_genderpreff_users),UserProfessionalDetails.employed_in.in_(employed_in_lst)).group_by(UserProfessionalDetails.employed_in).all()
    residential_status=db.query(UserProfessionalDetails.residential_status,func.count(UserProfessionalDetails.residential_status)).filter(UserProfessionalDetails.is_deleted==False,UserProfessionalDetails.user_id.in_(active_genderpreff_users),UserProfessionalDetails.residential_status.in_(residential_status_lst)).group_by(UserProfessionalDetails.residential_status).all()
    annual_income=db.query(UserProfessionalDetails.annual_income,func.count(UserProfessionalDetails.annual_income)).filter(UserProfessionalDetails.is_deleted==False,UserProfessionalDetails.user_id.in_(active_genderpreff_users),UserProfessionalDetails.annual_income.in_(annual_income_lst)).group_by(UserProfessionalDetails.annual_income).all()

    family_type_lst=[ i[0] for i in UserFamilyDetails.FAMILY_TYPE ]
    family_values_lst=[ i[0] for i in UserFamilyDetails.FAMILY_VALUES ]
    family_status_lst=[ i[0] for i in UserFamilyDetails.FAMILY_STATUS ]

    family_type=db.query(UserFamilyDetails.family_type,func.count(UserFamilyDetails.family_type)).filter(UserFamilyDetails.is_deleted==False,UserFamilyDetails.user_id.in_(active_genderpreff_users),UserFamilyDetails.family_type.in_(family_type_lst)).group_by(UserFamilyDetails.family_type).all()
    family_values=db.query(UserFamilyDetails.family_values,func.count(UserFamilyDetails.family_values)).filter(UserFamilyDetails.is_deleted==False,UserFamilyDetails.user_id.in_(active_genderpreff_users),UserFamilyDetails.family_values.in_(family_values_lst)).group_by(UserFamilyDetails.family_values).all()
    family_status=db.query(UserFamilyDetails.family_status,func.count(UserFamilyDetails.family_status)).filter(UserFamilyDetails.is_deleted==False,UserFamilyDetails.user_id.in_(active_genderpreff_users),UserFamilyDetails.family_status.in_(family_status_lst)).group_by(UserFamilyDetails.family_status).all()

    food_habits_lst=[ i[0] for i in UserLifeStyle.EATING ]
    smoking_habits_lst=[ i[0] for i in UserLifeStyle.SMOKING ]
    drinking_habits_lst=[ i[0] for i in UserLifeStyle.DRINKING ]

    food_habits=db.query(UserLifeStyle.food_habits,func.count(UserLifeStyle.food_habits)).filter(UserLifeStyle.is_deleted==False,UserLifeStyle.user_id.in_(active_genderpreff_users),UserLifeStyle.food_habits.in_(food_habits_lst)).group_by(UserLifeStyle.food_habits).all()
    smoking_habits=db.query(UserLifeStyle.smoking_habits,func.count(UserLifeStyle.smoking_habits)).filter(UserLifeStyle.is_deleted==False,UserLifeStyle.user_id.in_(active_genderpreff_users),UserLifeStyle.smoking_habits.in_(smoking_habits_lst)).group_by(UserLifeStyle.smoking_habits).all()
    drinking_habits=db.query(UserLifeStyle.drinking_habits,func.count(UserLifeStyle.drinking_habits)).filter(UserLifeStyle.is_deleted==False,UserLifeStyle.user_id.in_(active_genderpreff_users),UserLifeStyle.drinking_habits.in_(drinking_habits_lst)).group_by(UserLifeStyle.drinking_habits).all()

    country=db.query(UserLocationDetails.country,func.count(UserLocationDetails.country)).filter(UserLocationDetails.is_deleted==False,UserLocationDetails.user_id.in_(active_genderpreff_users)).group_by(UserLocationDetails.country).all()
    citizenship=db.query(UserLocationDetails.citizenship,func.count(UserLocationDetails.citizenship)).filter(UserLocationDetails.is_deleted==False,UserLocationDetails.user_id.in_(active_genderpreff_users)).group_by(UserLocationDetails.citizenship).all()

    education_all=db.query(Education).filter(Education.is_deleted==False).with_entities(Education.id,Education.qualification).all()
    education=db.query(UserProfessionalDetails).filter(UserProfessionalDetails.is_deleted==False,UserProfessionalDetails.user_id.in_(active_genderpreff_users)).with_entities(UserProfessionalDetails.highest_education_id).all()
    occupation_all=db.query(Occupation).filter(Occupation.is_deleted==False).with_entities(Occupation.id,Occupation.occupation_name).all()
    occupation=db.query(UserProfessionalDetails).filter(UserProfessionalDetails.is_deleted==False,UserProfessionalDetails.user_id.in_(active_genderpreff_users)).with_entities(UserProfessionalDetails.occupation_id).all()
    star_all=db.query(Star).filter(Star.is_deleted==False).with_entities(Star.id,Star.star_name).all()
    star=db.query(UserReligionInfo).filter(UserReligionInfo.is_deleted==False,UserReligionInfo.user_id.in_(active_genderpreff_users)).with_entities(UserReligionInfo.star_id).all()

    religion_count=db.query(UserReligionInfo.religion_id,func.count(UserReligionInfo.religion_id)).filter(UserReligionInfo.is_deleted==False,UserReligionInfo.user_id.in_(active_genderpreff_users)).group_by(UserReligionInfo.religion_id).all() #UserReligionInfo.user_id.in_(active_genderpreff_users)
    package_log_id = db.query(PackageLog).filter(PackageLog.is_deleted==False).with_entities(PackageLog.id).all()
    premium_members = db.query(SubscribePackage).filter(SubscribePackage.is_deleted==False,SubscribePackage.user_id.in_(active_genderpreff_users),SubscribePackage.expiry_date >= date.today(),SubscribePackage.package_log_id.in_(list(itertools.chain(*package_log_id)))).count()

    output_data={
        "profile_type":{
            "show_profiles_created": {
                "with_in_a_day":with_in_a_day,
                "with_in_a_week":with_in_a_week,
                "with_in_a_month":with_in_a_month,
            },
            "profiles": {
                "with_photo":len(imglst),
                # "currently_online":None,
                "premium_members":premium_members,
            },
            "profiles_created_by": profile_created_for_dict,
        },
        "basic_details":{
            "age":None,
            "height":None,
            "mother_tongue":Count_dict(db,mother_tongue_lst,mother_tongue),
            "marital_status":Count_dict(db,marital_status_lst,marital_status),
        },
        "religious_details":{
            "religion":Religion_table_count(db, religion_count,0 ),
            "sub_caste":Religion_table_count(db, religion_count, 2),
            "caste":Religion_table_count(db, religion_count,1 ),
            "star":Count_from_table(db,star_all,star),
        },
        "proffessional_details":{
            "education":Count_from_table(db,education_all,education),
            "employed_in":Count_dict(db,employed_in_lst,employed_in),
            "occupation":Count_from_table(db,occupation_all,occupation),
            "annual_income":Count_dict(db,annual_income_lst,annual_income),
        },
        "location_details":{
            "country":dict(country),
            "resident_status":Count_dict(db,residential_status_lst,residential_status),
            "citizenship":dict(citizenship),
        },
        "personal_details":{
            "physical_status":Count_dict(db,physical_status_lst,physical_status),
            "body_type":Count_dict(db,body_type_lst,body_type),
        },

        "lifestyle_habits":{
            "eating_habits":Count_dict(db,food_habits_lst,food_habits),
            "smoking_habits":Count_dict(db,smoking_habits_lst,smoking_habits),
            "drinking_habits":Count_dict(db,drinking_habits_lst,drinking_habits),

        },
        "family_details":{
            "family_values":Count_dict(db,family_values_lst,family_values),
            "family_status":Count_dict(db,family_status_lst,family_status),
            "family_type":Count_dict(db,family_type_lst,family_type),
        }
    }
    lst.append(output_data)
    return lst
