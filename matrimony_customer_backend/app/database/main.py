
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from config import Settings
import pymysql


pymysql.install_as_MySQLdb()

settings = Settings()
# SQLALCHEMY_DATABASE_URL = "mysql://"+settings.db_username+":" + \
#     settings.db_password+"@"+settings.db_host+"/"+settings.db_name
SQLALCHEMY_DATABASE_URL = settings.db_url
engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args=dict(
    host=settings.db_host, port=int(settings.db_port)))
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()