from typing import Optional
from pydantic import BaseModel

class SuccessStoriesBase(BaseModel):
    id: int
    title: Optional[str]
    image: Optional[str]
    desc: Optional[str]

    class Config:
        orm_mode = True
    