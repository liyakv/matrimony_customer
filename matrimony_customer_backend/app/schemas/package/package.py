from pydantic import BaseModel,Json
from typing import List, Optional
from datetime import datetime
from fastapi import File, UploadFile
from ..profile.profile import UserBase

class PackageBase(BaseModel):
    id: int
    package_name: str
    months: int
    amount: int
    is_premium: bool
    view_profiles: bool
    send_interest: bool
    recomd_profiles: bool
    advced_privacy_optn: bool
    send_ultd_prnal_msg: bool
    prty_in_srch_rslt: bool
    vw_cnts_adrs: bool
    prfl_tgd_as_pd_membr: bool
    sms_alert: bool
    notifications: bool
    vw_socl_mda_prfl_urs: bool
    persanal_astnce: bool
    create_quize: bool
    attend_quize: bool
    vw_al_phts: bool
    rqst_fr_phts: bool

    class Config:
        orm_mode = True

class PackageLogBase(BaseModel):
    id: int
    logged_package: PackageBase
    package_name: str
    months: int
    amount: int
    is_premium: bool
    view_profiles: bool
    send_interest: bool
    recomd_profiles: bool
    advced_privacy_optn: bool
    send_ultd_prnal_msg: bool
    prty_in_srch_rslt: bool
    vw_cnts_adrs: bool
    prfl_tgd_as_pd_membr: bool
    sms_alert: bool
    notifications: bool
    vw_socl_mda_prfl_urs: bool
    persanal_astnce: bool
    create_quize: bool
    attend_quize: bool
    vw_al_phts: bool
    rqst_fr_phts: bool
    log_status: str

    class Config:
        orm_mode = True


class SubscribePackageBase(BaseModel):
    id: int
    package_log_subscribed: PackageLogBase
    user_subscribed: UserBase
    expiry_date: Optional[datetime]
    subscription_status: str

    class Config:
        orm_mode = True


class PaymentBase(BaseModel):
    id: int
    user_payments: UserBase
    payment_id: str
    payment_method: str
    amount_paid: float
    subscribe_payments: SubscribePackageBase
    status: str

    class Config:
        orm_mode = True

class PackageId(BaseModel):
    id: int
    class Config:
        orm_mode = True

class PaymentCreate(BaseModel):
    paid_user_id: int
    payment_id: str
    payment_method: str
    amount_paid: float
    order_id: int
    status: str