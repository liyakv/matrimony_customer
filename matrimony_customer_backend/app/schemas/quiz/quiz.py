from pydantic import BaseModel, Json
from typing import Optional, List, Literal
from ..profile.profile import UserBase


class QuizQuestionChoiceBase(BaseModel):
    id: int
    choice: str

    class Config:
        orm_mode = True


class QuizQuestionsBase(BaseModel):
    id: int
    created_by: Literal['admin', 'user']
    question_type: Literal['multiple choice', 'rate', 'open ended']
    question: str
    is_verified: Optional[bool]
    quiz_question_choices: List[QuizQuestionChoiceBase]

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True


class AdminQuizQuestionsBase(BaseModel):
    id: int
    question: str

    class Config:
        orm_mode = True


class UserQuizQuestionsBase(BaseModel):
    id: int
    question: QuizQuestionsBase

    class Config:
        orm_mode = True


class UserQuizAnswersBase(BaseModel):
    id: int
    question_type: str
    question: str
    answer: str


class QuizUserQuestionCreate(BaseModel):
    id: Optional[int]
    question_id: Optional[int]
    question_type: int
    question: str
    choices: List[str]


class QuizQuestionCreate(BaseModel):
    userQuestions: List[QuizUserQuestionCreate]
    adminQuestions: List[int]
    removedQuestions: List[int]


class QuizAnswerPartnerID(BaseModel):
    partner_id: int


class QuizAnswerID(BaseModel):
    answer_id: int


class QuizQuestionAnswer(BaseModel):
    question_id: int
    answer: str