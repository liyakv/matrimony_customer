from pydantic import BaseModel,Json
from typing import Dict, List, Optional,Literal
from sqlalchemy.sql.sqltypes import DateTime
from datetime import date, datetime
from fastapi import File, UploadFile


class FilterParameters(BaseModel):
    search_key: Optional[str]
    show_profiles_created: Optional[str]
    profiles: List
    profiles_created_by: List
    age_upper_limit: Optional[int]
    age_lower_limit: Optional[int]
    height_upper_limit: Optional[int]
    height_lower_limit: Optional[int]
    mother_tongue: List
    marital_status: List
    religion: Optional[str]
    caste: List
    sub_caste: List
    star: List
    education: List
    employed_in: List
    occupation: List
    annual_income: Optional[str]
    country: List
    resident_status: List
    citizenship: List
    physical_status: List
    body_type: List
    eating_habits: List
    smoking_habits: List
    drinking_habits: List
    family_values: List
    family_type: List
    family_status: List