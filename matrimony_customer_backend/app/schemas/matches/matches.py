from datetime import datetime
from pydantic import BaseModel, Json
from ..profile.profile import UserBase, ProfileCard
from typing import Optional


class ProfileMatchesBase(BaseModel):
    id: int
    # name: str
    # profile_created_for: Optional[str]
    # is_user_verified: Optional[bool]
    # image: Optional[str]
    # details: Optional[ProfileCard]
    # is_viewed: bool
    # created_on: datetime
    # match_score_total: Optional[int]
    # match_score_matching_nos: Optional[int]
    # match_score: Optional[float]

    class Config:
        orm_mode = True


class PartnerIDMatch(BaseModel):
    type: bool
    to_id: int