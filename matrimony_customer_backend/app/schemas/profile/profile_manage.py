from pydantic import BaseModel,Json
from typing import Dict, List, Optional,Literal
from sqlalchemy.sql.sqltypes import DateTime
from datetime import date, datetime


class UserCreate(BaseModel):
    id: Optional[int]
    name: Optional[str]
    email: Optional[str]
    gender: Optional[str]
    dob:  Optional[date]


class UserReligionCreate(BaseModel):
    id: Optional[int]
    dosham: Optional[Literal["Yes", "No", "Doesn't Know"]]
    raasi: Optional[str]
    gothram: Optional[str]
    place_of_birth: Optional[str]
    time_of_birth: Optional[str]
    star_id: Optional[int]
    level_one : Optional[str]
    level_two : Optional[str]
    level_three : Optional[str]


class UserBasicCreate(BaseModel):
    id: Optional[int]
    profile_created_for: Optional[Literal['Self', 'Daughter', 'Son', 'Siblings', 'Relative', 'Friend']]
    mother_tounge: Optional[Literal['Malayalam', 'Hindi', 'Tamil', 'Punjabi', 'Gujarati', 'Bengla', 'Telugu', 'marathi', 'Konkani', 'Kannada', 'Nepali', 'Lushai', 'Manipuri', 'Assamese', 'Nissi']]
    body_type: Optional[Literal['Slim', 'Average', 'Athletic', 'Heavy']]
    physical_status: Optional[Literal['Normal', 'Physically Challenged']]
    skin_color: Optional[Literal['White', 'Whitish', 'Brown', 'Dark Brown']]
    height: Optional[int]
    weight: Optional[int]
    marital_status: Optional[Literal['Never Married', 'Widowed', 'Divorced', 'Awaiting Divorce']]
    get_married_in: Optional[Literal['ASAP', 'In 6 Months', 'In an Year', 'With in 2 Years']]
    about_me: Optional[str]
    have_child: Optional[bool]


class UserLifestyleCreate(BaseModel):
    id:Optional[int]
    smoking_habits: Optional[Literal['Smokes regularly', 'Smokes rarely', 'Never smokes']]
    drinking_habits: Optional[Literal['Drinks regularly', 'Drinks rarely', 'Never Drinks']]
    food_habits: Optional[Literal['Vegetarian', 'Non Vegetarian', 'Eggetarian']]
    hobbies: List
    interests: List
    favourite_music: List
    favourite_books: List
    preffered_movies: List
    sports_activities: List
    fitness_activities: List
    favourite_cuisine: List
    preffered_dress_style: List
    spoken_languages: List

class UserSocialMediaCreate(BaseModel):
    id: Optional[int]
    link: str
    social_media_profile: Literal['Facebook', 'Instagram', 'Twitter', 'Youtube Channel']


class UserFamilyCreate(BaseModel):
    id: Optional[int]
    family_type: Optional[Literal['Joint', 'Nuclear']]
    family_values: Optional[Literal['Orthodox', 'Traditional', 'Moderate', 'Liberal']]
    family_status: Optional[Literal['Middle Class', 'Upper Middle Class', 'Rich', 'Affluent']]
    living_status: Optional[Literal['With Family', 'Seperate']]
    description: Optional[str]
    father_name: Optional[str]
    father_occupation_id: Optional[int]
    mother_name: Optional[str]
    mother_occupation_id: Optional[int]


class UserSiblingCreate(BaseModel):
    id: Optional[int]
    siblings_type: Optional[Literal['Younger', 'Elder']]
    relation: Optional[Literal['Brother', 'Sister']]
    marital_status: Optional[Literal['Married', 'Not Married']]
    count:  Optional[int]


class UserProfessionalCreate(BaseModel):
    id: Optional[int]
    highest_education_id: Optional[int]
    institutions: Optional[str]
    area_of_specialization_id: Optional[int]
    employed_in: Optional[Literal['Government', 'PSU', 'Private', 'Business', 'Defence', 'Self-employed', 'Not working']]
    occupation_id: Optional[int]
    organization: Optional[str]
    role: Optional[str]
    currency_type: Optional[Literal['INR', 'AED', 'CAD', 'USD', 'EUR', 'GBP', 'CHF', 'ZAR']]
    annual_income: Optional[Literal['Less than 20,000', '20,000 to 40,000', '40,000 to 65,000', '65,000 to 1,00,000', '1,00,000 to 2,00,000', '2,00,000 to 5,00,000', '5,00,000 to 10,00,000', '10,00,000 to 20,00,000', '20,00,000 to 50,00,000', '50,00,000 to 1,00,00,000', 'More than 1,00,00,000']]
    work_location: Optional[str]
    work_state: Optional[str]
    work_city: Optional[str]
    work_citizenship: Optional[str]
    residential_status: Optional[Literal['Permanent Resident', 'Work Permit', 'Student Visa', 'Temporary Visa']]


class UserLocationCreate(BaseModel):
    id: Optional[int]
    country: Optional[str]
    state: Optional[str]
    city: Optional[str]
    district: Optional[str]
    citizenship: Optional[str]
    ancestral_origin: Optional[str]
    longitude: Optional[str]
    latitude: Optional[str]


class UserProfileQuestionCreate(BaseModel):
    id: Optional[int]
    question: str
    answer: str


class UserProfileUpdate(BaseModel):
    user_main: Optional[UserCreate]
    user_religion: Optional[UserReligionCreate]
    user_basic: Optional[UserBasicCreate]
    user_lifestyle: Optional[UserLifestyleCreate]
    user_socialmedia: List[UserSocialMediaCreate]
    user_family: Optional[UserFamilyCreate]
    user_siblings: List[UserSiblingCreate]
    user_profession: Optional[UserProfessionalCreate]
    user_location: Optional[UserLocationCreate]
    user_profile_question: List[UserProfileQuestionCreate]

class Subcastelist(BaseModel):
    idsubcaste: List