from pydantic import BaseModel
from ..profile.profile import UserBase


class DailyRecommendationsBase(BaseModel):
    id: int
    user_recommendations: UserBase
    user_recommended_to: UserBase
    is_viewed: bool

    class Config:
        orm_mode = True