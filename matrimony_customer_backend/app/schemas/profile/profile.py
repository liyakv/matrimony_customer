from pydantic import BaseModel,Json
from typing import Dict, List, Optional,Literal
from sqlalchemy.sql.sqltypes import DateTime
from datetime import date, datetime
from fastapi import File, UploadFile



class EducationBase(BaseModel):
    id: Optional[int]
    qualification: Optional[str]

    class Config:
        orm_mode = True


class AreaOfSpecializationBase(BaseModel):
    id: Optional[int]
    area_of_specialization: Optional[str]
    education_table: Optional[EducationBase]

    class Config:
        orm_mode = True


class OccupationBase(BaseModel):
    id: Optional[int]
    occupation_name: Optional[str]

    class Config:
        orm_mode = True


class ReligionBase(BaseModel):
    id: Optional[int]
    religion_name: Optional[str]

    class Config:
        orm_mode = True


class CasteBase(BaseModel):
    id: Optional[int]
    caste_name: Optional[str]
    religion: Optional[ReligionBase]

    class Config:
        orm_mode = True


class StarBase(BaseModel):
    id: Optional[int]
    star_name: Optional[str]

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    id: int
    username: str
    email: Optional[str]
    gender: Optional[str]
    phone_number: str
    dob: Optional[datetime]
    is_mobile_verified: bool

    class Config:
        orm_mode = True


class UserReligion(BaseModel):
    id: int
    dosham: Optional[str]
    raasi: Optional[str]
    gothram: Optional[str]
    place_of_birth: Optional[str]
    time_of_birth: Optional[str]
    from_other_religion: bool
    from_other_caste: bool
    user_star_table: StarBase
    user_caste_table: CasteBase
    user_religion_table : ReligionBase
    user_religion_details: UserBase

    class Config:
        orm_mode = True


class UserBasic(BaseModel):
    id: int
    profile_created_for: str
    mother_tongue: str
    body_type: str
    physical_status: str
    skin_color: str
    height: int
    weight: int
    marital_status: str
    get_married_in: str
    about_me: Optional[str]
    have_child: str
    user_basic_details: UserBase

    class Config:
        orm_mode = True


class UserLifestyle(BaseModel):
    id:int
    smoking_habits: bool
    drinking_habits: bool
    food_habits: str
    hobbies: Optional[Json]
    interests: Optional[Json]
    favourite_music: Optional[Json]
    favourite_books: Optional[Json]
    preffered_movies: Optional[Json]
    sports_activities: Optional[Json]
    fitness_activities: Optional[Json]
    favourite_cuisine: Optional[Json]
    preffered_dress_style: Optional[Json]
    spoken_languages: Optional[Json]
    user_lifestyle_details: UserBase

    class Config:
        orm_mode = True


class UserSocialMedia(BaseModel):
    id: int
    link: str
    user_socialmedia_details: UserBase

    class Config:
        orm_mode = True


class UserFamily(BaseModel):
    id: int
    family_type: str
    family_values: str
    family_status: str
    living_status: str
    description: str
    user_family_details: UserBase

    class Config:
        orm_mode = True


class UserSiblings(BaseModel):
    id: int
    siblings_type: str
    relation: str
    marital_status: str
    count: int
    user_siblings_details: UserBase

    class Config:
        orm_mode = True


class UserProffessional(BaseModel):
    id: int
    user_education_details: EducationBase
    institutions: str
    user_specialized_details: AreaOfSpecializationBase
    employed_in: str
    user_occupation_details: OccupationBase
    organization: str
    role: str
    crrency_type: str
    annual_income: str
    work_location: str
    state: str
    city: str
    citizenship: str
    residential_status: str
    #user relation

    class Config:
        orm_mode = True


class UserLocation(BaseModel):
    id: int
    country: str
    state: str
    city:str
    district: str
    citizenship: str
    longitude: str
    latitude: str
    user_location_details: UserBase

    class Config:
        orm_mode = True


class UserImagePost(BaseModel):
    images: str

    class Config:
        orm_mode = True

class UserImagesGet(BaseModel):
    id: int
    image: str
    is_main: Optional[bool]


    class Config:
        orm_mode = True


class AdditionalImageBase(BaseModel):
    id: int
    additional_image_table: UserBase
    image: str
    is_verified: bool
    section: str
    tag: Optional[Json]

    class Config:
        orm_mode = True


class DocumentsBase(BaseModel):
    id: int
    status: Optional[Literal['pending', 'approved','Passport', 'rejected', None]]
    document: str
    doc_type: Optional[Literal['Aadhar card', 'Driving license','Passport', 'Voter id', 'Pan card',None]]
    
    # is_verified: bool

    class Config:
        orm_mode = True

class DocumentsBaseAll(BaseModel):
    details: List[DocumentsBase]
    photo_verified_status:bool
    
    # is_verified: bool

    class Config:
        orm_mode = True

#Register------------------------

class UserInitialRegister(BaseModel):
    profile_created_for: str
    name: str
    phone_number: str

    class Config:
        orm_mode = True


class Otp(BaseModel):
    otp: int

    class Config:
        orm_mode = True

# ----

class UserRegistrationMainBase(BaseModel):
    gender: Optional[str]
    dob: Optional[date]
    email: Optional[str]
    # religion_id : Optional[int]
    # caste_id: Optional[int]
    level_one: Optional[str]
    level_two: Optional[str]
    level_three: Optional[str]
    mother_tongue: Optional[str]
    marital_status: Optional[str]
    country: Optional[str]
    state: Optional[str]
    city: Optional[str]
    # height_in_ft: Optional[str]
    height_in_cm: Optional[int]
    weight: Optional[int]
    physical_status: Optional[str]
    highest_education_id: Optional[int]
    employed_in: Optional[str]
    occupation_id: Optional[int]
    currency_type: Optional[str]
    annual_income: Optional[str]
    family_status: Optional[str]
    family_type: Optional[str]
    family_values: Optional[str]
    about_me: Optional[str]


class ProfileCard(BaseModel):
    age_year: Optional[int]
    age_month: Optional[int]
    height_in_ft: Optional[str]
    mother_tongue: Optional[str]
    city: Optional[str]
    district: Optional[str]
    state: Optional[str]
    level_one: Optional[str]
    level_two: Optional[str]
    star_name: Optional[str]
    raasi: Optional[str]
    qualification: Optional[str]
    employed_in: Optional[Literal['Government','PSU','Private','Business','Defence','Self-employed', 'Not working']]
    occupation_name: Optional[str]
    annual_income: Optional[Literal['Less than 20,000','20,000 to 40,000','40,000 to 65,000','65,000 to 1,00,000','1,00,000 to 2,00,000','2,00,000 to 5,00,000','5,00,000 to 10,00,000','10,00,000 to 20,00,000','20,00,000 to 50,00,000','50,00,000 to 1,00,00,000','More than 1,00,00,000']]
    currency_type: Optional[Literal['INR','AED','CAD','USD','EUR','GBP','CHF','ZAR']]




class EmailUpdate(BaseModel):
    email: str


# --user_registration_datas


class ReligionRegDatasBase(BaseModel):
    religion_id: Optional[int]
    value: Optional[str]
    label: Optional[str]

    class Config:
        orm_mode = True


class CasteRegDatasBase(BaseModel):
    religion_id: Optional[int]
    value: Optional[str]
    label: Optional[str]

    class Config:
        orm_mode = True


class SubCasteRegDatasBase(BaseModel):
    religion_id: Optional[int]
    value: Optional[str]
    label: Optional[str]

    class Config:
        orm_mode = True


class EducationRegDatasBase(BaseModel):
    value: Optional[int]
    label: Optional[str]

    class Config:
        orm_mode = True


class OccupationRegDatasBase(BaseModel):
    value: Optional[int]
    label: Optional[str]

    class Config:
        orm_mode = True



class UserRegDatas(BaseModel):
    religion: Optional[List[ReligionRegDatasBase]]
    caste: Optional[List[CasteRegDatasBase]]
    sub_caste: Optional[List[SubCasteRegDatasBase]]
    mother_tounge: Optional[List]
    marital_status: Optional[List]
    physical_status: Optional[List]
    highest_education: Optional[List[EducationRegDatasBase]]
    employed_in: Optional[List]
    occupation: Optional[List[OccupationRegDatasBase]]
    currency_type: Optional[List]
    annual_income: Optional[List]
    family_status: Optional[List]
    family_type: Optional[List]
    family_values: Optional[List]


# ///documents_type_datas///
class DocTypeDatas(BaseModel):
    doc_types: Optional[List]


class PrivacyOptionCreate(BaseModel):
    type: str
    privacy_option: str


class PrivacyOptionBase(BaseModel):
    type: Literal['photo','phone_number']
    privacy_option: Literal['Everyone','Verified account only','By request only']

    class Config:
        orm_mode = True
class DeactivationReason(BaseModel):
    reason: Optional[str]
