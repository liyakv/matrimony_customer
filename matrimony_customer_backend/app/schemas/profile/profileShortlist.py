from datetime import datetime
from pydantic import BaseModel, Json
from ..profile.profile import UserBase, ProfileCard
from typing import List, Optional


class UserVerificationBase(BaseModel):
    is_number_verified: bool
    is_photos_verified: bool
    is_documents_verified: bool


class ProfileShortlistBase(BaseModel):
    shortlist_id: int
    partner_id: int
    name: str
    gender: str
    profile_created_by: Optional[str]
    is_user_verified: UserVerificationBase
    image: Optional[str]
    details: Optional[ProfileCard]
    is_viewed: bool
    created_on: datetime
    updated_on: Optional[datetime]
    match_score_total: Optional[int]
    match_score_matching_nos: Optional[int]
    match_score: Optional[float]
    is_shortlisted: bool
    is_interest_send: bool


class ShortListCreate(BaseModel):
    user_id: int


class ShortListUpdate(BaseModel):
    shortlist_id: int


# ***pagination***
class ProfileShortlistBasePagination(BaseModel):
    users : List[ProfileShortlistBase]
    total: int
    count: int
    pagination: dict

    class Config:
        orm_mode = True
