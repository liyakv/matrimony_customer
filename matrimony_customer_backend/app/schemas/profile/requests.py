from pydantic import BaseModel
from .profile import UserBase


class PhotoRequestsBase(BaseModel):
    id: int
    user_requested_photos: UserBase
    user_received_photo_requests: UserBase
    status: str
    is_viewed: bool

    class Config:
        orm_mode = True


class PhotoRequestsCreate(BaseModel):
    user_id: int


class PhotoRequestsUpdate(BaseModel):
    request_id: int
    status: str


class NumberRequestsBase(BaseModel):
    id: int
    user_requested_numbers: UserBase
    user_received_number_requests: UserBase
    status: str
    is_viewed: bool

    class Config:
        orm_mode = True


class NumberRequestsCreate(BaseModel):
    user_id: int

class SendReminder(BaseModel):
    user_id: int


class NumberRequestsUpdate(BaseModel):
    request_id: int
    status: str