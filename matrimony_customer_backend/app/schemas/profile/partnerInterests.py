from datetime import datetime
from pydantic import BaseModel, Json
from ..profile.profile import UserBase
from datetime import datetime
from typing import List, Optional
from ..profile.profile import ProfileCard


class PartnerInterestsBase(BaseModel):
    id: int
    user_sended_interests: UserBase
    user_received_interests: UserBase
    status: str
    is_viewed: bool

    class Config:
        orm_mode = True


class PartnerInterestCreate(BaseModel):
    user_id: int


class PartnerInterestUpdate(BaseModel):
    request_id: int
    status: str


class PartnerInterestReminderBase(BaseModel):
    id: int
    partner_interest_reminder: PartnerInterestsBase
    is_viewed: bool

    class Config:
        orm_mode = True



# //InterestSent//
class UserBasicDetailsInterestSent(BaseModel):
    about_me : str

    class Config:
        orm_mode = True


class InterestSentUserBase(BaseModel):
    id: int
    name:str
    username :str
    is_user_verified :bool
    user_basic_details: List[UserBasicDetailsInterestSent]

    class Config:
        orm_mode = True


class InterestSentCheckBase(BaseModel):
    id: int
    from_user_id: int
    to_user_id: int
    to_user: InterestSentUserBase


    class Config:
        orm_mode = True

# ************
class IsUserVerifiedBase(BaseModel):
    is_number_verified: bool
    is_photos_verified: bool
    is_documents_verified: bool


class InterestSentDetailsBase(BaseModel):
    interest_id: int
    partner_id: int
    name: str
    gender: str
    profile_created_by: Optional[str]
    is_user_verified: IsUserVerifiedBase
    image: Optional[str]
    details: Optional[ProfileCard]
    is_viewed: bool
    status: Optional[str]
    updated_on: Optional[datetime]
    created_on: datetime
    match_score: Optional[float]
    match_score_total: Optional[float]
    match_score_matching_nos: Optional[float]
    is_shortlisted: bool
    is_interest_send: bool

    class Config:
        orm_mode = True


class InterestReceivedDetailsBase(BaseModel):
    interest_id: int
    partner_id: int
    name: str
    gender: str
    question_add: bool
    is_answer_attended: bool
    profile_created_by: Optional[str]
    is_user_verified: IsUserVerifiedBase
    image: Optional[str]
    details: Optional[ProfileCard]
    is_viewed: bool
    status: Optional[str]
    updated_on: Optional[datetime]
    created_on: datetime
    match_score: Optional[float]
    match_score_total: Optional[float]
    match_score_matching_nos: Optional[float]
    is_shortlisted: bool
    is_interest_send: bool

    class Config:
        orm_mode = True


# *** pagination - getInterestReceivedListPagination ***
class InterestReceivedDetailsBasePagination(BaseModel):
    users : List[InterestReceivedDetailsBase]
    total: int
    count: int
    pagination: dict

    class Config:
        orm_mode = True


# *** pagination - getInterestSentByUserPagination ***
class InterestSentDetailsBasePagination(BaseModel):
    users : List[InterestSentDetailsBase]
    total: int
    count: int
    pagination: dict

    class Config:
        orm_mode = True

