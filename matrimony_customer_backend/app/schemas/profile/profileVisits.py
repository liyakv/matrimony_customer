from pydantic import BaseModel
from ..profile.profile import UserBase
from typing import List, Optional
from datetime import datetime
from ..profile.profileShortlist import UserVerificationBase
from ..profile.profile import UserBase, ProfileCard


class ProfileVisitsBase(BaseModel):
    id: int
    user_visited_profile: UserBase
    profile_visitors: UserBase
    visit_count: int
    is_viewed: bool

    class Config:
        orm_mode = True


class ProfileVisitsDetailsBase(BaseModel):

    partner_id: int
    name: str
    profile_created_by: Optional[str]
    is_user_verified: UserVerificationBase
    image: Optional[str]
    details: Optional[ProfileCard]
    visited_count: Optional[int]
    created_on: datetime
    updated_on: Optional[datetime]
    match_score_total: Optional[int]
    match_score_matching_nos: Optional[int]
    match_score: Optional[float]
    is_shortlisted: bool
    is_interest_send: bool

    class Config:
        orm_mode = True


class ProfileVisitCreate(BaseModel):
    user_id: int

# ***pagination - getProfileViewedByUserPagination***
class ProfileVisitsDetailsBasePagination(BaseModel):
    users : List[ProfileVisitsDetailsBase]
    total: int
    count: int
    pagination: dict

    class Config:
        orm_mode = True