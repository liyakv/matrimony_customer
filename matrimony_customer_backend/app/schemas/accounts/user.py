from pydantic import BaseModel
from typing import List, Optional
from sqlalchemy.sql.sqltypes import DateTime
from datetime import datetime
from fastapi import File, UploadFile


class AuthDetails(BaseModel):
    mobile: str
    otp: str
    
    class Config:
        orm_mode = True


class UserResendOTP(BaseModel):
    phone_number: str
    

    class Config:
        orm_mode = True
        
      

class TknExprySchema(BaseModel):
    acces_token: str
    

    class Config:
        orm_mode = True  
