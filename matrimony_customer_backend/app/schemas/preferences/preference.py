from ast import DictComp
from typing import Dict, List, Literal, Optional
from pydantic import BaseModel,Json
from ..profile.profile import UserBase,ProfileCard
from datetime import datetime

from app.models.model import Education, Star
from ..profile.profile import CasteBase, EducationBase, OccupationBase, ReligionBase, StarBase, UserBase



class PartnerPreferencesBasicDetails(BaseModel):
    # id: Optional[int]
    gender: Optional[str]
    age_upperLimit: Optional[int]
    age_lowerLimit: Optional[int]
    age_strict: Optional[bool]
    height_upperLimit: Optional[int]
    height_lowerLimit: Optional[int]
    height_strict: Optional[bool]
    physical_status: Optional[Dict]
    physical_status_strict: Optional[bool]
    marital_status: Optional[Dict]
    marital_status_strict: Optional[bool]
    having_child: Optional[Dict]
    having_child_strict: Optional[bool]
    eating_habit: Optional[Dict]
    eating_habit_strict: Optional[bool]
    smoking_habit: Optional[Dict]
    smoking_habit_strict: Optional[bool]
    drinking_habit: Optional[Dict]
    drinking_habit_strict: Optional[bool]
    mother_tounge: Optional[Dict]
    mother_tounge_strict: Optional[bool]


class ProffesionalPreferencesDetails(BaseModel):
    # id: Optional[int]
    education: Optional[Dict]
    education_strict: Optional[bool]
    employed_in: Optional[Dict]
    employed_in_strict: Optional[bool]
    occupation: Optional[Dict]
    occupation_strict: Optional[bool]
    annual_income: Optional[str]
    annual_income_strict: Optional[bool]
    income_currency: Optional[str]


class LocationPartnerPreferencesDetails(BaseModel):
    # id: Optional[int]
    country: Optional[Dict]
    country_strict: Optional[bool]
    residing_city: Optional[Dict]
    residing_city_strict: Optional[bool]
    residing_state: Optional[Dict]
    residing_state_strict: Optional[bool]
    citizenship: Optional[Dict]
    citizenship_strict: Optional[bool]


class ReligiousPartnerPreferencesDetails(BaseModel):
    # id: Optional[int]
    # religion: Optional[Dict]
    # religion_strict: bool
    # caste: Optional[Dict]
    # caste_strict: bool
    # sub_caste: Optional[Dict]
    # sub_caste_strict: bool
    level_one: Optional[str]
    level_one_strict: Optional[bool]
    level_two: Optional[Dict]
    level_two_strict: Optional[bool]
    level_three: Optional[Dict]
    level_three_strict: Optional[bool]
    star: Optional[Dict]
    star_strict: Optional[bool]
    dosham: Optional[str]
    dosham_strict: Optional[bool]


class CreatePartnerPreferencesMainBase(BaseModel):
    basic_partner_preferenes: Optional[PartnerPreferencesBasicDetails]
    proffesional_preferences: Optional[ProffesionalPreferencesDetails]
    location_preferences: Optional[LocationPartnerPreferencesDetails]
    religion_preferences: Optional[ReligiousPartnerPreferencesDetails]




# //
class EducationsBase(BaseModel):
    value: Optional[int]
    label: Optional[str]

    class Config:
        orm_mode = True


class OccupationsBase(BaseModel):
    value: Optional[int]
    label: Optional[str]

    class Config:
        orm_mode = True
        

class ReligionsBase(BaseModel):
    religion_id: Optional[int]
    value: Optional[str]
    label: Optional[str]

    class Config:
        orm_mode = True


class CastesBase(BaseModel):
    religion_id: Optional[int]
    value: Optional[str]
    label: Optional[str]

    class Config:
        orm_mode = True


class SubCasteBase(BaseModel):
    religion_id: Optional[int]
    value: Optional[str]
    label: Optional[str]

    class Config:
        orm_mode = True



class StarsBase(BaseModel):
    value: Optional[int]
    label: Optional[str]

    class Config:
        orm_mode = True


class PreferenceDatas(BaseModel):
# //basic_preferneces//
    physical_status: Optional[List]
    marital_status: Optional[List]
    having_child: Optional[List]
    eating_habit: Optional[List]
    smoking_habit: Optional[List]
    drinking_habit: Optional[List]
    mother_tounge: Optional[List]
# //proffessional_preferences///
    education: Optional[List[EducationsBase]]
    employed_in: Optional[List]
    occupation: Optional[List[OccupationsBase]]
    annual_income: Optional[List]
    income_currency: Optional[List]
# //religion_preferences//
    religion: Optional[List[ReligionsBase]]
    caste: Optional[List[CastesBase]]
    sub_caste: Optional[List[SubCasteBase]]
    star: Optional[List[StarsBase]]
    dosham: Optional[List]




# *****
class ProffesionalPartnerPreferencesBase(BaseModel):
    id: int
    proffesional_preferences_table: UserBase
    education: Optional[Json]
    education_strict: bool
    employed_in: Optional[Json]
    employed_in_strict: bool
    occupation: Optional[Json]
    occupation_strict: bool
    annual_income: Optional[str]
    annual_income_strict: bool
    income_currency: Optional[str]

    class Config:
        orm_mode = True


class LocationPartnerPreferencesBase(BaseModel):
    id: int
    location_preferences_table: UserBase
    country: Optional[Json]
    country_strict: bool
    residing_city: Optional[Json]
    residing_city_strict: bool
    residing_state: Optional[Json]
    residing_state_strict: bool
    citizenship: Optional[Json]
    citizenship_strict: bool

    class Config:
        orm_mode = True


class ReligiousPartnerPreferencesBase(BaseModel):
    id: int
    religious_preferences_table: UserBase
    religion: Optional[Json]
    religion_strict: bool
    caste: Optional[Json]
    caste_strict: bool
    sub_caste: Optional[Json]
    sub_caste_strict: bool
    divisions: Optional[Json]
    divisions_strict: bool
    star: Optional[Json]
    star_strict: bool
    dosham: Optional[Json]
    dosham_strict: bool

    class Config:
        orm_mode = True

class ProfesionalPreferenceProfile(BaseModel):
    id: int
    name: str
    profile_created_for: Optional[Literal['Self', 'Daughter','Son', 'Siblings', 'Relative', 'Friend',None]]
    # profile_created_for:Optional[str]
    is_user_verified: Optional[bool]
    image: Optional[str]
    details: Optional[ProfileCard]
    created_on: datetime
    match_score_total: Optional[int]
    match_score_matching_nos: Optional[int]
    match_score: Optional[float]


class PartnerPreferencesCreateBase(BaseModel):
    gender: Optional[str]
    age_upperLimit: Optional[int]
    age_lowerLimit: Optional[int]
    age_strict: bool
    height_upperLimit: Optional[int]
    height_lowerLimit: Optional[int]
    height_strict: bool
    physical_status: Optional[str]
    physical_status_strict: bool
    marital_status: Optional[str]
    marital_status_strict: bool
    having_child: Optional[str]
    having_child_strict: bool
    eating_habit: Optional[Dict]
    eating_habit_strict: bool
    smoking_habit: Optional[str]
    smoking_habit_strict: bool
    drinking_habit: Optional[str]
    drinking_habit_strict: bool
    mother_tounge: Optional[Dict]
    mother_tounge_strict: bool

    education: Optional[Dict]
    education_strict: bool
    employed_in: Optional[Dict]
    employed_in_strict: bool
    occupation: Optional[Dict]
    occupation_strict: bool
    annual_income: Optional[str]
    annual_income_strict: bool
    income_currency: Optional[str]
    

    class Config:
        orm_mode = True


# ** pagination - getProfessionalPreferenceMatchPagination**
class ProfesionalPreferenceProfilePagination(BaseModel):
    users : List[ProfesionalPreferenceProfile]
    total: int
    count: int
    pagination: dict

    class Config:
        orm_mode = True

