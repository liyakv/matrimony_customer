from typing import Literal, Optional
from pydantic import BaseModel

class AdvertisementBase(BaseModel):
    id: int
    ad_name: Optional[str]
    image: Optional[str]
    url: Optional[str]

    class Config:
        orm_mode = True


class AdvertisementPositionBase(BaseModel):
    position: str
