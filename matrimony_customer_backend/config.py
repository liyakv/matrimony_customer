from pydantic import BaseSettings
from forex_python.converter import CurrencyRates
import math
from datetime import date


class Settings(BaseSettings):
    db_name: str
    db_username: str
    db_password: str
    db_host: str
    db_port: str
    db_url: str

    class Config:
        env_file = ".env"


default_avatar_male = "https://dummyimage.com/200x200/000/fff&text=male"
default_avatar_female = "https://dummyimage.com/200x200/000/fff&text=female"
default_avatar_others = "https://dummyimage.com/200x200/000/fff&text=others"
default_avatar_user = "https://dummyimage.com/200x200/000/fff&text=user"
new_calculation_days = 30


def cm_to_feet(height_in_cm: int):
    feet = int(height_in_cm / 30.48)
    remain = height_in_cm - (feet * 30.48)
    inch = int(round(remain) / 2.54)
    return f'{feet}\'{inch}\"'


def convert_currency(from_cur, to_cur, amount):
    cr = CurrencyRates()
    try:
        return round(cr.convert(from_cur, to_cur, amount))
    except:
        return None


def calculate_age(birthDate):
    today = date.today()
    age = today.year - birthDate.year - ((today.month, today.day) < (birthDate.month, birthDate.day))
    return age

def calculate_age_month(birthDate):
    today = date.today()
    years= ((today-birthDate).total_seconds()/ (365.242*24*3600))
    yearsInt=int(years)
    months=(years-yearsInt)*12
    monthsInt=int(months)
    # agedesc = str(yearsInt) + " years and " + str(monthsInt) + " months"
    return yearsInt, monthsInt


MATCH_SCORE_PERCENT = 40


INCOME_CHOICE_INPUT = [
    ('Less than 20,000','Less than 20,000',0,20000),
    ('20,000 to 40,000','20,000 to 40,000',20000,40000),
    ('40,000 to 65,000','40,000 to 65,000',40000,65000),
    ('65,000 to 1,00,000','65,000 to 1,00,000',65000,100000),
    ('1,00,000 to 2,00,000','1,00,000 to 2,00,000',100000,200000),
    ('2,00,000 to 5,00,000','2,00,000 to 5,00,000',200000,500000),
    ('5,00,000 to 10,00,000','5,00,000 to 10,00,000',500000,1000000),
    ('10,00,000 to 20,00,000','10,00,000 to 20,00,000',1000000,2000000),
    ('20,00,000 to 50,00,000','20,00,000 to 50,00,000',2000000,5000000),
    ('50,00,000 to 1,00,00,000','50,00,000 to 1,00,00,000',5000000,10000000),
    ('More than 1,00,00,000','More than 1,00,00,000',10000000,20000000),
]

RAZORPAY_KEY_ID = "rzp_test_ikPuQeBC6j7uqv"
RAZORPAY_KEY_SECRET = "XuBSNyFEmP6sgd1Im17HagQI"
IMAGE_ANALYSIS_URL = "http://128.199.139.248:6090"


# http://128.199.139.248:6090/