from pickle import EMPTY_LIST
from fastapi import FastAPI, Depends, HTTPException, Response, Request, File, UploadFile, Form, status
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session
import uvicorn
from typing import Literal
import razorpay
from app.crud.filter.filter import profile_by_age_limit, profile_by_height_limit, profile_by_marital_status, profile_by_mother_tongue, profile_by_religion_level_one, profile_by_religion_level_three, profile_by_religion_level_two, profile_by_star

from app.database.main import SessionLocal
from app.schemas.accounts.user import *
from app.schemas.profile.profile import *
from app.schemas.package.package import *
from app.schemas.successStories.successStories import *
from app.schemas.advertisement.advertisement import *
from app.schemas.profile.requests import *
from app.schemas.profile.partnerInterests import *
from app.schemas.profile.profileVisits import *
from app.schemas.profile.profileShortlist import *
from app.schemas.quiz.quiz import *
from app.schemas.preferences.preference import *
from app.schemas.profile.profile_manage import *
from app.schemas.matches.matches import *
from app.schemas.filter.profile_filter import *

from app.crud.profile.profile_manage import update_user_basic_info, update_user_family_details, update_user_lifestyle, update_user_location, update_user_main, update_user_professional_details, update_user_profile_questions, update_user_religion_info, update_user_sibling_details, update_user_socialmedia,Profile_Deactivation, get_partner_image
from app.crud.accounts.user import *
from app.crud.profile.profile import *
from app.crud.packages.package import *
from app.crud.successStories.success_stories import *
from app.crud.advertisement.advertisement import *
from app.crud.profile.photo_request import *
from app.crud.profile.number_request import *
from app.crud.profile.partnerInterests import *
from app.crud.profile.shortlist import *
from app.crud.documents.documents import *
from app.crud.quiz.quiz import *
from app.crud.profile.user_images import *
from app.crud.preferences.preferences import *
from app.crud.home.home import *
from app.crud.matches.matches import *
from app.crud.preferences.matches import *
from app.crud.filter.filter import *

from pydantic import BaseModel
import random
import math
from typing import Dict
from datetime import datetime

import cloudinary
import cloudinary.uploader
import cloudinary.api
from cloudinary.utils import cloudinary_url

from config import INCOME_CHOICE_INPUT, calculate_age


app = FastAPI()

class Settings(BaseModel):
    authjwt_secret_key: str = "secret"

cloudinary.config(
        cloud_name = "beacon11222",
        api_key = "538336974251932",
        api_secret = "BhLnUpBJ5sqz9FLnV2LtCEVoIXo" ,
    )


@AuthJWT.load_config
def get_config():
    return Settings()


@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.message}
    )


origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    response = Response("Internal server error", status_code=500)
    try:
        request.state.db = SessionLocal()
        response = await call_next(request)
    finally:
        request.state.db.close()
    return response


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def generateOTP():
    digits = "0123456789"
    OTP = ""
    print("OTP Generation")
    for i in range(4):
        OTP += digits[math.floor(random.random() * 10)]
    return "1234"


@app.post("/resendOTP", status_code=200)
async def resend_otp(customer: UserResendOTP, db: Session = Depends(get_db)):
    otp = generateOTP()
    customer = resend_otp_crud(db, customer, otp)
    if customer:
        return customer
    else:
        raise HTTPException(status_code=404, detail={"user_exist":False})


@app.get("/getProfileCompleteness")
async def get_match_api(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    percentage = get_profile_percentage(db,user.id)
    return {"profile_complete_percentage": percentage}


@app.post("/verifyotp")
async def login(auth_details: AuthDetails, Authorize:AuthJWT=Depends(), db: Session = Depends(get_db)):
    user_authenticated, user = verify_otp(db, credential=auth_details)
    if user_authenticated:
        access_token=Authorize.create_access_token(auth_details.mobile, expires_time=85000)
        refresh_token=Authorize.create_refresh_token(auth_details.mobile)
        user_image = None
        percentage = get_profile_percentage(db,user.id)
        registered_user = None
        if len(user.user_subscribed) == 0:
            subscribe_free_package_crud(db, user.id)
        if (user.user_basic_details) and (user.user_religion_details) and (user.user_location_details) \
                    and (user.user_professional_details) and (user.user_family_details):
            registered_user = True
        else:
            registered_user = False

        if len(user.user_table) > 0:
            image = get_user_default_image(db,user.id)
            if image:
                user_image = image.image
            else:
                if user.gender == None:
                    user_image = default_avatar_user
                elif user.gender == "Male" or user.gender =="male":
                    user_image = default_avatar_male
                else:
                    user_image = default_avatar_female
        else:
            if user.gender == None:
                user_image = default_avatar_user
            elif user.gender == "Male" or user.gender =="male":
                user_image = default_avatar_male
            else:
                user_image = default_avatar_female

        resdata = {'access_token': access_token, 'refresh_token': refresh_token, 'name': user.name,
                   'username': user.username, 'phone_number': user.phone_number, "user_image": user_image, "profile_complete_percentage": percentage,
                   "registered_user": registered_user}

        raise HTTPException(status_code=200, detail=resdata)
    else:
        raise HTTPException(status_code=401, detail='Invalid otp')


@app.get('/new_token')
def create_new_token(Authorize:AuthJWT=Depends()):
    try:
        Authorize.jwt_refresh_token_required()
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,detail="Invalid token")
    current_user=Authorize.get_jwt_subject()
    access_token=Authorize.create_access_token(subject=current_user)
    return {"new_access_token":access_token}


@app.get('/check_access_token_expiry')
async def check_token_expiry(Authorize:AuthJWT=Depends()):
    try:
        Authorize.jwt_required()
    except:
        raise HTTPException(status_code=status.HTTP_200_OK, detail=True)
    raise HTTPException(status_code=status.HTTP_200_OK,
                    detail=False)


@app.post("/userRegistrationOtp", status_code=200)
async def create_initial_user(user: UserInitialRegister, db: Session = Depends(get_db)):
    user = create_initial_user_crud(db, user)
    return user


@app.get("/getAllPackages", status_code=200)
async def get_all_packages(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    user = None
    user_packages = []
    if Authorize.get_jwt_subject():
        Authorize.jwt_required()
        auth = Authorize.get_jwt_subject()
        user = get_user_details(db,auth)
        user_packages = get_subscribed_packages(db, user.id)
    package_list = get_all_packages_crud(db)
    resp = []
    for package in package_list:
        pkg = {}
        pkg["id"] = package["id"]
        pkg["name"] = package["package_name"]
        pkg["duration"] = package["months"]
        pkg["amount"] = package["amount"]
        pkg["is_premium"] = package["is_premium"]
        pkg["isSubscribed"] = False
        pkg["expiry"] = package["expiry_date"]
        for pack in user_packages:
            if pack["package_id"] == package["id"]:
                pkg["isSubscribed"] = True
                pkg["expiry"] = pack["expiry_date"]
                break

        pkg["details"] =[]
        pkg["details"].append({"name":"View Profiles", "status":package["view_profiles"]})
        pkg["details"].append({"name":"Send Interest", "status":package["send_interest"]})
        pkg["details"].append({"name":"Recommended Profiles", "status":package["recomd_profiles"]})
        pkg["details"].append({"name":"Advanced Privacy Option", "status":package["advced_privacy_optn"]})
        pkg["details"].append({"name":"Send Unlimited Personal Messages", "status":package["send_ultd_prnal_msg"]})
        pkg["details"].append({"name":"Priority in Search Result", "status":package["prty_in_srch_rslt"]})
        pkg["details"].append({"name":"View Contacts and Address", "status":package["vw_cnts_adrs"]})
        pkg["details"].append({"name":"Profile Tagged as Paid Member", "status":package["prfl_tgd_as_pd_membr"]})
        pkg["details"].append({"name":"SMS Alert", "status":package["sms_alert"]})
        pkg["details"].append({"name":"Notifications", "status":package["notifications"]})
        pkg["details"].append({"name":"View Social Media Profiles of Members", "status":package["vw_socl_mda_prfl_urs"]})
        pkg["details"].append({"name":"Personal Assistance", "status":package["persanal_astnce"]})
        pkg["details"].append({"name":"Create Quizzes", "status":package["create_quize"]})
        pkg["details"].append({"name":"Attend Quizzes", "status":package["attend_quize"]})
        pkg["details"].append({"name":"View all photos", "status":package["vw_al_phts"]})
        pkg["details"].append({"name":"Request for Photos", "status":package["rqst_fr_phts"]})
        resp.append(pkg)
    return resp


@app.get("/getSuccessStories",response_model=List[SuccessStoriesBase], status_code=200)
async def get_success_stories(db: Session = Depends(get_db)):
    return get_all_success_stories(db)


@app.post("/getHorizontalAdvertisements",response_model=List[AdvertisementBase], status_code=200)
async def get_advertisements(ad_position:AdvertisementPositionBase, db: Session = Depends(get_db)):
    return get_horizontal_advertisement(db, ad_position.position)


@app.get("/getVerticalAdvertisements",response_model=AdvertisementBase, status_code=200)
async def get_advertisements(db: Session = Depends(get_db)):
    return get_vertical_advertisement(db)


@app.post("/sendPhotoRequest")
async def send_photo_request(photo_request:PhotoRequestsCreate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    if len(check_photo_request_already_send(db,photo_request,user.id))>0:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                        detail="already send")
    send_photo_requests(db,photo_request,user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")


@app.get("/getReceivedPhotoRequests", status_code=200)
async def get_received_photo_requests(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    reqts = get_photo_requests(db, user.id)
    resp = []
    for profile in reqts:
        partner = get_user_by_id(db,profile.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        profile_created_by = profile.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.user_id,user.id))>0:
            is_interest_send = True
        image = get_partner_image(db, user, partner)
        question_add=question_add_status(db, partner)
        pr ={
            "request_id": profile.request_id,
            "partner_id": profile.user_id,
            "name": profile.name,
            "gender": profile.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": profile.is_number_verified,
                "is_photos_verified": profile.is_photos_verified,
                "is_documents_verified": profile.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": profile.age_year,
                "age_month": profile.age_month,
                "height_in_ft": profile.height_in_ft,
                "mother_tongue": profile.mother_tongue,
                "city": profile.city,
                "district": profile.district,
                "state": profile.state,
                "level_one": profile.level_one,
                "level_two": profile.level_two,
                "star_name": profile.star_name,
                "raasi": profile.raasi,
                "qualification": profile.qualification,
                "employed_in": profile.employed_in,
                "occupation_name": profile.occupation_name,
                "annual_income": profile.annual_income,
                "currency_type": profile.currency_type,
            },
            "status": profile.status,
            "created_on": profile.created_on,
            "updated_on": profile.updated_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)
    return resp


@app.post("/sendNumberRequest")
async def send_number_request(number_request:NumberRequestsCreate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    if len(check_number_request_already_send(db,number_request,user.id))>0:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                        detail="already send")
    send_number_requests(db,number_request,user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")


@app.get("/getReceivedNumberRequests", status_code=200)
async def get_received_number_requests(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    reqts = get_number_requests(db, user.id)
    resp = []
    for profile in reqts:
        partner = get_user_by_id(db,profile.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        profile_created_by = profile.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.user_id,user.id))>0:
            is_interest_send = True
        image = get_partner_image(db, user, partner)
        question_add=question_add_status(db, partner)
        pr ={
            "request_id": profile.request_id,
            "partner_id": profile.user_id,
            "name": profile.name,
            "gender": profile.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": profile.is_number_verified,
                "is_photos_verified": profile.is_photos_verified,
                "is_documents_verified": profile.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": profile.age_year,
                "age_month": profile.age_month,
                "height_in_ft": profile.height_in_ft,
                "mother_tongue": profile.mother_tongue,
                "city": profile.city,
                "district": profile.district,
                "state": profile.state,
                "level_one": profile.level_one,
                "level_two": profile.level_two,
                "star_name": profile.star_name,
                "raasi": profile.raasi,
                "qualification": profile.qualification,
                "employed_in": profile.employed_in,
                "occupation_name": profile.occupation_name,
                "annual_income": profile.annual_income,
                "currency_type": profile.currency_type,
            },
            "status": profile.status,
            "created_on": profile.created_on,
            "updated_on": profile.updated_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)
    return resp



# @app.post("/sendRemainder")
# async def send_remainder(send_reminder:SendReminder,db: Session = Depends(get_db)): #, Authorize:AuthJWT=Depends()
#     # Authorize.jwt_required()
#     # auth = Authorize.get_jwt_subject()
#     # user = get_user_details(db,auth)
#     db.query(PartnerInterestReminder).filter(PartnerInterestReminder.is_deleted==False, PartnerInterestReminder.from_user_id==from_user_id, PartnerInterestReminder.to_user_id==number_request.user_id).all()
#     # if len(check_number_request_already_send(db,number_request,user.id))>0:
#     #     raise HTTPException(status_code=status.HTTP_409_CONFLICT,
#     #                     detail="already send")
#     send_number_requests(db,number_request,user.id)
#     raise HTTPException(status_code=status.HTTP_201_CREATED,
#                         detail="success")


@app.put("/updatePhotoRequestStatus")
async def update_photo_request_status(photo_request:PhotoRequestsUpdate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    update_photo_requests(db,photo_request)
    raise HTTPException(status_code=status.HTTP_200_OK,
                        detail="success")


@app.put("/updateNumberRequestStatus")
async def update_number_request_status(number_request:NumberRequestsUpdate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    update_number_requests(db,number_request)
    raise HTTPException(status_code=status.HTTP_200_OK,
                        detail="success")


@app.post("/sendPartnerInterest")
async def send_partner_interest_request(interest_request:PartnerInterestCreate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    partner_interest = check_partner_interest_recently_send(db,interest_request.user_id,user.id)
    if partner_interest:
        delete_partner_interest(db, partner_interest)
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail="removed from interest list")
    send_partner_interest(db,interest_request,user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")


@app.put("/updatePartnerInterestStatus")
async def update_partner_interest_request_status(partner_request_schema:PartnerInterestUpdate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    partner_request = get_partner_interest_by_id(db,partner_request_schema.request_id)
    if len(check_partner_interest_already_send(db, user.id, partner_request.from_user_id)) == 0:
        raise HTTPException(status_code=status.HTTP_200_OK,
                        detail="partner request not found")
    update_partner_interest(db,partner_request_schema,partner_request)
    raise HTTPException(status_code=status.HTTP_200_OK,
                        detail="success")


@app.post("/profileShortList")
async def short_list_profile(short_list:ShortListCreate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    shortlist = check_recently_shortlisted(db,short_list.user_id,user.id)
    if shortlist:
        delete_shortlist(db, shortlist)
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail="removed from shortlist")
    profile_short_list(db,short_list,user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")


@app.get("/getShortlistedByPartners",response_model=List[ProfileShortlistBase], status_code=200)
async def get_shortlisted_by_partners(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    profiles = get_short_listed_by_partners(db, user.id)
    resp = []
    for profile in profiles:
        partner = get_user_by_id(db,profile.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        profile_created_by = profile.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.user_id,user.id))>0:
            is_interest_send = True
        image = get_partner_image(db, user, partner)
        question_add=question_add_status(db, partner)
        pr ={
            "shortlist_id": profile.shortlist_id,
            "partner_id": profile.user_id,
            "name": profile.name,
            "gender": profile.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": profile.is_number_verified,
                "is_photos_verified": profile.is_photos_verified,
                "is_documents_verified": profile.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": profile.age_year,
                "age_month": profile.age_month,
                "height_in_ft": profile.height_in_ft,
                "mother_tongue": profile.mother_tongue,
                "city": profile.city,
                "district": profile.district,
                "state": profile.state,
                "level_one": profile.level_one,
                "level_two": profile.level_two,
                "star_name": profile.star_name,
                "raasi": profile.raasi,
                "qualification": profile.qualification,
                "employed_in": profile.employed_in,
                "occupation_name": profile.occupation_name,
                "annual_income": profile.annual_income,
                "currency_type": profile.currency_type,
            },
            "is_viewed": profile.is_viewed,
            "created_on": profile.created_on,
            "updated_on": profile.updated_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)
    return resp


@app.put("/updateShortlistStatus")
async def update_shortlist_status(shortlist_schema:ShortListUpdate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    shortlist = get_shortlist_by_id(db,shortlist_schema.shortlist_id)
    if shortlist:
        update_shortlist_views_status(db,shortlist)
        raise HTTPException(status_code=status.HTTP_200_OK,
                        detail="success")
    else:
        raise HTTPException(status_code=status.HTTP_200_OK,
                        detail="not found")


@app.get("/getShortlistedByUser",response_model=List[ProfileShortlistBase], status_code=200)
async def get_shortlisted_by_user(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    profiles = get_short_listed_by_user(db, user.id)
    resp = []
    for profile in profiles:
        partner = get_user_by_id(db,profile.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        profile_created_by = profile.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.user_id,user.id))>0:
            is_interest_send = True
        image = get_partner_image(db, user, partner)
        question_add=question_add_status(db, partner)
        pr ={
            "shortlist_id": profile.shortlist_id,
            "partner_id": profile.user_id,
            "name": profile.name,
            "gender": profile.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": profile.is_number_verified,
                "is_photos_verified": profile.is_photos_verified,
                "is_documents_verified": profile.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": profile.age_year,
                "age_month": profile.age_month,
                "height_in_ft": profile.height_in_ft,
                "mother_tongue": profile.mother_tongue,
                "city": profile.city,
                "district": profile.district,
                "state": profile.state,
                "level_one": profile.level_one,
                "level_two": profile.level_two,
                "star_name": profile.star_name,
                "raasi": profile.raasi,
                "qualification": profile.qualification,
                "employed_in": profile.employed_in,
                "occupation_name": profile.occupation_name,
                "annual_income": profile.annual_income,
                "currency_type": profile.currency_type,
            },
            "is_viewed": profile.is_viewed,
            "created_on": profile.created_on,
            "updated_on": profile.updated_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)
    return resp


@app.post("/createQuizQuestion")
async def create_quiz_question(quiz_schema:QuizQuestionCreate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    add_quiz_question(db,quiz_schema,user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")


@app.post("/addPartnerAnswers")
async def add_partner_answers(quiz_answer_schema_list:List[QuizQuestionAnswer], db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    add_quiz_answers(db,quiz_answer_schema_list,user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")


@app.get("/getInterestSentByUser", response_model=List[InterestSentDetailsBase], status_code=200)
async def get_interest_sent_by_user(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    interest_sent = get_interest_sent_details_by_user(db, user.id)
    interests = []
    for partner_detail in interest_sent:
        partner = get_user_by_id(db, partner_detail.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        image = get_partner_image(db, user, partner)
        profile_created_by = partner_detail.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,partner_detail.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,partner_detail.user_id,user.id))>0:
            is_interest_send = True
        question_add=question_add_status(db, partner)
        pr ={
            "interest_id": partner_detail.interest_id,
            "partner_id": partner_detail.user_id,
            "name": partner_detail.name,
            "gender": partner_detail.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": partner_detail.is_number_verified,
                "is_photos_verified": partner_detail.is_photos_verified,
                "is_documents_verified": partner_detail.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": partner_detail.age_year,
                "age_month": partner_detail.age_month,
                "height_in_ft": partner_detail.height_in_ft,
                "mother_tongue": partner_detail.mother_tongue,
                "city": partner_detail.city,
                "district": partner_detail.district,
                "state": partner_detail.state,
                "level_one": partner_detail.level_one,
                "level_two": partner_detail.level_two,
                "star_name": partner_detail.star_name,
                "raasi": partner_detail.raasi,
                "qualification": partner_detail.qualification,
                "employed_in": partner_detail.employed_in,
                "occupation_name": partner_detail.occupation_name,
                "annual_income": partner_detail.annual_income,
                "currency_type": partner_detail.currency_type,
            },
            "is_viewed": partner_detail.is_viewed,
            "status": partner_detail.status,
            "updated_on": partner_detail.updated_on,
            "created_on": partner_detail.created_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        interests.append(pr)
    return interests

@app.get("/getInterestReceivedList", response_model=List[InterestReceivedDetailsBase], status_code=200)
async def get_interest_received_by_user(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    interest_received = get_interest_received_details_by_user(db, user.id)

    interests = []
    for partner_detail in interest_received:
        partner = get_user_by_id(db, partner_detail.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        image = get_partner_image(db, user, partner)
        profile_created_by = partner_detail.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,partner_detail.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,partner_detail.user_id,user.id))>0:
            is_interest_send = True
        question_add=question_add_status(db, partner)

        useraddanswer=False
        useranswerattend = db.query(UserQuizQuestions).join(UserQuizAnswers).filter(UserQuizQuestions.user_id==user.id,UserQuizAnswers.user_id==partner.id,UserQuizQuestions.is_deleted==False,UserQuizAnswers.is_deleted==False).all()
        print(useranswerattend,"kkkkkkkkkkkkkkkkkkk")
        if len(useranswerattend)>0:
            useraddanswer=True

        pr ={
            "interest_id": partner_detail.interest_id,
            "partner_id": partner_detail.user_id,
            "name": partner_detail.name,
            "gender": partner_detail.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_answer_attended":useraddanswer,
            "is_user_verified": {
                "is_number_verified": partner_detail.is_number_verified,
                "is_photos_verified": partner_detail.is_photos_verified,
                "is_documents_verified": partner_detail.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": partner_detail.age_year,
                "age_month": partner_detail.age_month,
                "height_in_ft": partner_detail.height_in_ft,
                "mother_tongue": partner_detail.mother_tongue,
                "city": partner_detail.city,
                "district": partner_detail.district,
                "state": partner_detail.state,
                "level_one": partner_detail.level_one,
                "level_two": partner_detail.level_two,
                "star_name": partner_detail.star_name,
                "raasi": partner_detail.raasi,
                "qualification": partner_detail.qualification,
                "employed_in": partner_detail.employed_in,
                "occupation_name": partner_detail.occupation_name,
                "annual_income": partner_detail.annual_income,
                "currency_type": partner_detail.currency_type,
            },
            "is_viewed": partner_detail.is_viewed,
            "status": partner_detail.status,
            "updated_on": partner_detail.updated_on,
            "created_on": partner_detail.created_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        print("--------------------",pr)
        interests.append(pr)
    return interests


@app.get("/getUserQuestions",response_model=List[UserQuizQuestionsBase], status_code=200)
async def get_user_questions(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    return get_quiz(db, user.id)

@app.post("/getUserAnswers", status_code=200)
async def get_user_answers(quiz_partner:QuizAnswerPartnerID, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    return get_partner_added_answers(db, user.id, quiz_partner.partner_id)


@app.post("/getPartnerQuestions",response_model=List[UserQuizQuestionsBase], status_code=200)
async def get_partner_questions(quiz_partner:QuizAnswerPartnerID, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    return get_quiz(db, quiz_partner.partner_id)


@app.post("/getUserAnswers",response_model=List[UserQuizAnswersBase], status_code=200)
async def get_partner_answers(partner_obj:QuizAnswerPartnerID, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    return get_partner_added_answers(db, user.id, partner_obj.partner_id)


# @app.get("/getUserAnswers",response_model=List[UserQuizAnswersBase], status_code=200)
# async def get_user_answers(partner_obj:QuizAnswerPartnerID, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
#     Authorize.jwt_required()
#     auth = Authorize.get_jwt_subject()
#     user = get_user_details(db,auth)
#     return get_users_added_answers(db, user.id, partner_obj.partner_id)


@app.get("/getAnswerByID",response_model=List[UserQuizAnswersBase], status_code=200)
async def get_answer_by_id(answer_id:QuizAnswerID, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    return get_quiz_answer_by_id(db, user.id, answer_id)


@app.get("/getProfileViewedByUserHome",status_code=200)
def getProfileViewedByUserHome(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    profile_viewed = get_profile_viewed_details_by_user_home(db, user.id)
    resp = []
    for partner_detail in profile_viewed:
        partner = get_user_by_id(db, partner_detail.user_id)
        image = get_partner_image(db, user, partner)
        pr = {
            "id" : partner_detail.user_id,
            "name": partner_detail.name,
            "location" : partner_detail.city,
            "age": calculate_age(partner_detail.dob),
            "height" : partner_detail.height_in_ft,
            "image" : image
        }
        resp.append(pr)
    return {
        "count": len(profile_viewed),
        "users": resp
    }


@app.get("/getProfileViewedByUser", response_model=List[ProfileVisitsDetailsBase], status_code=200)
async def get_profile_viewed_by_user(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    profile_viewed = get_profile_viewed_details_by_user(db, user.id)
    interests = []
    for partner_detail in profile_viewed:
        partner = get_user_by_id(db, partner_detail.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        image = get_partner_image(db, user, partner)
        profile_created_by = partner_detail.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,partner_detail.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,partner_detail.user_id,user.id))>0:
            is_interest_send = True
        question_add=question_add_status(db, partner)
        pr ={
            "partner_id": partner_detail.user_id,
            "name": partner_detail.name,
            "gender": partner_detail.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": partner_detail.is_number_verified,
                "is_photos_verified": partner_detail.is_photos_verified,
                "is_documents_verified": partner_detail.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": partner_detail.age_year,
                "age_month": partner_detail.age_month,
                "height_in_ft": partner_detail.height_in_ft,
                "mother_tongue": partner_detail.mother_tongue,
                "city": partner_detail.city,
                "district": partner_detail.district,
                "state": partner_detail.state,
                "level_one": partner_detail.level_one,
                "level_two": partner_detail.level_two,
                "star_name": partner_detail.star_name,
                "raasi": partner_detail.raasi,
                "qualification": partner_detail.qualification,
                "employed_in": partner_detail.employed_in,
                "occupation_name": partner_detail.occupation_name,
                "annual_income": partner_detail.annual_income,
                "currency_type": partner_detail.currency_type,
            },
            "visited_count": partner_detail.visit_count,
            "updated_on": partner_detail.updated_on,
            "created_on": partner_detail.created_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        interests.append(pr)
    return interests


@app.get("/getAllDocumentTypeDatas", response_model=DocTypeDatas, status_code=200)
async def get_doctype_datas(db: Session = Depends(get_db)):
    doc_type_datas = {}
    document_type_datas = Documents.ID_PROOFS
    doc_type_datas["doc_types"] = []
    for phy in document_type_datas:
        doc_type_datas["doc_types"].append({"value":phy[0], "label":phy[0]})
    return doc_type_datas


@app.post("/documentsUpload")
async def user_documents_upload(docmnt_type: str = Form(...), doc_file: UploadFile = File(...), db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    upload_result = cloudinary.uploader.upload(doc_file.file, folder='/user_documents')
    public_id = upload_result.get('public_id')
    image_url = upload_result['secure_url']
    # if len(check_docs_already_send(db, user.id, docmnt_type))>0:
    #     # cloudinary.uploader.destroy(public_id, folder='/user_documents')
    #     update_docs_upload(db, user.id, image_url, docmnt_type, public_id)
    #     raise HTTPException(status_code=status.HTTP_200_OK,
    #                     detail="already uploaded the same document")
    if len(check_docs_approved_or_pending(db, user.id))>0:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="document already uploaded, please wait for the verification process")
    add_user_documents(db, user.id, image_url, docmnt_type, public_id)
    user.is_documents_verified =True
    db.commit()
    percentage = get_profile_percentage(db,user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                      detail={"profile_percentage": percentage, "status":"success"})
                        


@app.get("/getUploadedDocuments",response_model=DocumentsBaseAll, status_code=200)
async def get_uploaded_documents_details(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    photo_status = user.is_photos_verified
    details = get_user_documents(db, user.id)
    return {"details":details, "photo_verified_status":photo_status}


@app.get("/getAdminQuizQuestions",response_model=List[AdminQuizQuestionsBase], status_code=200)
async def get_admin_questions(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    return get_admin_created_questions(db)


@app.put("/updateUserEmail")
async def update_email(email_updt: EmailUpdate, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    update_user_email(db, email_updt, user.id)
    raise HTTPException(status_code = status.HTTP_200_OK,
                         detail="success")

# , response_model=PreferenceDatas
@app.get("/getProfileDatas", status_code=200)
async def get_profile_datas(db: Session = Depends(get_db)):
    profile_datas = {}
    #user_religion
    star_datas = get_all_star_datas(db)
    profile_datas['star'] = star_datas
    level_one = get_all_religion_datas(db)
    profile_datas['level_one'] = level_one
    level_two = get_all_caste_datas(db)
    profile_datas['level_two'] = level_two
    level_three = get_all_subcaste_datas(db)
    profile_datas['level_three'] = level_three
    #user_basic
    mother_tongue_datas = UserBasicDetails.MOTHER_TONGUE
    profile_datas["mother_tounge"] = [{"value":item[0], "label":item[0]} for item in mother_tongue_datas]
    body_type_datas = UserBasicDetails.BODY_TYPE
    profile_datas["body_type"] = [{"value":item[0], "label":item[0]} for item in body_type_datas]
    physical_status_datas = UserBasicDetails.PHYSICAL_STATUS
    profile_datas["physical_status"] = [{"value":item[0], "label":item[0]} for item in physical_status_datas]
    skin_color_datas = UserBasicDetails.SKIN_COLOR
    profile_datas["skin_color"] = [{"value":item[0], "label":item[0]} for item in skin_color_datas]
    marital_status_datas = UserBasicDetails.MARITAL_STATUS
    profile_datas["marital_status"] = [{"value":item[0], "label":item[0]} for item in marital_status_datas]
    get_married_in_datas = UserBasicDetails.GET_MARRIED_IN
    profile_datas["get_married_in"] = [{"value":item[0], "label":item[0]} for item in get_married_in_datas]
    #life_style
    smoking_datas = UserLifeStyle.SMOKING
    profile_datas["smoking_habits"] = [{"value":item[0], "label":item[0]} for item in smoking_datas]
    drinking_datas = UserLifeStyle.DRINKING
    profile_datas["drinking_habits"] = [{"value":item[0], "label":item[0]} for item in drinking_datas]
    fooding_datas = UserLifeStyle.EATING
    profile_datas["food_habits"] = [{"value":item[0], "label":item[0]} for item in fooding_datas]
    #social_media
    social_media_datas = UserSocialMediaProfiles.SOCIALMEDIA_PROFILES
    profile_datas["social_media"] = [{"value":item[0], "label":item[0]} for item in social_media_datas]
    #family_details
    family_type_datas = UserFamilyDetails.FAMILY_TYPE
    profile_datas["family_type"] = [{"value":item[0], "label":item[0]} for item in family_type_datas]
    family_values_datas = UserFamilyDetails.FAMILY_VALUES
    profile_datas["family_values"] = [{"value":item[0], "label":item[0]} for item in family_values_datas]
    family_status_datas = UserFamilyDetails.FAMILY_STATUS
    profile_datas["family_status"] = [{"value":item[0], "label":item[0]} for item in family_status_datas]
    living_status_datas = UserFamilyDetails.LIVING_STATUS
    profile_datas["living_status"] = [{"value":item[0], "label":item[0]} for item in living_status_datas]
    #sibling_details
    sibling_type_datas = UserSiblingsDetails.SIBLINGS_TYPE
    profile_datas["sibling_type"] = [{"value":item[0], "label":item[0]} for item in sibling_type_datas]
    sibling_relation_datas = UserSiblingsDetails.SIBLINGS_RELATION
    profile_datas["sibling_relation"] = [{"value":item[0], "label":item[0]} for item in sibling_relation_datas]
    sibling_marital_status_datas = UserSiblingsDetails.SIBLINGS_MARITAL_STATUS
    profile_datas["sibling_marital_status"] = [{"value":item[0], "label":item[0]} for item in sibling_marital_status_datas]
    #professional details
    education_datas = get_all_education_datas(db)
    profile_datas['education'] = education_datas
    employed_in_datas = UserProfessionalDetails.EMPLOYED_IN
    profile_datas["employed_in"] = [{"value":item[0], "label":item[0]} for item in employed_in_datas]
    occupation_datas = get_all_occupation_datas(db)
    profile_datas['occupation'] = occupation_datas
    currency_datas = UserProfessionalDetails.CURRENCY_TYPE
    profile_datas["currency_type"] = [{"value":item[0], "label":item[0]} for item in currency_datas]
    annual_income_datas = UserProfessionalDetails.INCOME_CHOICE
    profile_datas["annual_income"] = [{"value":item[0], "label":item[0]} for item in annual_income_datas]
    residential_status_datas = UserProfessionalDetails.RESIDENTIAL_STATUS
    profile_datas["residential_status"] = [{"value":item[0], "label":item[0]} for item in residential_status_datas]
    return profile_datas


@app.get("/getAreaOfSpecialisationByEducationID/{id}", status_code=200)
async def get_area_of_specialisation_by_educationID(id:int, db: Session = Depends(get_db)):
    return get_area_of_specialization_by_education(db, id)


@app.put("/userProfileUpdate")
async def user_profile_update(user_profile: UserProfileUpdate, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)

    if user_profile.user_main is not None:
        update_user_main(db, user_profile.user_main, user.id)
    if user_profile.user_religion is not None:
        update_user_religion_info(db, user_profile.user_religion, user.id)
    if user_profile.user_basic is not None:
        update_user_basic_info(db, user_profile.user_basic, user.id)
    if user_profile.user_lifestyle is not None:
        update_user_lifestyle(db, user_profile.user_lifestyle, user.id)
    update_user_socialmedia(db,user_profile.user_socialmedia, user.id)
    if user_profile.user_family is not None:
        update_user_family_details(db, user_profile.user_family, user.id)
    if user_profile.user_siblings is not None:
        update_user_sibling_details(db, user_profile.user_siblings, user.id)
    if user_profile.user_location is not None:
        update_user_location(db,user_profile.user_location, user.id)
    update_user_profile_questions(db, user_profile.user_profile_question, user.id)
    if user_profile.user_profession is not None:
        update_user_professional_details(db, user_profile.user_profession, user.id)

    percentage = get_profile_percentage(db,user.id)
    raise HTTPException(status_code=status.HTTP_200_OK,
                        detail={"profile_percentage": percentage, "status":"success"})


@app.post("/createPartnerPreference")
async def create_partner_preference(preference_schema: CreatePartnerPreferencesMainBase, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    if preference_schema.basic_partner_preferenes is not None:
        add_user_basic_preferenes(db, preference_schema.basic_partner_preferenes, user.id)
    if preference_schema.proffesional_preferences is not None:
        add_user_proffesion_preferenes(db, preference_schema.proffesional_preferences, user.id)
    if preference_schema.location_preferences is not None:
        add_user_location_preferenes(db, preference_schema.location_preferences, user.id)
    if preference_schema.religion_preferences is not None:
        add_user_religion_preferenes(db, preference_schema.religion_preferences, user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")



@app.get("/getPreferenceEditData", status_code = status.HTTP_200_OK)
async def getPreferenceEditData(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):#
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    pref_edit_data = get_pref_edit_data(db,user.id)#
    return pref_edit_data



@app.get("/getUserImages", response_model = Dict[str, List[UserImagesGet]], response_model_exclude_unset=True, status_code = 200)
async def get_user_image(db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    print(user.id,"kkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
    img_dict = {}
    user_images = get_user_images(db, user.id)  # crud function
    img_dict['UserPhotos'] = user_images
    user_home_images = get_user_home_images(db, user.id)
    img_dict['HomePhotos'] = user_home_images
    user_vehichle_images = get_user_vehichle_images(db, user.id)
    img_dict["VehiclePhotos"] = user_vehichle_images
    return img_dict


@app.post("/addImages",status_code=status.HTTP_201_CREATED)
async def add_user_images(request: Request, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    imageBase64 = await request.json()
    import requests
    import json
    # resp = requests.post(config.IMAGE_ANALYSIS_URL + '/profile_images',data=json.dumps(imageBase64), headers={"Content-Type":"application/json"})
    # resp_data = resp.json()
    for image in imageBase64['UserPhotos']:
        # wtr_markd = image["watermarked_image"]
        # if "data:image/jpeg;base64," not in wtr_markd:
        #     wtr_markd = "data:image/jpeg;base64,"+ wtr_markd
        # thb_markd = image["thumbnail_image"]
        # if "data:image/jpeg;base64," not in thb_markd:
        #     thb_markd = "data:image/jpeg;base64,"+ thb_markd
        # blr_markd = image["blurred_image"]
        # if "data:image/jpeg;base64," not in blr_markd:
        #     blr_markd = "data:image/jpeg;base64,"+ blr_markd
        # if "data:image/" not in image:
        #     image = "data:image/jpeg;base64,"+ image
        watermarked_upload_result = cloudinary.uploader.upload(image, folder='/user_images')
        public_id = watermarked_upload_result.get('public_id')
        watermarked_image_url = watermarked_upload_result['secure_url']
        thumbnail_upload_result = cloudinary.uploader.upload(image, folder='/user_images')
        thumbnail_image_url = thumbnail_upload_result['secure_url']
        blurred_upload_result = cloudinary.uploader.upload(image, folder='/user_images')
        blurred_image_url = blurred_upload_result['secure_url']
        add_user_image(db, user.id, watermarked_image_url, public_id, thumbnail_image_url, blurred_image_url)
    for image in imageBase64["HomePhotos"]:
        img = image
        if "data:image/" not in img:
            img = "data:image/jpeg;base64,"+ img
        upload_result_H = cloudinary.uploader.upload(img, folder='/home_images')
        public_id_H = upload_result_H.get('public_id')
        image_url_H = upload_result_H['secure_url']
        add_user_house_image(db, user.id, image_url_H, public_id_H)
    for image in imageBase64["VehiclePhotos"]:
        img = image
        # if "data:image/" not in img:
        #     img = "data:image/jpeg;base64,"+ img
        upload_result_V = cloudinary.uploader.upload(img, folder='/vehicle_images')
        public_id_V = upload_result_V.get('public_id')
        image_url_V = upload_result_V['secure_url']
        add_user_vehicle_image(db, user.id, image_url_V, public_id_V)
    user.is_photos_verified = True
    db.commit()
    db.refresh(user)
    raise HTTPException(status_code=status.HTTP_201_CREATED, detail="success")


@app.post("/removeImages",status_code = status.HTTP_202_ACCEPTED)
async def remove_user_images(request: Request, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    try:
        image_id = await request.json()
        if image_id['type'] == 'user':
            remove_user_image(db, user.id, image_id['id'])
        elif image_id['type'] == 'home':
            remove_user_home_image(db, user.id, image_id['id'])
        elif image_id['type'] == 'vehicle':
            remove_user_vehicle_image(db, user.id, image_id['id'])
        else:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="error")
        return {"message": "deleted"}
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="error")


@app.post("/setProfile",status_code = status.HTTP_202_ACCEPTED)
async def set_profile(request: Request, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    try:
        image_id = await request.json()
        set_user_profile(db, user.id, image_id)
        return {"message": "success"}
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="error")


@app.get("/discoverMatches", status_code = status.HTTP_200_OK)
async def discover_matches(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):#
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    # user=db.query(User).filter(User.id == 6).first()
    match_list = []
    print(user.id,"....................")
    city_match = get_city_match(db, user.id)
    match_list.append(city_match)
    profesion_match = get_profession_match(db,user.id)
    match_list.append(profesion_match)
    education_match = get_education_match(db,user.id)
    match_list.append(education_match)
    return match_list

@app.get("/Matches_in_your_city", status_code = status.HTTP_200_OK)
async def matches_in_your_city(db: Session = Depends(get_db),Authorize:AuthJWT=Depends()):#
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    city_match = get_city_match_detailed(db, user)
    return city_match


@app.get("/getPartnerDetailsbyID/{id}", status_code = status.HTTP_200_OK)
async def partner_details(id:int,db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    partner_details = get_partner_details_byid(db, id, user.id)
    return partner_details

@app.get("/get_level_two/{typelevel}", status_code = status.HTTP_200_OK)
async def level_two_caste(typelevel:str,db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    level_two_details = get_level_two_bytype(db, typelevel)
    level_two_details.insert(0,{"level_two":"Any"})
    return level_two_details

@app.post("/get_level_three")
async def level_three_subcaste(subcaste_list:Subcastelist, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    level_three_details = get_level_three_byname(db, subcaste_list)
    level_three_details.insert(0,{"level_three":"Any"})
    return level_three_details


# ***user religion for user-registration***
@app.get("/get_level_two_user_reg/{typelevel}", status_code = status.HTTP_200_OK)
async def level_two_caste(typelevel:str,db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    level_two_details = get_level_two_bytype(db, typelevel)
    # level_two_details.insert(0,{"level_two":"Any"})
    return level_two_details

@app.post("/get_level_three_user_reg")
async def level_three_subcaste(subcaste_list:Subcastelist, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    level_three_details = get_level_three_byname(db, subcaste_list)
    # level_three_details.insert(0,{"level_three":"Any"})
    return level_three_details


@app.get("/getAllPreferenceDatas", status_code=200) #, response_model=PreferenceDatas
async def get_preferenes_datas(db: Session = Depends(get_db)):
    pref_datas = {}
# //basic_preferneces//
    physical_status_datas = BasicPartnerPreferences.DISABILITY_CHOICE
    pref_datas["physical_status"] = []
    for phy in physical_status_datas:
        pref_datas["physical_status"].append({"value":phy[0], "label":phy[0]})
    marital_status_datas = BasicPartnerPreferences.MARITAL_STATUS
    pref_datas["marital_status"] = []
    for phy in marital_status_datas:
        pref_datas["marital_status"].append({"value":phy[0], "label":phy[0]})
    child_choice_datas = BasicPartnerPreferences.CHILD_CHOICE
    pref_datas["having_child"] = []
    for phy in child_choice_datas:
        pref_datas["having_child"].append({"value":phy[0], "label":phy[0]})
    eating_habit_datas = BasicPartnerPreferences.EATING
    pref_datas["eating_habit"] = []
    for phy in eating_habit_datas:
        pref_datas["eating_habit"].append({"value":phy[0], "label":phy[0]})
    smoking_habit_datas = BasicPartnerPreferences.SMOKING_CHOICE
    pref_datas["smoking_habit"] = []
    for phy in smoking_habit_datas:
        pref_datas["smoking_habit"].append({"value":phy[0], "label":phy[0]})
    drinking_habit_datas = BasicPartnerPreferences.DRINKING_CHOICE
    pref_datas["drinking_habit"] = []
    for phy in drinking_habit_datas:
        pref_datas["drinking_habit"].append({"value":phy[0], "label":phy[0]})
    mother_tongue_datas = BasicPartnerPreferences.MOTHER_TONGUE
    pref_datas["mother_tounge"] = []
    for phy in mother_tongue_datas:
        pref_datas["mother_tounge"].append({"value":phy[0], "label":phy[0]})
# //proffessional_preferences///
    education_datas = get_all_education_datas(db)
    education_datas.insert(0,{"label":"Any","value":"Any"})
    pref_datas['education'] = education_datas
    employed_in_datas = ProffesionalPartnerPreferences.EMPLOYED_IN
    pref_datas["employed_in"] = []
    for phy in employed_in_datas:
        pref_datas["employed_in"].append({"value":phy[0], "label":phy[0]})
    occupation_datas = get_all_occupation_datas(db)
    occupation_datas.insert(0,{"label":"Any","value":"Any"})
    pref_datas['occupation'] = occupation_datas
    annual_income_datas = ProffesionalPartnerPreferences.PREFERENCE_INCOME_CHOICE
    pref_datas["annual_income"] = []
    for phy in annual_income_datas:
        pref_datas["annual_income"].append({"value":phy[0], "label":phy[0]})
    income_currency_datas = UserProfessionalDetails.CURRENCY_TYPE
    pref_datas["income_currency"] = []
    for phy in income_currency_datas:
        pref_datas["income_currency"].append({"value":phy[0], "label":phy[0]})
# //religion_preferences//
    religion_datas = get_all_religion_datas(db)
    religion_datas.insert(0,{"label":"Any","value":"Any"})
    pref_datas['religion'] = religion_datas
    caste_datas = get_all_caste_datas(db)
    caste_datas.insert(0,{"label":"Any","value":"Any"})
    pref_datas['caste'] = caste_datas
    sub_caste_datas = get_all_subcaste_datas(db)
    sub_caste_datas.insert(0,{"label":"Any","value":"Any"})
    pref_datas['sub_caste'] = sub_caste_datas
    star_datas = get_all_star_datas(db)
    star_datas.insert(0,{"label":"Any","value":"Any"})
    pref_datas['star'] = star_datas
    dosham_datas = ReligiousPartnerPreferences.DOSHAM_PREF
    pref_datas["dosham"] = []
    for phy in dosham_datas:
        pref_datas["dosham"].append({"value":phy[0], "label":phy[0]})

    return pref_datas


@app.post("/visitProfile")
async def visit_profile(visit_profile:ProfileVisitCreate, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    if len(check_already_visited(db,visit_profile.user_id,user.id))>0:
        update_visit_count(db, visit_profile.user_id,user.id)
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                        detail="profile visited count updated !")
    profile_visit_crud(db,visit_profile,user.id)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")


@app.post("/userRegistration")
async def create_user_registration(user_registration: UserRegistrationMainBase, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    if user_registration is not None:
        user_registration_crud(db, user_registration, user.id)
        percentage = get_profile_percentage(db,user.id)
        raise HTTPException(status_code=status.HTTP_200_OK,
                            detail={"profile_percentage": percentage, "status":"success"})



@app.get("/getUserRegistrationDatas", response_model=UserRegDatas, status_code=200)
async def get_user_reg_datas(db: Session = Depends(get_db)):
    user_reg_datas = {}
    religion_datas = get_all_religion_datas_user_reg(db)
    user_reg_datas['religion'] = religion_datas
    caste_datas = get_all_caste_datas_user_reg(db)
    user_reg_datas['caste'] = caste_datas
    sub_caste_datas = get_all_subcaste_datas(db)
    user_reg_datas['sub_caste'] = sub_caste_datas
    mother_tongue_datas = UserBasicDetails.MOTHER_TONGUE
    user_reg_datas["mother_tounge"] = []
    for phy in mother_tongue_datas:
        user_reg_datas["mother_tounge"].append({"value":phy[0], "label":phy[0]})
    marital_status_datas = UserBasicDetails.MARITAL_STATUS
    user_reg_datas["marital_status"] = []
    for phy in marital_status_datas:
        user_reg_datas["marital_status"].append({"value":phy[0], "label":phy[0]})
    physical_status_datas = UserBasicDetails.PHYSICAL_STATUS
    user_reg_datas["physical_status"] = []
    for phy in physical_status_datas:
        user_reg_datas["physical_status"].append({"value":phy[0], "label":phy[0]})
    education_datas = get_all_education_datas_user_reg(db)
    user_reg_datas['highest_education'] = education_datas
    employed_in_datas = UserProfessionalDetails.EMPLOYED_IN
    user_reg_datas["employed_in"] = []
    for phy in employed_in_datas:
        user_reg_datas["employed_in"].append({"value":phy[0], "label":phy[0]})
    occupation_datas = get_all_occupation_datas_user_reg(db)
    user_reg_datas['occupation'] = occupation_datas
    income_currency_datas = UserProfessionalDetails.CURRENCY_TYPE
    user_reg_datas["currency_type"] = []
    for phy in income_currency_datas:
        user_reg_datas["currency_type"].append({"value":phy[0], "label":phy[0]})
    annual_income_datas = UserProfessionalDetails.INCOME_CHOICE
    user_reg_datas["annual_income"] = []
    for phy in annual_income_datas:
        user_reg_datas["annual_income"].append({"value":phy[0], "label":phy[0]})
    family_status_datas = UserFamilyDetails.FAMILY_STATUS
    user_reg_datas["family_status"] = []
    for phy in family_status_datas:
        user_reg_datas["family_status"].append({"value":phy[0], "label":phy[0]})
    family_type_datas = UserFamilyDetails.FAMILY_TYPE
    user_reg_datas["family_type"] = []
    for phy in family_type_datas:
        user_reg_datas["family_type"].append({"value":phy[0], "label":phy[0]})
    family_values_datas = UserFamilyDetails.FAMILY_VALUES
    user_reg_datas["family_values"] = []
    for phy in family_values_datas:
        user_reg_datas["family_values"].append({"value":phy[0], "label":phy[0]})

    return user_reg_datas


@app.post("/getMatchScore")
async def get_match_api(partner: PartnerIDMatch, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    partnerid = get_user_by_id(db, partner.to_id)
    if partner.type:
        prefence_details,total_preferences,score = get_match_score(db,partnerid,user)
    else:
        prefence_details,total_preferences,score = get_match_score(db,user,partnerid)
    score_perge = 0
    if total_preferences > 0 :
        score_perge = round((score*100)/total_preferences)
    partner_image = get_partner_image(db, user, partnerid)
    user_image = None
    images = get_user_default_image(db, user.id)
    if images == None:
        if (user.gender == "Female"):
            user_image= default_avatar_female
        elif (user.gender == "Male"):
            user_image= default_avatar_male
        else:
            user_image= default_avatar_others
    else:
        user_image= images.image
    return {"user_image": user_image, "partner_image": partner_image, "user_gender": user.gender, "partner_gender": partnerid.gender, "score": score_perge, "details": prefence_details}


@app.get("/getProfessionalPreferenceMatch",response_model=List[ProfesionalPreferenceProfile],status_code=200)
async def get_professional_preference_match(db: Session = Depends(get_db),Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    final_users = get_professional_preference_match_users(db, user.id)
    resp = []
    for profile in final_users:

        year,month=calculate_age_month(profile.dob)
        if profile.user_basic_details == None:
            profile_created_for=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue
        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city,
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one
            caste_name = profile.user_religion_details.religion.level_two
            star_name= profile.user_religion_details.star.star_name
            raasi=profile.user_religion_details.raasi
        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification
            employed_in=profile.user_professional_details.employed_in
            occupation_name= profile.user_professional_details.occupation.occupation_name
            annual_income= profile.user_professional_details.annual_income
            currency_type=profile.user_professional_details.currency_type

        gndr=[i for i in get_gender_preference(db,user.id)]
        if len(profile.user_table)==0:
            if gndr =='Male':
                image=default_avatar_male
            elif gndr =='Female':
                image=default_avatar_female
            else:
                image=default_avatar_others
        else:
            img=[i for i in profile.user_table]
            for value in img:
                if value.is_main==True:image=value.image
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_for": profile_created_for,
            "image": image,
            "details": {
                "age_year": year,
                "age_month": month,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type,
            },
            "created_on": profile.date_joined,
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":user.is_documents_verified},
            "match_score": 85,
            "match_score_total": 45,
            "match_score_matching_nos": 25
        }
        resp.append(pr)
    return resp


@app.get("/getMyProfessionMatch",status_code=200)
async def get_my_profession_match(db: Session = Depends(get_db),Authorize: AuthJWT=Depends()): #
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    # user=db.query(User).filter(User.id == 6).first()
    login_user_id = user.id
    final_users = get_my_profession_match_users(db, user.id)
    resp = []
    for profile in final_users:
        year,month=calculate_age_month(profile.dob)
        if profile.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for != None else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by = "Parent"
            else:
                profile_created_by = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue != None else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city,
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion != None else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion != None else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star != None else None
            raasi=profile.user_religion_details.raasi
        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education != None else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in != None else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation != None else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income != None else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type != None else None

        match_dict, total,score = get_match_score(db,user,profile)
        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        image = get_partner_image(db, user, profile)
        is_shortlisted = False
        if len(check_already_shortlisted(db,user.id,login_user_id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,user.id,login_user_id))>0:
            is_interest_send = True
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by,
            "image": image,
            "details": {
                "age_year": year,
                "age_month": month,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type,
            },
            "created_on": profile.date_joined,
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":user.is_documents_verified},
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)
    return resp

@app.get("/getEducationMatch",status_code=200)
def getEducationMatch(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()): #
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    match_list = get_education_match_list(db, user.id)
    resp = []
    for profile in match_list:
        year,month=calculate_age_month(profile.dob)
        if profile.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by = "Parent"
            else:
                profile_created_by = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star else None
            raasi=profile.user_religion_details.raasi

        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None

        image = get_partner_image(db, user, profile)


        if profile.user_subscribed==[]:
            premium = False
        else:
            premium_ids = get_premium_package_ids(db)
            for i in profile.user_subscribed:
                if i.package_log_id in premium_ids:
                    premium = True
                    break
                else:
                    premium = False

        match_dict, total,score = get_match_score(db,user,profile)
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.id,user.id))>0:
            is_interest_send = True
        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by,
            "image": image,
            "details": {
                "age_year": year,
                "age_month": month,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type
            },
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":user.is_documents_verified},
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)
    return resp


@app.get("/getProfileById",status_code=200)
def get_profile_by_id(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):#, Authorize: AuthJWT=Depends()
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    resp = get_profile_by_id_details(db, user.id)
    return resp

@app.get("/getFilterComponents",status_code=200)
def getFilterComponents(db:Session=Depends(get_db),Authorize: AuthJWT=Depends()):#
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    resp = get_filter_components(db, user.id)
    return resp

# //**\\
@app.get("/getMatchTest", status_code=200)
async def get_match_by_user_test(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    # matches = get_total_matches_crud(db, user.id, user)
    # total_matches = get_gender_matched_partners(db, user.id)
    matches = get_matches_test(db, user.id)
    resp = []
    for profile in matches:
        match_dict, total, score = get_match_score(db,user,profile)
        percent = round((score*100)/total)
        # if percent >= config.MATCH_SCORE_PERCENT:
            
        if profile.dob:
            age=calculate_age(profile.dob)
        dob=str(profile.dob).split('-')
        
        if profile.user_location_details == None:
            city=None
        else:
            city=profile.user_location_details.city
            
        if profile.user_professional_details==None:
            employed_in=None
        else:
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in != None else None

        if profile.user_basic_details == None:
            height_in_ft=None
        else:
            height_in_ft=profile.user_basic_details.height_in_ft
        
        image = get_partner_image(db, user, profile)
        
        pr ={
            "id": profile.id,
            "name": profile.name,
            "location" : city,
            "age": age,
            "height" : height_in_ft,
            "image": image,
            "details": {
                "employed_in": employed_in,
                "age_year": dob[0],
                "age_month": dob[1],
            },
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score

        }
        resp.append(pr)
        if (len(resp)==8):
            break
    return {
        "count": len(matches),
        "users": resp
    }

    # resp = []
    # for profile in matches:
        # match_dict, total, score = get_match_score(db,user,profile)
        # percent = round((score*100)/total)
        # if percent >= config.MATCH_SCORE_PERCENT:
            # if profile.dob:
            #     age=calculate_age(profile.dob)
            # dob=str(profile.dob).split('-')

        # if profile.user_location_details == None:
        #     city=None
        #     district=None

        # else:
        #     city=profile.user_location_details.city
        #     district=profile.user_location_details.district

            # if profile.user_basic_details == None:
            #     height_in_ft=None
            # else:
            #     height_in_ft=profile.user_basic_details.height_in_ft
            # if profile.user_location_details == None:
            #     city=None
            # else:
            #     city=profile.user_location_details.city

            # if profile.user_basic_details == None:
            #     height_in_ft=None
            # else:
            #     height_in_ft=profile.user_basic_details.height_in_ft
            # image = get_partner_image(db, user, profile)
            # pr ={
            #     "id": profile.id,
            #     "name": profile.name,
                # "location" : city,
                # "district": district,
                # "age": age,
                # "height" : height_in_ft,
                # "image": image,
                # "details": {
                #     "age_year": dob[0],
                #     "age_month": dob[1],
                #     "city": city,
                # },
                # "created_on": profile.date_joined,
                # "match_score": percent,
                # "match_score_total": total,
                # "match_score_matching_nos": score
            # }
    #         resp.append(pr)
    #         if (len(resp)==8):
    #             break
    # return {
    #     "count": len(matches),
    #     "users": resp
    # }

# \\**//


@app.get("/getTotalMatch", status_code=200)
async def get_total_matches_by_user(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    # matches = get_total_matches_crud(db, user.id, user)
    gender_matches = get_gender_matched_partners(db, user)
    resp = []
    for profile in gender_matches:
        match_dict, total, score = get_match_score(db,user,profile)
        percent = round((score*100)/total)
        if percent >= config.MATCH_SCORE_PERCENT:
            year= 0
            month =0
            if profile.dob:
                year,month=calculate_age_month(profile.dob)
            agedesc = str(year) + " years and " + str(month) + " months"
            # dob=str(profile.dob).split('-')

            if profile.user_basic_details == None:
                height_in_ft=None
            else:
                height_in_ft=profile.user_basic_details.height_in_ft
            if profile.user_location_details == None:
                city=None
            else:
                city=profile.user_location_details.city

            if profile.user_basic_details == None:
                height_in_ft=None
            else:
                height_in_ft=profile.user_basic_details.height_in_ft

            image = get_partner_image(db, user, profile)

            pr ={
                "id": profile.id,
                "name": profile.name,
                "location" : city,
                "age": year,
                "height" : height_in_ft,
                "image": image,
                "details": {
                    "age_year": year,
                    "age_month": month,
                    "city": city,
                },
                "created_on": profile.date_joined,
                "match_score": percent,
                "match_score_total": total,
                "match_score_matching_nos": score
            }
            resp.append(pr)
            if (len(resp)==8):
                break
    return {
        "count": len(gender_matches),
        "users": resp
    }



@app.get("/getTotalMatchAll",status_code=200)
async def getNewMatchAll(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    gender_matches = get_gender_matched_partners(db, user)
    resp = []
    for profile in gender_matches:
        year,month=calculate_age_month(profile.dob)
        if profile.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by = "Parent"
            else:
                profile_created_by = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star else None
            raasi=profile.user_religion_details.raasi

        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None

        image = get_partner_image(db, user, profile)
        match_dict, total,score = get_match_score(db,user,profile)

        is_shortlisted = False
        # print(len(check_already_shortlisted(db,profile.id,user.id)),"kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
        if len(check_already_shortlisted(db,profile.id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.id,user.id))>0:
            is_interest_send = True

        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        # question_add=question_add_status(db, partner)
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by,
            "image": image,
            # "question_add":question_add,
            "details": {
                "age_year": year,
                "age_month": month,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type
            },
            "is_user_verified": {"is_number_verified":profile.is_number_verified,"is_photos_verified":profile.is_photos_verified,"is_documents_verified":profile.is_documents_verified},
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)
    return resp


@app.get("/getNewMatch",status_code=200)
def getNewMatch(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    match_list = get_new_match_list(db, user.id)
    resp = []
    for profile in match_list:
        age = None
        if profile.dob:
            year,month=calculate_age_month(profile.dob)
            age = year
        if profile.user_basic_details == None:
            height_in_ft=None
        else:
            height_in_ft=profile.user_basic_details.height_in_ft

        if profile.user_location_details == None:
            city=None
        else:
            city=profile.user_location_details.city



        image = get_partner_image(db, user, profile)
        pr = {
            "id" : profile.id,
            "name": profile.name,
            "location" : city,
            "age": age,
            "height" : height_in_ft,
            "image" : image
        }
        resp.append(pr)
        if (len(resp)==8):
            break
    return {
        "count": len(match_list),
        "users": resp
    }


@app.get("/getNewMatchAll",status_code=200)
async def getNewMatchAll(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    match_list = get_new_match_list(db, user.id)
    resp = []
    for profile in match_list:
        year,month=calculate_age_month(profile.dob)
        if profile.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by = "Parent"
            else:
                profile_created_by = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star else None
            raasi=profile.user_religion_details.raasi

        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None

        image = get_partner_image(db, user, profile)
        match_dict, total,score = get_match_score(db,user,profile)

        is_shortlisted = False
        # print(len(check_already_shortlisted(db,profile.id,user.id)),"kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
        if len(check_already_shortlisted(db,profile.id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.id,user.id))>0:
            is_interest_send = True

        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        # question_add=question_add_status(db, partner)
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by,
            "image": image,
            # "question_add":question_add,
            "details": {
                "age_year": year,
                "age_month": month,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type
            },
            "is_user_verified": {"is_number_verified":profile.is_number_verified,"is_photos_verified":profile.is_photos_verified,"is_documents_verified":profile.is_documents_verified},
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)
    return resp

# @app.get("/getMatchScore")
# async def get_match_api(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
#     Authorize.jwt_required()
#     auth = Authorize.get_jwt_subject()
#     user = get_user_details(db, auth)
#     gender_matches = get_gender_matched_partners(db, user)
#     if len(gender_matches)>0:
#         for gender_match in gender_matches:
#             get_match_score(db,user,gender_match)
#     # get_user_star_matches(db,user)
#     return "ok"


@app.get("/dailyRecommendations",status_code=200)
async def getDailyRecommendations(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()): #
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    # daily_users = get_daily_recommendations(db, user.id,"first")
    daily_users=get_new_users(db,user.id)
    if len(daily_users) ==0:
        daily_users=''
        daily_users=get_new_users(db,user.id)
    resp = []
    priority_package_logs = db.query(PackageLog).filter(PackageLog.prty_in_srch_rslt == True).all()
    priority_package_id = []
    for i in range(0,len(priority_package_logs)):
        j = priority_package_logs[i].id
        priority_package_id.append(j)

    for profile in daily_users:
        image=get_partner_image(db, user, profile)
        age = None
        if profile.dob:
            age=calculate_age(profile.dob)
        if profile.user_basic_details == None:
            height_in_ft=None
        else:
            height_in_ft=profile.user_basic_details.height_in_ft

        if profile.user_location_details == None:
            city=None
        else:
            city=profile.user_location_details.city

        if profile.user_subscribed == None:
            premium = False
        else:
            try:
                premium = profile.user_subscribed[0].package_log.is_premium
            except IndexError:
                premium = False
        pr = {
            "id" : profile.id,
            "name": profile.name,
            "premium":premium,
            "location" : city,
            "age": age,
            "height" : height_in_ft,
            "image" : image
        }
        resp.append(pr)
        if (len(resp)==8):
            break
    day = int(datetime.now().strftime("%d"))
    if 4 <= day <= 20 or 24 <= day <= 30:
        suffix = "th"
    else:
        suffix = ["st", "nd", "rd"][day % 10 - 1]
    return {
        "date": [str(day)+suffix, datetime.now().strftime("%b")],
        "users": resp
    }


@app.get("/dailyRecommendationsAll",status_code=200)
async def getDailyRecommendationsAll(db: Session = Depends(get_db),Authorize: AuthJWT=Depends()): #
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    daily_users = get_new_users(db,user.id)
    # if len(daily_users)<8:
    #     daily_users = get_daily_recommendations(db, user.id,"first")
    # if len(daily_users)<4:
    #     get_new_users(db,user.id)
    # else:
    #     daily_users = get_daily_recommendations(db, user.id,"first")
        # return []
    resp = []

    # priority_package_logs = db.query(PackageLog).filter(PackageLog.prty_in_srch_rslt == True).all()
    # priority_package_id = []
    # for i in range(0,len(priority_package_logs)):
    #     j = priority_package_logs[i].id
    #     priority_package_id.append(j)

    for profile in daily_users :
        year, month = calculate_age_month(profile.dob)
        # dob=str(profile.dob).split('-')
        if profile.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by = "Parent"
            else:
                profile_created_by = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star else None
            raasi=profile.user_religion_details.raasi

        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None
        image = get_partner_image(db, user, profile)
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.id,user.id))>0:
            is_interest_send = True
        match_dict, total,score = get_match_score(db,user,profile)
        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by,
            "image": image,
            "details": {
                "age_year": year,
                "age_month": month,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type
            },
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":user.is_documents_verified},
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score,
            "is_interest_send": is_interest_send,
            "is_shortlisted": is_shortlisted
        }
        resp.append(pr)
    return resp

def get_premium_package_ids(db: Session):
    return [r.id for r in db.query(PackageLog).filter(PackageLog.is_premium == True).all()]

@app.post("/profileFilter",status_code=200)
async def profile_filter(filter_params: FilterParameters, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    gender_list = gender_pref_list2(db,user.id, filter_params.search_key)
    active_list = active_users(db,user.id)
    gndr_intrsct_active = list(set(gender_list) & set(active_list))
    # profile type filter
    if filter_params.show_profiles_created:
        show_profile_create_list = []
        if (filter_params.show_profiles_created=="with_in_a_day"):
            show_profile_create_list.extend(within_a_day(db))
        elif (filter_params.show_profiles_created=="with_in_a_week"):
            show_profile_create_list.extend(within_a_week(db))
        elif (filter_params.show_profiles_created=="with_in_a_month"):
            show_profile_create_list.extend(within_a_month(db))
        gndr_intrsct_active = list(set(show_profile_create_list) & set(gndr_intrsct_active))
    if filter_params.profiles:
        profiles_list = []
        if ("with_photo" in filter_params.profiles):
            profiles_list.extend(with_photo(db))
        gndr_intrsct_active = list(set(profiles_list) & set(gndr_intrsct_active))
    if filter_params.profiles_created_by:
        profile_created_by_list = profile_created_by(db,filter_params.profiles_created_by)
        if ("Parent" in filter_params.profiles_created_by):
            profile_created_by_list.extend(profile_by_parent(db))
        gndr_intrsct_active = list(set(profile_created_by_list) & set(gndr_intrsct_active))
    # basic profile filter
    if filter_params.age_lower_limit is not None:
        age_filter = profile_by_age_limit(db, filter_params.age_lower_limit, filter_params.age_upper_limit)
        gndr_intrsct_active = list(set(age_filter) & set(gndr_intrsct_active))
    if filter_params.height_lower_limit is not None:
        height_filter = profile_by_height_limit(db, filter_params.height_lower_limit, filter_params.height_upper_limit)
        gndr_intrsct_active = list(set(height_filter) & set(gndr_intrsct_active))
    if len(filter_params.mother_tongue) > 0:
        mother_tongue_filter = profile_by_mother_tongue(db, filter_params.mother_tongue)
        gndr_intrsct_active = list(set(mother_tongue_filter) & set(gndr_intrsct_active))
    if len(filter_params.marital_status) > 0:
        marital_status_filter = profile_by_marital_status(db, filter_params.marital_status)
        gndr_intrsct_active = list(set(marital_status_filter) & set(gndr_intrsct_active))
    #religion filter
    if filter_params.religion is not None:
        level_one_filter = profile_by_religion_level_one(db, filter_params.religion)
        gndr_intrsct_active = list(set(level_one_filter) & set(gndr_intrsct_active))
    if len(filter_params.caste) > 0:
        level_two_filter = profile_by_religion_level_two(db, filter_params.caste)
        gndr_intrsct_active = list(set(level_two_filter) & set(gndr_intrsct_active))
    if len(filter_params.sub_caste) > 0:
        level_three_filter = profile_by_religion_level_three(db, filter_params.sub_caste)
        gndr_intrsct_active = list(set(level_three_filter) & set(gndr_intrsct_active))
    if len(filter_params.star) > 0:
        star_filter = profile_by_star(db, filter_params.star)
        gndr_intrsct_active = list(set(star_filter) & set(gndr_intrsct_active))
    # proffesional details filter
    if len(filter_params.education) > 0:
        education_list = by_education(db, filter_params.education)
        gndr_intrsct_active = list(set(education_list) & set(gndr_intrsct_active))
    if len(filter_params.employed_in) > 0:
        employed_in_list = by_employed(db, filter_params.employed_in)
        gndr_intrsct_active = list(set(employed_in_list) & set(gndr_intrsct_active))
    if len(filter_params.occupation) > 0:
        occupation_list = by_occupation(db, filter_params.occupation)
        gndr_intrsct_active = list(set(occupation_list) & set(gndr_intrsct_active))
    if filter_params.annual_income:
        annual_income_list = by_annual_income(db, filter_params.annual_income)
        gndr_intrsct_active = list(set(annual_income_list) & set(gndr_intrsct_active))
    # location details filter
    if len(filter_params.country) > 0:
        country_list = by_country(db, filter_params.country)
        gndr_intrsct_active = list(set(country_list) & set(gndr_intrsct_active))
    if len(filter_params.citizenship) > 0:
        citizenship_list = by_citizenship(db, filter_params.citizenship)
        gndr_intrsct_active = list(set(citizenship_list) & set(gndr_intrsct_active))
    if len(filter_params.resident_status) > 0:
        resident_status_list = by_resident_status(db, filter_params.resident_status)
        gndr_intrsct_active = list(set(resident_status_list) & set(gndr_intrsct_active))
    # family details filter
    if len(filter_params.family_status) > 0:
        family_status_list = by_family_status(db, filter_params.family_status)
        gndr_intrsct_active = list(set(family_status_list) & set(gndr_intrsct_active))
    if len(filter_params.family_values) > 0:
        family_values_list = by_family_values(db, filter_params.family_values)
        gndr_intrsct_active = list(set(family_values_list) & set(gndr_intrsct_active))
    if len(filter_params.family_type) > 0:
        family_type_list = by_family_type(db, filter_params.family_type)
        gndr_intrsct_active = list(set(family_type_list) & set(gndr_intrsct_active))
    #lifestyle
    if len(filter_params.eating_habits) > 0:
        eating_habit_filter = get_profile_by_eating_habits(db, filter_params.eating_habits)
        gndr_intrsct_active = list(set(eating_habit_filter) & set(gndr_intrsct_active))
    if len(filter_params.smoking_habits) > 0:
        smoking_habit_filter = get_profile_by_smoking_habits(db, filter_params.smoking_habits)
        gndr_intrsct_active = list(set(smoking_habit_filter) & set(gndr_intrsct_active))
    if len(filter_params.drinking_habits) > 0:
        drinking_habit_filter = get_profile_by_drinking_habits(db, filter_params.drinking_habits)
        gndr_intrsct_active = list(set(drinking_habit_filter) & set(gndr_intrsct_active))
    #physical status
    if len(filter_params.physical_status) > 0:
        physical_status_filter = get_profile_by_physical_status(db, filter_params.physical_status)
        gndr_intrsct_active = list(set(physical_status_filter) & set(gndr_intrsct_active))
    #body type
    if len(filter_params.body_type) > 0:
        body_type_filter = get_profile_by_body_type(db, filter_params.body_type)
        gndr_intrsct_active = list(set(body_type_filter) & set(gndr_intrsct_active))


    random.shuffle(gndr_intrsct_active)

    # profile listing
    match_list = [ get_profile_from_id(db, i) for i in gndr_intrsct_active ]
    resp = []
    for profile in match_list:
        year,month = calculate_age_month(profile.dob)
        if profile.user_basic_details == None:
            profile_created_by_=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by_ = "Parent"
            else:
                profile_created_by_ = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star else None
            raasi=profile.user_religion_details.raasi

        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None

        image = get_partner_image(db, user, profile)

        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.id,user.id))>0:
            is_interest_send = True
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.id,user.id))>0:
            is_shortlisted = True

        match_dict, total,score = get_match_score(db,user,profile)
        percent = round((score*100)/total)
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by_,
            "image": image,
            "details": {
                "age_year": year,
                "age_month": month,
                # "age_month": dob[1],
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type
            },
            "is_user_verified": {"is_number_verified":profile.is_number_verified,"is_photos_verified":profile.is_photos_verified,"is_documents_verified":profile.is_documents_verified},
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score,
            "is_interest_send" : is_interest_send,
            "is_shortlisted" : is_shortlisted,
        }
        resp.append(pr)

    return resp


@app.get("/getUserPrivacyOptions",response_model=List[PrivacyOptionBase], status_code=200)
async def get_user_privacy_options( db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    return get_user_privacy_details(db, user.id)


@app.post("/updateUserPrivacyOptions")
async def update_user_privacy_options(privacy_options: List[PrivacyOptionCreate], db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    update_user_privacy_details(db, user.id, privacy_options)
    raise HTTPException(status_code=status.HTTP_201_CREATED,
                        detail="success")
@app.post("/profileDeactivation")
async def updateActiveStatus(reason: DeactivationReason,db: Session = Depends(get_db), Authorize: AuthJWT=Depends()): #
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    daily_users = Profile_Deactivation(db, reason, user.id)

    return daily_users

@app.get("/getPersonalProfile", status_code = status.HTTP_200_OK)
async def getPersonalProfile(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):#
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    # user=db.query(User).filter(User.id == 6).first()
    partner_details = get_personal_profile(db,user.id)#
    return partner_details


@app.get("/getEditData", status_code = status.HTTP_200_OK)
async def getEditData(db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):#
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    edit_data = get_edit_data(db,user.id)#
    return edit_data

@app.post('/subscribePackage')
async def subscribe_package(package_id: PackageId, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    package_detail = get_package_details(db, package_id.id)
    if package_detail:
        amount_to_pay = package_detail.amount
        import config
        client = razorpay.Client(auth=(config.RAZORPAY_KEY_ID, config.RAZORPAY_KEY_SECRET))
        data = {"amount": amount_to_pay*100, "currency": "INR", "receipt": "purchase"}
        payment = client.order.create(data=data)
        print(payment)
        subscribed_package = package_subscribe_crud(db, user.id, package_id.id, payment['id'])
        raise HTTPException(status_code=status.HTTP_201_CREATED,detail={"message":"package subscribed", "subscribed_package_id":subscribed_package.package_log_id, "amount_to_pay":amount_to_pay*100, "payment_id":payment['id']})
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail={"message":"package not found"})


@app.post('/subscribePackageWebhook')
async def subscribe_package_webhook(request: Request, db: Session = Depends(get_db)):
    received_json_data = await request.json()
    razorpay_order_id = received_json_data['payload']['payment']['entity']['order_id']
    payment_id = received_json_data['payload']['payment']['entity']['id']
    status = received_json_data['payload']['payment']['entity']['status']
    print(status)
    amount = received_json_data['payload']['payment']['entity']['amount']
    method = received_json_data['payload']['payment']['entity']['method']
    subscribed_package = get_package_by_razorpayID(db, razorpay_order_id)
    payment = PaymentCreate(paid_user_id=subscribed_package.user_id, payment_id=payment_id, payment_method=method,
                            amount_paid=amount, order_id=subscribed_package.id, status=status)
    add_payment_details(db, payment)
    if status == 'captured':
        if subscribed_package.subscription_status == 'new':
            expire_on = datetime.now() + timedelta(days=subscribed_package.package_log.months * 30)
            db_payment = Payment(user_id = subscribed_package.user_id, payment_id=payment_id, payment_method=method, amount_paid=amount, subscribed_package_id=subscribed_package.id, status=status)
            db.add(db_payment)
            db.commit()
            db_subscribed = db.query(SubscribePackage).filter(SubscribePackage.id==subscribed_package.id).first()
            db_subscribed.subscription_status = 'completed'
            db_subscribed.expiry_date = expire_on
            db.commit()
    return {"message": "ok"}


@app.get("/discoverInterests", status_code = status.HTTP_200_OK)
async def discover_Interests(db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    interest_sent = get_interest_sent_details_by_user(db, user.id)
    interest_sent_new = get_new_interest_sent_details_by_user(db, user.id)
    interest_received = get_interest_received_details_by_user(db, user.id)
    interest_received_new = get_new_interest_received_details_by_user(db, user.id)
    profile_viewed = get_profile_viewed_details_by_user(db, user.id)
    profile_viewed_new = get_new_profile_viewed_details_by_user(db, user.id)

    response_data = []
    interest_sent_dict = {
                "name" : "Interest You Sent",
                "count" : len(interest_sent),
                "display_images" : None,
                "new_count" : len(interest_sent_new)
            }
    interests_last_five  = interest_sent[:5]
    interest_send_images = []
    for interests_sent in interests_last_five:
        partner = get_user_by_id(db, interests_sent.user_id)
        image = get_partner_image(db, user, partner)
        interest_send_images.append(image)
    interest_sent_dict["display_images"] = interest_send_images
    response_data.append(interest_sent_dict)

    interest_received_dict = {
                "name" : "Interest You Received",
                "count" : len(interest_received),
                "display_images" : None,
                "new_count" : len(interest_received_new)
            }
    interests_rec_last_five  = interest_received[:5]
    interest_rec_images = []
    for interests_rec in interests_rec_last_five:
        partner = get_user_by_id(db, interests_rec.user_id)
        image = get_partner_image(db, user, partner)
        interest_rec_images.append(image)
    interest_received_dict["display_images"] = interest_rec_images
    response_data.append(interest_received_dict)

    profile_viewed_dict = {
                "name" : "Viewed You",
                "count" : len(profile_viewed),
                "display_images" : None,
                "new_count" : len(profile_viewed_new)
            }
    profile_view_last_five  = profile_viewed[:5]
    profile_view_images = []
    for profile_view in profile_view_last_five:
        partner = get_user_by_id(db, profile_view.user_id)
        image = get_partner_image(db, user, partner)
        profile_view_images.append(image)
    profile_viewed_dict["display_images"] = profile_view_images
    response_data.append(profile_viewed_dict)

    return response_data



# ***pagination***
@app.get("/getInterestReceivedListPagination", response_model=InterestReceivedDetailsBasePagination, status_code=200)
async def get_interest_received_by_user(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    interest_received = get_interest_received_details_by_user(db, user.id)

    interests = []
    for partner_detail in interest_received:
        partner = get_user_by_id(db, partner_detail.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        image = get_partner_image(db, user, partner)
        profile_created_by = partner_detail.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,partner_detail.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,partner_detail.user_id,user.id))>0:
            is_interest_send = True
        question_add=question_add_status(db, partner)

        useraddanswer=False
        useranswerattend = db.query(UserQuizQuestions).join(UserQuizAnswers).filter(UserQuizQuestions.user_id==user.id,UserQuizAnswers.user_id==partner.id,UserQuizQuestions.is_deleted==False,QuizQuestions.is_deleted==False).all()
        print(useranswerattend,"kkkkkkkkkkkkkkkkkkk")
        if len(useranswerattend)>0:
            useraddanswer=True

        pr ={
            "interest_id": partner_detail.interest_id,
            "partner_id": partner_detail.user_id,
            "name": partner_detail.name,
            "gender": partner_detail.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_answer_attended":useraddanswer,
            "is_user_verified": {
                "is_number_verified": partner_detail.is_number_verified,
                "is_photos_verified": partner_detail.is_photos_verified,
                "is_documents_verified": partner_detail.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": partner_detail.age_year,
                "age_month": partner_detail.age_month,
                "height_in_ft": partner_detail.height_in_ft,
                "mother_tongue": partner_detail.mother_tongue,
                "city": partner_detail.city,
                "district": partner_detail.district,
                "state": partner_detail.state,
                "level_one": partner_detail.level_one,
                "level_two": partner_detail.level_two,
                "star_name": partner_detail.star_name,
                "raasi": partner_detail.raasi,
                "qualification": partner_detail.qualification,
                "employed_in": partner_detail.employed_in,
                "occupation_name": partner_detail.occupation_name,
                "annual_income": partner_detail.annual_income,
                "currency_type": partner_detail.currency_type,
            },
            "is_viewed": partner_detail.is_viewed,
            "status": partner_detail.status,
            "updated_on": partner_detail.updated_on,
            "created_on": partner_detail.created_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        interests.append(pr)


    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(interests)

    response = {
        "users": interests[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getInterestReceivedListPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getInterestReceivedListPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getInterestReceivedListPagination?page_num={page_num+1}&page_size={page_size}"


    return response


# *****

@app.get("/getShortlistedByPartnersPagination",response_model=ProfileShortlistBasePagination, status_code=200)
async def get_shortlisted_by_partners(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    profiles = get_short_listed_by_partners(db, user.id)
    resp = []
    for profile in profiles:
        partner = get_user_by_id(db,profile.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        profile_created_by = profile.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.user_id,user.id))>0:
            is_interest_send = True
        image = get_partner_image(db, user, partner)
        question_add=question_add_status(db, partner)
        pr ={
            "shortlist_id": profile.shortlist_id,
            "partner_id": profile.user_id,
            "name": profile.name,
            "gender": profile.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": profile.is_number_verified,
                "is_photos_verified": profile.is_photos_verified,
                "is_documents_verified": profile.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": profile.age_year,
                "age_month": profile.age_month,
                "height_in_ft": profile.height_in_ft,
                "mother_tongue": profile.mother_tongue,
                "city": profile.city,
                "district": profile.district,
                "state": profile.state,
                "level_one": profile.level_one,
                "level_two": profile.level_two,
                "star_name": profile.star_name,
                "raasi": profile.raasi,
                "qualification": profile.qualification,
                "employed_in": profile.employed_in,
                "occupation_name": profile.occupation_name,
                "annual_income": profile.annual_income,
                "currency_type": profile.currency_type,
            },
            "is_viewed": profile.is_viewed,
            "created_on": profile.created_on,
            "updated_on": profile.updated_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(resp)

    response = {
        "users": resp[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getShortlistedByPartnersPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getShortlistedByPartnersPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getShortlistedByPartnersPagination?page_num={page_num+1}&page_size={page_size}"

    return response

# ***

@app.get("/getShortlistedByUserPagination",response_model=ProfileShortlistBasePagination, status_code=200)
async def get_shortlisted_by_user(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    profiles = get_short_listed_by_user(db, user.id)
    resp = []
    for profile in profiles:
        partner = get_user_by_id(db,profile.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        profile_created_by = profile.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.user_id,user.id))>0:
            is_interest_send = True
        image = get_partner_image(db, user, partner)
        question_add=question_add_status(db, partner)
        pr ={
            "shortlist_id": profile.shortlist_id,
            "partner_id": profile.user_id,
            "name": profile.name,
            "gender": profile.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": profile.is_number_verified,
                "is_photos_verified": profile.is_photos_verified,
                "is_documents_verified": profile.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": profile.age_year,
                "age_month": profile.age_month,
                "height_in_ft": profile.height_in_ft,
                "mother_tongue": profile.mother_tongue,
                "city": profile.city,
                "district": profile.district,
                "state": profile.state,
                "level_one": profile.level_one,
                "level_two": profile.level_two,
                "star_name": profile.star_name,
                "raasi": profile.raasi,
                "qualification": profile.qualification,
                "employed_in": profile.employed_in,
                "occupation_name": profile.occupation_name,
                "annual_income": profile.annual_income,
                "currency_type": profile.currency_type,
            },
            "is_viewed": profile.is_viewed,
            "created_on": profile.created_on,
            "updated_on": profile.updated_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(resp)

    response = {
        "users": resp[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getShortlistedByUserPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getShortlistedByUserPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getShortlistedByUserPagination?page_num={page_num+1}&page_size={page_size}"

    return response

# ***


@app.get("/getInterestSentByUserPagination", response_model=InterestSentDetailsBasePagination, status_code=200)
async def get_interest_sent_by_user(page_num: int = 1, page_size: int= 1,db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    interest_sent = get_interest_sent_details_by_user(db, user.id)
    interests = []
    for partner_detail in interest_sent:
        partner = get_user_by_id(db, partner_detail.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        image = get_partner_image(db, user, partner)
        profile_created_by = partner_detail.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,partner_detail.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,partner_detail.user_id,user.id))>0:
            is_interest_send = True
        question_add=question_add_status(db, partner)
        pr ={
            "interest_id": partner_detail.interest_id,
            "partner_id": partner_detail.user_id,
            "name": partner_detail.name,
            "gender": partner_detail.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": partner_detail.is_number_verified,
                "is_photos_verified": partner_detail.is_photos_verified,
                "is_documents_verified": partner_detail.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": partner_detail.age_year,
                "age_month": partner_detail.age_month,
                "height_in_ft": partner_detail.height_in_ft,
                "mother_tongue": partner_detail.mother_tongue,
                "city": partner_detail.city,
                "district": partner_detail.district,
                "state": partner_detail.state,
                "level_one": partner_detail.level_one,
                "level_two": partner_detail.level_two,
                "star_name": partner_detail.star_name,
                "raasi": partner_detail.raasi,
                "qualification": partner_detail.qualification,
                "employed_in": partner_detail.employed_in,
                "occupation_name": partner_detail.occupation_name,
                "annual_income": partner_detail.annual_income,
                "currency_type": partner_detail.currency_type,
            },
            "is_viewed": partner_detail.is_viewed,
            "status": partner_detail.status,
            "updated_on": partner_detail.updated_on,
            "created_on": partner_detail.created_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        interests.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(interests)

    response = {
        "users": interests[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getInterestSentByUserPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getInterestSentByUserPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getInterestSentByUserPagination?page_num={page_num+1}&page_size={page_size}"

    return response

# *****

@app.get("/getProfileViewedByUserPagination", response_model=ProfileVisitsDetailsBasePagination, status_code=200)
async def get_profile_viewed_by_user(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db), Authorize:AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    profile_viewed = get_profile_viewed_details_by_user(db, user.id)
    interests = []
    for partner_detail in profile_viewed:
        partner = get_user_by_id(db, partner_detail.user_id)
        prefence_details,total_preferences,score = get_match_score(db,user,partner)
        score_perge = 0
        if total_preferences > 0:
            score_perge = round((score*100)/total_preferences)
        image = get_partner_image(db, user, partner)
        profile_created_by = partner_detail.profile_created_for
        if profile_created_by == "Daughter" or profile_created_by == "Son":
            profile_created_by = "Parent"
        is_shortlisted = False
        if len(check_already_shortlisted(db,partner_detail.user_id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,partner_detail.user_id,user.id))>0:
            is_interest_send = True
        question_add=question_add_status(db, partner)
        pr ={
            "partner_id": partner_detail.user_id,
            "name": partner_detail.name,
            "gender": partner_detail.gender,
            "profile_created_by": profile_created_by,
            "question_add":question_add,
            "is_user_verified": {
                "is_number_verified": partner_detail.is_number_verified,
                "is_photos_verified": partner_detail.is_photos_verified,
                "is_documents_verified": partner_detail.is_documents_verified
            },
            "image": image,
            "details": {
                "age_year": partner_detail.age_year,
                "age_month": partner_detail.age_month,
                "height_in_ft": partner_detail.height_in_ft,
                "mother_tongue": partner_detail.mother_tongue,
                "city": partner_detail.city,
                "district": partner_detail.district,
                "state": partner_detail.state,
                "level_one": partner_detail.level_one,
                "level_two": partner_detail.level_two,
                "star_name": partner_detail.star_name,
                "raasi": partner_detail.raasi,
                "qualification": partner_detail.qualification,
                "employed_in": partner_detail.employed_in,
                "occupation_name": partner_detail.occupation_name,
                "annual_income": partner_detail.annual_income,
                "currency_type": partner_detail.currency_type,
            },
            "visited_count": partner_detail.visit_count,
            "updated_on": partner_detail.updated_on,
            "created_on": partner_detail.created_on,
            "match_score": score_perge,
            "match_score_total": total_preferences,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        interests.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(interests)

    response = {
        "users": interests[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getProfileViewedByUserPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getProfileViewedByUserPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getProfileViewedByUserPagination?page_num={page_num+1}&page_size={page_size}"

    return response

# ***

@app.get("/Matches_in_your_city_pagination", status_code = status.HTTP_200_OK)
async def matches_in_your_city(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db),Authorize:AuthJWT=Depends()):#
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    city_match = get_city_match_detailed(db, user.id)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(city_match)

    response = {
        "users": city_match[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/Matches_in_your_city_pagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/Matches_in_your_city_pagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/Matches_in_your_city_pagination?page_num={page_num+1}&page_size={page_size}"

    return response

# ***


@app.get("/getProfessionalPreferenceMatchPagination",response_model=ProfesionalPreferenceProfilePagination,status_code=200)
async def get_professional_preference_match(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db),Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    final_users = get_professional_preference_match_users(db, user.id)
    resp = []
    for profile in final_users:

        age=calculate_age(profile.dob)
        if profile.user_basic_details == None:
            profile_created_for=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue
        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city,
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one
            caste_name = profile.user_religion_details.religion.level_two
            star_name= profile.user_religion_details.star.star_name
            raasi=profile.user_religion_details.raasi
        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification
            employed_in=profile.user_professional_details.employed_in
            occupation_name= profile.user_professional_details.occupation.occupation_name
            annual_income= profile.user_professional_details.annual_income
            currency_type=profile.user_professional_details.currency_type

        gndr=[i for i in get_gender_preference(db,user.id)]
        if len(profile.user_table)==0:
            if gndr =='Male':
                image=default_avatar_male
            elif gndr =='Female':
                image=default_avatar_female
            else:
                image=default_avatar_others
        else:
            img=[i for i in profile.user_table]
            for value in img:
                if value.is_main==True:image=value.image
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_for": profile_created_for,
            "image": image,
            "details": {
                "age": age,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type,
            },
            "created_on": profile.date_joined,
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":user.is_documents_verified},
            "match_score": 85,
            "match_score_total": 45,
            "match_score_matching_nos": 25
        }
        resp.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(resp)

    response = {
        "users": resp[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getProfessionalPreferenceMatchPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getProfessionalPreferenceMatchPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getProfessionalPreferenceMatchPagination?page_num={page_num+1}&page_size={page_size}"

    return response


# ***


@app.get("/getMyProfessionMatchPagination",status_code=200)
async def get_my_profession_match(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db),Authorize: AuthJWT=Depends()): #
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    final_users = get_my_profession_match_users(db, user.id)
    resp = []
    for profile in final_users:
        age=calculate_age(profile.dob)
        if profile.user_basic_details == None:
            profile_created_for=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for != None else None
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue != None else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city,
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion != None else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion != None else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star != None else None
            raasi=profile.user_religion_details.raasi
        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education != None else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in != None else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation != None else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income != None else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type != None else None

        match_dict, total,score = get_match_score(db,user,profile)
        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        image = get_partner_image(db, user, profile)
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_for": profile_created_for,
            "image": image,
            "details": {
                "age": age,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type,
            },
            "created_on": profile.date_joined,
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":user.is_documents_verified},
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score
        }
        resp.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(resp)

    response = {
        "users": resp[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getMyProfessionMatchPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getMyProfessionMatchPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getMyProfessionMatchPagination?page_num={page_num+1}&page_size={page_size}"

    return response


# ***

@app.get("/getEducationMatchPagination",status_code=200)
def getEducationMatch(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()): #
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    match_list = get_education_match_list(db, user.id)
    resp = []
    for profile in match_list:
        age=calculate_age(profile.dob)
        if profile.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by = "Parent"
            else:
                profile_created_by = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star else None
            raasi=profile.user_religion_details.raasi

        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None

        image = get_partner_image(db, user, profile)
        if profile.user_received_interests==[]:
            interest = False
        else:
            for i in profile.user_received_interests:
                if i.from_user_id == user.id:
                    interest = True
                    break
                else:
                    interest = False

        if profile.profile_shortlisted_users==[]:
            shortlisted = False
        else:
            for i in profile.profile_shortlisted_users:
                if i.from_user_id == user.id:
                    shortlisted = True
                    break
                else:
                    shortlisted = False

        if profile.user_subscribed==[]:
            premium = False
        else:
            premium_ids = get_premium_package_ids(db)
            for i in profile.user_subscribed:
                if i.package_log_id in premium_ids:
                    premium = True
                    break
                else:
                    premium = False

        match_dict, total,score = get_match_score(db,user,profile)
        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by,
            "image": image,
            "details": {
                "age": age,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type
            },
            "is_user_verified": {"is_number_verified":user.is_number_verified,"is_photos_verified":user.is_photos_verified,"is_documents_verified":user.is_documents_verified},
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score
        }
        resp.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(resp)

    response = {
        "users": resp[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getEducationMatchPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getEducationMatchPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getEducationMatchPagination?page_num={page_num+1}&page_size={page_size}"

    return response


# ****


@app.get("/getNewMatchAllPagination",status_code=200)
async def getNewMatchAll(page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db,auth)
    match_list = get_new_match_list(db, user.id)
    resp = []
    for profile in match_list:
        age=calculate_age(profile.dob)
        if profile.user_basic_details == None:
            profile_created_by=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by = "Parent"
            else:
                profile_created_by = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star else None
            raasi=profile.user_religion_details.raasi

        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None

        image = get_partner_image(db, user, profile)
        match_dict, total,score = get_match_score(db,user,profile)

        is_shortlisted = False
        # print(len(check_already_shortlisted(db,profile.id,user.id)),"kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
        if len(check_already_shortlisted(db,profile.id,user.id))>0:
            is_shortlisted = True
        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.id,user.id))>0:
            is_interest_send = True

        try:
            percent = round((score*100)/total)
        except ZeroDivisionError:
            percent = 0
        # question_add=question_add_status(db, partner)
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by,
            "image": image,
            # "question_add":question_add,
            "details": {
                "age": age,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type
            },
            "is_user_verified": {"is_number_verified":profile.is_number_verified,"is_photos_verified":profile.is_photos_verified,"is_documents_verified":profile.is_documents_verified},
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score,
            "is_shortlisted": is_shortlisted,
            "is_interest_send": is_interest_send
        }
        resp.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(resp)

    response = {
        "users": resp[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/getNewMatchAllPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/getNewMatchAllPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/getNewMatchAllPagination?page_num={page_num+1}&page_size={page_size}"

    return response


# ***

@app.post("/profileFilterPagination",status_code=200)
async def profile_filter(filter_params: FilterParameters, page_num: int = 1, page_size: int= 1, db: Session = Depends(get_db), Authorize: AuthJWT=Depends()):
    Authorize.jwt_required()
    auth = Authorize.get_jwt_subject()
    user = get_user_details(db, auth)
    gender_list = gender_pref_list2(db,user.id, filter_params.search_key)
    active_list = active_users(db,user.id)
    gndr_intrsct_active = list(set(gender_list) & set(active_list))
    print("======filter1111", gndr_intrsct_active)
    # profile type filter
    if filter_params.show_profiles_created:
        show_profile_create_list = []
        if (filter_params.show_profiles_created=="with_in_a_day"):
            show_profile_create_list.extend(within_a_day(db))
        elif (filter_params.show_profiles_created=="with_in_a_week"):
            show_profile_create_list.extend(within_a_week(db))
        elif (filter_params.show_profiles_created=="with_in_a_month"):
            show_profile_create_list.extend(within_a_month(db))
        gndr_intrsct_active = list(set(show_profile_create_list) & set(gndr_intrsct_active))
        print("======filter2222", show_profile_create_list)
    if filter_params.profiles:
        profiles_list = []
        if ("with_photo" in filter_params.profiles):
            profiles_list.extend(with_photo(db))
        gndr_intrsct_active = list(set(profiles_list) & set(gndr_intrsct_active))
        print("======filter33333", profiles_list)
    if filter_params.profiles_created_by:
        profile_created_by_list = profile_created_by(db,filter_params.profiles_created_by)
        if ("Parent" in filter_params.profiles_created_by):
            profile_created_by_list.extend(profile_by_parent(db))
        gndr_intrsct_active = list(set(profile_created_by_list) & set(gndr_intrsct_active))
    # basic profile filter
    if filter_params.age_lower_limit is not None:
        age_filter = profile_by_age_limit(db, filter_params.age_lower_limit, filter_params.age_upper_limit)
        gndr_intrsct_active = list(set(age_filter) & set(gndr_intrsct_active))
    if filter_params.height_lower_limit is not None:
        height_filter = profile_by_height_limit(db, filter_params.height_lower_limit, filter_params.height_upper_limit)
        gndr_intrsct_active = list(set(height_filter) & set(gndr_intrsct_active))
    if len(filter_params.mother_tongue) > 0:
        mother_tongue_filter = profile_by_mother_tongue(db, filter_params.mother_tongue)
        gndr_intrsct_active = list(set(mother_tongue_filter) & set(gndr_intrsct_active))
    if len(filter_params.marital_status) > 0:
        marital_status_filter = profile_by_marital_status(db, filter_params.marital_status)
        gndr_intrsct_active = list(set(marital_status_filter) & set(gndr_intrsct_active))
    #religion filter
    if filter_params.religion is not None:
        level_one_filter = profile_by_religion_level_one(db, filter_params.religion)
        gndr_intrsct_active = list(set(level_one_filter) & set(gndr_intrsct_active))
    if len(filter_params.caste) > 0:
        level_two_filter = profile_by_religion_level_two(db, filter_params.caste)
        gndr_intrsct_active = list(set(level_two_filter) & set(gndr_intrsct_active))
    if len(filter_params.sub_caste) > 0:
        level_three_filter = profile_by_religion_level_three(db, filter_params.sub_caste)
        gndr_intrsct_active = list(set(level_three_filter) & set(gndr_intrsct_active))
    if len(filter_params.star) > 0:
        star_filter = profile_by_star(db, filter_params.star)
        gndr_intrsct_active = list(set(star_filter) & set(gndr_intrsct_active))
    # proffesional details filter
    if len(filter_params.education) > 0:
        education_list = by_education(db, filter_params.education)
        gndr_intrsct_active = list(set(education_list) & set(gndr_intrsct_active))
    if len(filter_params.employed_in) > 0:
        employed_in_list = by_employed(db, filter_params.employed_in)
        gndr_intrsct_active = list(set(employed_in_list) & set(gndr_intrsct_active))
    if len(filter_params.occupation) > 0:
        occupation_list = by_occupation(db, filter_params.occupation)
        gndr_intrsct_active = list(set(occupation_list) & set(gndr_intrsct_active))
    if filter_params.annual_income:
        annual_income_list = by_annual_income(db, filter_params.annual_income)
        gndr_intrsct_active = list(set(annual_income_list) & set(gndr_intrsct_active))
    # location details filter
    if len(filter_params.country) > 0:
        country_list = by_country(db, filter_params.country)
        gndr_intrsct_active = list(set(country_list) & set(gndr_intrsct_active))
    if len(filter_params.citizenship) > 0:
        citizenship_list = by_citizenship(db, filter_params.citizenship)
        gndr_intrsct_active = list(set(citizenship_list) & set(gndr_intrsct_active))
    if len(filter_params.resident_status) > 0:
        resident_status_list = by_resident_status(db, filter_params.resident_status)
        gndr_intrsct_active = list(set(resident_status_list) & set(gndr_intrsct_active))
    # family details filter
    if len(filter_params.family_status) > 0:
        family_status_list = by_family_status(db, filter_params.family_status)
        gndr_intrsct_active = list(set(family_status_list) & set(gndr_intrsct_active))
    if len(filter_params.family_values) > 0:
        family_values_list = by_family_values(db, filter_params.family_values)
        gndr_intrsct_active = list(set(family_values_list) & set(gndr_intrsct_active))
    if len(filter_params.family_type) > 0:
        family_type_list = by_family_type(db, filter_params.family_type)
        gndr_intrsct_active = list(set(family_type_list) & set(gndr_intrsct_active))
    #lifestyle
    if len(filter_params.eating_habits) > 0:
        eating_habit_filter = get_profile_by_eating_habits(db, filter_params.eating_habits)
        gndr_intrsct_active = list(set(eating_habit_filter) & set(gndr_intrsct_active))
    if len(filter_params.smoking_habits) > 0:
        smoking_habit_filter = get_profile_by_smoking_habits(db, filter_params.smoking_habits)
        gndr_intrsct_active = list(set(smoking_habit_filter) & set(gndr_intrsct_active))
    if len(filter_params.drinking_habits) > 0:
        drinking_habit_filter = get_profile_by_drinking_habits(db, filter_params.drinking_habits)
        gndr_intrsct_active = list(set(drinking_habit_filter) & set(gndr_intrsct_active))
    #physical status
    if len(filter_params.physical_status) > 0:
        physical_status_filter = get_profile_by_physical_status(db, filter_params.physical_status)
        gndr_intrsct_active = list(set(physical_status_filter) & set(gndr_intrsct_active))
    #body type
    if len(filter_params.body_type) > 0:
        body_type_filter = get_profile_by_body_type(db, filter_params.body_type)
        gndr_intrsct_active = list(set(body_type_filter) & set(gndr_intrsct_active))


    random.shuffle(gndr_intrsct_active)
    print("_________gndr_intrsct_active", gndr_intrsct_active)

    # profile listing
    match_list = [ get_profile_from_id(db, i) for i in gndr_intrsct_active ]
    resp = []
    for profile in match_list:
        age=calculate_age(profile.dob)
        if profile.user_basic_details == None:
            profile_created_by_=None
            height_in_ft=None
            mother_tongue=None
        else:
            profile_created_for=profile.user_basic_details.profile_created_for.value if profile.user_basic_details.profile_created_for else None
            if profile_created_for == "Son" or profile_created_for == "Daughter":
                profile_created_by_ = "Parent"
            else:
                profile_created_by_ = profile_created_for
            height_in_ft=profile.user_basic_details.height_in_ft
            mother_tongue= profile.user_basic_details.mother_tongue.value if profile.user_basic_details.mother_tongue else None

        if profile.user_location_details == None:
            city=None
            district=None
            state=None
        else:
            city=profile.user_location_details.city
            district=profile.user_location_details.district
            state=profile.user_location_details.state

        if profile.user_religion_details==None:
            religion_name= None
            caste_name = None
            star_name= None
            raasi=None
        else:
            religion_name= profile.user_religion_details.religion.level_one if profile.user_religion_details.religion else None
            caste_name = profile.user_religion_details.religion.level_two if profile.user_religion_details.religion else None
            star_name= profile.user_religion_details.star.star_name if profile.user_religion_details.star else None
            raasi=profile.user_religion_details.raasi

        if profile.user_professional_details==None:
            qualification=None
            employed_in=None
            occupation_name=None
            annual_income=None
            currency_type=None
        else:
            qualification= profile.user_professional_details.highest_education.qualification if profile.user_professional_details.highest_education else None
            employed_in=profile.user_professional_details.employed_in.value if profile.user_professional_details.employed_in else None
            occupation_name= profile.user_professional_details.occupation.occupation_name if profile.user_professional_details.occupation else None
            annual_income= profile.user_professional_details.annual_income.value if profile.user_professional_details.annual_income else None
            currency_type=profile.user_professional_details.currency_type.value if profile.user_professional_details.currency_type else None

        image = get_partner_image(db, user, profile)

        is_interest_send = False
        if len(check_partner_interest_already_send(db,profile.id,user.id))>0:
            is_interest_send = True
        is_shortlisted = False
        if len(check_already_shortlisted(db,profile.id,user.id))>0:
            is_shortlisted = True

        match_dict, total,score = get_match_score(db,user,profile)
        percent = round((score*100)/total)
        pr ={
            "id": profile.id,
            "name": profile.name,
            "profile_created_by": profile_created_by_,
            "image": image,
            "details": {
                "age": age,
                "height_in_ft": height_in_ft,
                "mother_tongue": mother_tongue,
                "city": city,
                "district":district ,
                "state":state ,
                "level_one": religion_name,
                "level_two": caste_name,
                "star_name": star_name,
                "raasi": raasi,
                "qualification": qualification,
                "employed_in": employed_in,
                "occupation_name": occupation_name,
                "annual_income":annual_income,
                "currency_type": currency_type
            },
            "is_user_verified": {"is_number_verified":profile.is_number_verified,"is_photos_verified":profile.is_photos_verified,"is_documents_verified":profile.is_documents_verified},
            "created_on": profile.date_joined,
            "match_score": percent,
            "match_score_total": total,
            "match_score_matching_nos": score,
            "is_interest_send" : is_interest_send,
            "is_shortlisted" : is_shortlisted,
        }
        resp.append(pr)

    start = (page_num - 1) * page_size
    end = start + page_size
    data_length = len(resp)

    response = {
        "users": resp[start:end],
        "total": data_length,
        "count": page_size,
        "pagination": {}
    }

    if end >= data_length:
        response["pagination"]["next"] = None

        if page_num > 1:
            response["pagination"]["previous"] = f"/profileFilterPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
    else:
        if page_num > 1:
            response["pagination"]["previous"] = f"/profileFilterPagination?page_num={page_num-1}&page_size={page_size}"
        else:
            response["pagination"]["previous"] = None
        response["pagination"]["next"] = f"/profileFilterPagination?page_num={page_num+1}&page_size={page_size}"

    return response

# ****

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8089, log_level="info", reload=True, debug=True)
